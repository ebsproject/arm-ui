import { ApolloClient, InMemoryCache, gql } from '@apollo/client';
import axios from 'axios';
/*
 Initial state and properties
 */
export const initialState = {
  experiment: [],
  responseVariableTrait: [],
  analysisObjective: 0,
  traitAnalysisPattern: '',
  experimentLocationAnalysisPattern: '',
  analysisConfiguration: 0,
  mainModel: 0,
  prediction: 0,
  spatialAdjusting: 0,
};
/*
 Action types
 */
export const ADD_EXPERIMENT_ACTION = 'ADD_EXPERIMENT';
export const ADD_RESPONSEVT_ACTION = 'ADD_RESPONSEVARIABLETRAIT';
export const ADD_ANALISYSO_ACTION = 'ADD_ANALYSISOBJECTIVE';
export const ADD_TRAITAP_ACTION = 'ADD_TRAITANALYSISPATTERN';
export const ADD_EXPERIMENTLAP_ACTION = 'ADD_EXPERIMENTLOCATIONANALYSISPATTERN';
export const ADD_ANALISYSC_ACTION = 'ADD_ANALYSISCONFIGURATION';
export const ADD_MAINM_ACTION = 'ADD_MAINMODEL';
export const ADD_PREDICTION_ACTION = 'ADD_PREDICTION';
export const ADD_SPATIALA_ACTION = 'ADD_SPATIALADJUSTING';
export const NEW_REQUEST_ACTION = 'NEW_REQUEST';
export const ADD_REMOVE_EXPERIMENT_ACTION = 'ADD_REMOVE_EXPERIMENT';
export const ADD_REMOVE_RESPONSEVT_ACTION = 'ADD_REMOVE_RESPONSEVARIABLETRAIT';
/*
 Arrow function for change state
 */
export const setData = (action, payload) => ({
  type: action,
  payload,
});
/*
 Reducer to describe how the state changed
 */
export default function Reducer(state = initialState, { type, payload }) {
  switch (type) {
    case ADD_EXPERIMENT_ACTION:
      return {
        ...state,
        experiment: payload,
      };
    case ADD_RESPONSEVT_ACTION:
      return {
        ...state,
        responseVariableTrait: payload,
      };
    case ADD_ANALISYSO_ACTION:
      return {
        ...state,
        analysisObjective: payload,
      };
    case ADD_TRAITAP_ACTION:
      return {
        ...state,
        traitAnalysisPattern: payload,
      };
    case ADD_EXPERIMENTLAP_ACTION:
      return {
        ...state,
        experimentLocationAnalysisPattern: payload,
      };
    case ADD_ANALISYSC_ACTION:
      return {
        ...state,
        analysisConfiguration: payload,
      };
    case ADD_MAINM_ACTION:
      return {
        ...state,
        mainModel: payload,
      };
    case ADD_PREDICTION_ACTION:
      return {
        ...state,
        prediction: payload,
      };
    case ADD_SPATIALA_ACTION:
      return {
        ...state,
        spatialAdjusting: payload,
      };
    case NEW_REQUEST_ACTION:
      return {
        ...state,
        experiment: [],
        responseVariableTrait: [],
        analysisObjective: 0,
        traitAnalysisPattern: '',
        experimentLocationAnalysisPattern: '',
        analysisConfiguration: 0,
        mainModel: 0,
        prediction: 0,
        spatialAdjusting: 0,
      };
    case ADD_REMOVE_EXPERIMENT_ACTION:
      let exps = state.experiment;
      let index = exps.findIndex(
        (exp) => exp.occurrenceDbId === payload.data.occurrenceDbId,
      );
      if (payload.add) {
        if (index === -1) {
          exps.push(payload.data);
        }
      } else {
        if (index > -1) {
          exps.splice(index, 1);
        }
      }
      return {
        ...state,
        experiment: exps,
      };
    case ADD_REMOVE_RESPONSEVT_ACTION:
      let traits = state.responseVariableTrait;
      if (payload.add) {
        traits.push(payload.data);
      } else {
        let index = traits.findIndex((trait) => trait.id === payload.data.id);
        if (index > -1) {
          traits.splice(index, 1);
        }
      }
      return {
        ...state,
        responseVariableTrait: traits,
      };
    default:
      return state;
  }
}
