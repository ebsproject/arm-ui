import AddRequest from './AddRequest';
import React from 'react';
import { render, cleanup, screen } from '@testing-library/react';
import '@testing-library/dom';
import '@testing-library/jest-dom/extend-expect';

afterEach(cleanup);

const props = {
  setting: {
    tokenHelpPopUp: false,
    action: {
      NEW_REQUEST_ACTION: '',
    },
    routes: {
      rotuelist: '',
    },
  },
  selects: {
    responseVariableTrait: {},
    analysisObjective: {},
    traitAnalysisPattern: {},
    experimentLocationAnalysisPattern: {},
    analysisConfiguration: {},
    mainModel: {},
    prediction: {},
    spatialAdjusting: {},
  },
  experiment: {
    style: { rowOccurrence: { backgroundColor: '' } },
  },
  titledata: {
    title: '',
    subtitle: '',
    note: '',
  },
  request: {},
  version: '',
};
jest.mock('@material-ui/styles', () => ({
  ...jest.requireActual('@material-ui/styles'),
  makeStyles: jest.fn().mockReturnValue(
    jest.fn().mockImplementation(() =>
      Promise.resolve({
        grid: {},
      }),
    ),
  ),
}));
jest.mock('../../organisms/AddRequestTab', () => () => {
  return <div></div>;
});

test('AddRequest is in the DOM', () => {
  render(<AddRequest {...props}></AddRequest>);
  expect(screen.getByTestId('AddRequestTestId')).toBeInTheDocument();
});
