import React from 'react'
import ReactDOM from 'react-dom'
// Component to be Test
import MainRequest from './mainrequest'
// Test Library
import { render, cleanup } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'

afterEach(cleanup)
// Props to send component to be rendered
const props = {
  requestlist: {},
  navbar: {},
  version: '',
}
jest.mock('@material-ui/styles', () => ({
  ...jest.requireActual('@material-ui/styles'),
  makeStyles: jest.fn().mockReturnValue(
    jest.fn().mockImplementation(() =>
      Promise.resolve({
        grid: {},
      }),
    ),
  ),
}))
jest.mock('../../organisms/MainTable', () => () => {
  return <div></div>
})
jest.mock('../../organisms/MainNavBar', () => () => {
  return <div></div>
})
jest.mock('../../organisms/Footer', () => () => {
  return <div></div>
})

test('Report name', () => {
  const div = document.createElement('div')
  ReactDOM.render(<MainRequest {...props}></MainRequest>, div)
})

test('Render correctly', () => {
  const { getByTestId } = render(<MainRequest {...props}></MainRequest>)
  expect(getByTestId('MainRequestTestId')).toBeInTheDocument()
})
