import React from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS
import { Table, TableBody, TableCell, TableRow } from '@material-ui/core'
import { Alert } from '@material-ui/lab'

//MAIN FUNCTION
/**
 * Display message in new table with the same size to other table
 * @param props: component properties
 * @param ref: reference made by React.forward
 */
const MessageRowAtom = React.forwardRef((props, ref) => {
  const { children, message, numberVisibleColumn, ...rest } = props
  const isMessage = message !== ''
  return (
    /* 
     @prop data-testid: Id to use inside messagerow.test.js file.
    */
    <Table data-testid={'MessageRowTestId'} ref={ref}>
      <TableBody>
        <TableRow>
          {isMessage ? (
            <TableCell>
              <Alert severity='error'>{message}</Alert>
            </TableCell>
          ) : (
            <></>
          )}
        </TableRow>
      </TableBody>
    </Table>
  )
})
// Type and required properties
MessageRowAtom.propTypes = {
  /**Message to display*/
  message: PropTypes.string.isRequired,
  /**Number of column to align the message with the grid*/
  numberVisibleColumn: PropTypes.number.isRequired,
}
// Default properties
MessageRowAtom.defaultProps = {
  message: '',
}

export default MessageRowAtom
