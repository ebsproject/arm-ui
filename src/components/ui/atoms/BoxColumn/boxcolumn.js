import React from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS
import {
  Box,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from '@material-ui/core';

//MAIN FUNCTION
/**
 * @description Box with borders to display information in read-only columns and rows
 * @param props - component properties
 * @param ref - reference made by React.forward
 * @param {array} props.columns - Information about the columns
 * @param {array} props.columns[].field - Id to search into the rows
 * @param {array} props.columns[].headerName - Title of the column
 * @param {array} props.rows - Rows with data
 * @param {string} props.maxheight [0|number] - If is 0 remove the maxHeight in TableContainer component, other value set maxHeight of component
 * @returns Return componet with border and simple table into it
 * @returns data-testid - ID to use inside boxcolumn.test.js file.
 */
const BoxColumnAtom = React.forwardRef((props, ref) => {
  const { children, rows, columns, maxheight, ...rest } = props;
  const styleTableContainer = maxheight === undefined || maxheight === 0 ?{} : { maxHeight: maxheight };
  return (
    <Box border={1} data-testid={'BoxColumnTestId'} ref={ref}>
      <TableContainer  component={Paper} style={styleTableContainer}>
        <Table>
          <TableHead>
            <TableRow>
              {columns.map((column, index) => (
                <TableCell key={`columns_${index}`}>{column.headerName}</TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row, rowIndex) => (
              <TableRow key={`rows_${rowIndex}`}>
                {columns.map((column, columnIndex) => (
                  <TableCell key={`cell_${rowIndex}_${columnIndex}`}>
                    {row[column.field]}
                  </TableCell>
                ))}
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Box>
  );
});
// Type and required properties
BoxColumnAtom.propTypes = {
  rows: PropTypes.array.isRequired,
  columns: PropTypes.array.isRequired,
  maxheight: PropTypes.number,
};
// Default properties
BoxColumnAtom.defaultProps = {};

export default BoxColumnAtom;
