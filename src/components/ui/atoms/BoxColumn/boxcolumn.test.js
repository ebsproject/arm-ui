import React from 'react'
import ReactDOM from 'react-dom'
// Component to be Test
import BoxColumn from './boxcolumn'
// Test Library
import { render, cleanup } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'

afterEach(cleanup)
// Props to send component to be rendered
const props = {
  columns: [{ field: 'a', headerName: 'a' }],
  rows: [{ a: 'a' }],
}

test('Report name', () => {
  const div = document.createElement('div')
  ReactDOM.render(<BoxColumn {...props}></BoxColumn>, div)
})

test('Render correctly', () => {
  const { getByTestId } = render(<BoxColumn {...props}></BoxColumn>)
  expect(getByTestId('BoxColumnTestId')).toBeInTheDocument()
})
