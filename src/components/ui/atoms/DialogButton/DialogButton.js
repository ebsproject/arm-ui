import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
// GLOBALIZATION COMPONENT
import { FormattedMessage } from 'react-intl';
// CORE COMPONENTS
import {
  Box,
  Dialog,
  DialogContent,
  DialogTitle,
  Grid,
  IconButton,
  Typography,
} from '@material-ui/core';
import SettingsIcon from '@material-ui/icons/Settings';
import CloseIcon from '@material-ui/icons/Close';
import ButtonIconToolbarAtom from 'components/ui/atoms/buttons/ButtonIconToolbar';
//MAIN FUNCTION
/**
 * @description           Button to display a dialog component
 * @param {string}        title - title of the dialog component
 * @param {string}        helptext - Helper text to button to open dialog component
 * @param {object}        classes - Classes to put spesific style with makestyle
 * @param {component}     children - Component to display in dialog content
 * @param {string}        ref - Reference made by React.forward
 * @property {string}     data-testid: Id to use inside DialogButton.test.js file.
 */
const DialogButtonAtom = React.forwardRef(
  ({ children, classes, helptext, title }, ref) => {
    const [open, setOpen] = useState(false);

    const handleClose = () => {
      setOpen(false);
    };
    const handleConfigButtonClick = () => {
      setOpen(true);
    };
    return (
      <Box component='div' ref={ref} data-testid={'DialogButtonTestId'}>
        <ButtonIconToolbarAtom
          helptext={helptext}
          functiononclick={handleConfigButtonClick}
          color={'primary'}
          classbutton={classes.button}
          icon={<SettingsIcon  className="fill-current text-white" />}
        >
        </ButtonIconToolbarAtom>
        <Dialog
          onClose={handleClose}
          aria-labelledby='dialog-configuration-column'
          open={open}
          fullWidth={true}
          maxWidth={'sm'}
        >
          <DialogTitle id='dialog-configuration-column-title'>
            <Grid
              container
              direction='row'
              justifyContent='space-between'
              alignItems='flex-start'
            >
              <Grid item>
                <Typography variant='h5'>{title}</Typography>
              </Grid>
              <Grid item>
                <IconButton
                  edge='end'
                  color='inherit'
                  onClick={handleClose}
                  aria-label='close'
                >
                  <CloseIcon />
                </IconButton>
              </Grid>
            </Grid>
          </DialogTitle>
          <DialogContent dividers>{children}</DialogContent>
        </Dialog>
      </Box>
    );
  },
);
// Type and required properties
DialogButtonAtom.propTypes = {
  title: PropTypes.string.isRequired,
  helptext: PropTypes.string.isRequired,
  classes: PropTypes.object.isRequired,
};
// Default properties
DialogButtonAtom.defaultProps = {};

export default DialogButtonAtom;
