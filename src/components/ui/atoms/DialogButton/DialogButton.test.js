import DialogButton from './DialogButton';
import React from 'react';
import { render, cleanup, screen } from '@testing-library/react';
import '@testing-library/dom';
import '@testing-library/jest-dom/extend-expect';

afterEach(cleanup);

const props = {
  children: <></>,
  classes: {
    button: '',
  },
  helptext: '',
  title: '',
};
test('DialogButton is in the DOM', () => {
  render(<DialogButton {...props}></DialogButton>);
  expect(screen.getByTestId('DialogButtonTestId')).toBeInTheDocument();
});
