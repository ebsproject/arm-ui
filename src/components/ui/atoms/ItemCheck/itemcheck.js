import React, { useState } from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS
import {
  Checkbox,
  FormControlLabel,
  Grid,
  ListItemIcon,
  ListItemText,
  Switch,
} from '@material-ui/core';

//MAIN FUNCTION
/**
 * @description          - Componente to dislpay the columns that be able to hidden or show in the grid
 * @param {object}       props - Children properties
 * @param {string}       label - Text to display information
 * @param {boolean}      ischeck - Put the chen in true or false
 * @param {object}       classes - Classes to put spesific style with makestyle
 * @param {function}     handlechangecolumns - Function to launch when the item is checket to modefile the column
 * @param {string}       field - Identifaider to search the column in the array column
 * @param {string}       ref - reference made by React.forward
 * @function {void}      handelCheck - Function to call parent function to enable o disable column in the grid
 * @property {string}    data-testid - Id to use inside itemcheck.test.js file.
 */
const ItemCheckAtom = React.forwardRef((props, ref) => {
  const {
    children,
    label,
    ischeck,
    classes,
    handlechangecolumns,
    field,
    iscancheck,
    ...rest
  } = props;
  const [isCheck, setIsCheck] = useState(ischeck);
  const handelCheck = () => {
    setIsCheck((check) => !check);
    handlechangecolumns(field, isCheck);
  };

  return (
    <Grid
      container
      direction='row'
      justifyContent='flex-start'
      alignItems='flex-start'
      data-testid={'ItemCheckTestId'}
      ref={ref}
    >
      <Grid item>
        <FormControlLabel
          disabled={!iscancheck}
          control={<Checkbox checked={isCheck} onChange={handelCheck} />}
          label={label}
        />
      </Grid>
    </Grid>
  );
});
// Type and required properties
ItemCheckAtom.propTypes = {
  label: PropTypes.string.isRequired,
  ischeck: PropTypes.bool.isRequired,
  classes: PropTypes.object.isRequired,
  handlechangecolumns: PropTypes.func.isRequired,
  field: PropTypes.string.isRequired,
  iscancheck: PropTypes.bool.isRequired,
};
// Default properties
ItemCheckAtom.defaultProps = {
  iscancheck: true,
};

export default ItemCheckAtom;
