import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS
import { IconButton, Snackbar } from '@material-ui/core'
import CloseIcon from '@material-ui/icons/Close'

//MAIN FUNCTION
/**
 * @description       Display informatio for some time
 * @param {object}    props - component properties
 * @param {boolean}   open - Flag to display the component
 * @param {function}  closeparent - Parent function to close the component and false the prop open
 * @param {string}    message - Message to display in the component
 * @param {number}    time - Milisecond that message were display by default 1 minute
 * @param {reference} ref - reference made by React.forward
 * @property {object} position - Define the vertial and gorizontal position of the component
 * @property {string} data-testid: Id to use inside infomessage.test.js file.
 * @returns {element} Snack bar component, auto hide by default 1 minute
 */
const InfoMessageAtom = React.forwardRef((props, ref) => {
  const { children, open, closeparent, message, time, ...rest } = props
  const position = {
    vertical: 'bottom',
    horizontal: 'left',
  }

  return (
    <div data-testid={'InfoMessageTestId'}>
      <Snackbar
        anchorOrigin={position}
        open={open}
        autoHideDuration={time}
        onClose={closeparent}
        message={message}
        action={
          <Fragment>
            <IconButton
              size='small'
              aria-label='close'
              color='inherit'
              onClick={closeparent}
            >
              <CloseIcon fontSize='small' />
            </IconButton>
          </Fragment>
        }
        ref={ref}
      />
    </div>
  )
})
// Type and required properties
InfoMessageAtom.propTypes = {
  open: PropTypes.bool.isRequired,
  closeparent: PropTypes.func.isRequired,
  message: PropTypes.string.isRequired,
  time: PropTypes.number,
}
// Default properties
InfoMessageAtom.defaultProps = {
  time: 60000,
}

export default InfoMessageAtom
