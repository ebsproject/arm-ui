import React from 'react'
import ReactDOM from 'react-dom'
// Component to be Test
import InfoMessage from './infomessage'
// Test Library
import { render, cleanup } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'

afterEach(cleanup)

// Props to send component to be rendered
const props = {
  open: false,
  closeparent: jest.fn(),
  message: '',
  time: 50000,
}

test('Report name', () => {
  const div = document.createElement('div')
  ReactDOM.render(<InfoMessage {...props}></InfoMessage>, div)
})

test('Render correctly', () => {
  const { getByTestId } = render(<InfoMessage {...props}></InfoMessage>)
  expect(getByTestId('InfoMessageTestId')).toBeInTheDocument()
})
