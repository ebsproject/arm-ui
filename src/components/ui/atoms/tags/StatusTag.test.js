import { unmountComponentAtNode } from "react-dom";
import StatusTagAtom from "./StatusTag";
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'


let container = null;
beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
});
afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
});

let props = {
    classes: {
        complete: 'classComplete',
        inproccess: 'classInProccess',
        pending: '',
        failure: '',
    },
    text: ''
}
const id = 'StatusTagAtomTestId';
describe('Mount component Status tag', () => {
    it('Mount and search by testId', () => {
        render(<StatusTagAtom {...props}> </StatusTagAtom>, container);
        expect(screen.getByTestId(id)).toBeInTheDocument()
    });
});
describe('Search properties on StatusTag', ()=>{
    it('Search text of tag equals to "DONE"', () => {
        props.text = 'DONE';
        render(<StatusTagAtom {...props}> </StatusTagAtom>, container);
        expect(screen.getByText(props.text)).toBeInTheDocument()
    })
    it('Search only class "classComplete"', () => {
        props.text = 'DONE';
        render(<StatusTagAtom {...props}> </StatusTagAtom>, container);
        const component = screen.getByTestId(id);
        expect(component.classList.value.includes(props.classes.complete)).toEqual(true);
        expect(component.classList.value.includes(props.classes.inproccess)).toEqual(false);
    })
})

