import PropTypes from 'prop-types';
import { Box, Typography } from '@material-ui/core';
import { STATE_REQUEST_COMPLETE, STATE_REQUEST_INPROGRESS, STATE_REQUEST_PENDING } from 'utils/helpers/afHelper';
import { forwardRef } from 'react';


/**
 * @description         Create a tag with text [FAILURE|PENDING|DONE|IN-PROGRESS] in a specific  format
 * @param {object}      classes - Style to pun on tag
 * @param {string}      text - Text to display on the tag
 * @returns {JSX}       Tag with a style according to the text sent
 */
export const StatusTagAtom = forwardRef(({ classes, text }, ref) => {
    const textUp = text.toUpperCase();
    return (
        <Box
            data-testid='StatusTagAtomTestId'
            ref={ref}
            className={
                textUp === STATE_REQUEST_COMPLETE
                    ? classes.complete
                    : textUp === STATE_REQUEST_INPROGRESS
                        ? classes.inproccess
                        : textUp === STATE_REQUEST_PENDING
                            ? classes.pending
                            : classes.failure
            }
            flexWrap='nowrap'
            alignItems='center'
            textAlign='center'
            style={{ width: 100 }}
        >
            <Typography variant={'body1'}>
                {textUp}
            </Typography>
        </Box>
    )
});

StatusTagAtom.propTypes = {
    classes: PropTypes.object.isRequired,
    text: PropTypes.string.isRequired,
}

StatusTagAtom.defaultProps = {
    text: '',
}

export default StatusTagAtom;
