import { IconButton, Tooltip, Typography } from '@material-ui/core';
import GetAppIcon from '@material-ui/icons/GetApp';
import PropTypes from 'prop-types';
import { forwardRef } from 'react';
import { useDownloadButton } from './hooks/useDownloadButton';

/**
 * @description         Create a button to download the resultDownloadRelativeUrl only if status are equals to complete
 * @param {string}      baseurlaf - Base of URL to download the file
 * @param {string}      enableEqualToStatus - Text to compare the status to enable or disable the button
 * @param {string}      resultDownloadRelativeUrl - Part of URL to download the file
 * @param {string}      status - Text status of enable or disable the button
 * @returns {JSX}       Download button to get the information from request
 */
export const DownloadButtonAtom = forwardRef(({
    baseurlaf,
    enableEqualToStatus,
    resultDownloadRelativeUrl,
    status,
}, ref) => {
    const { handleDownload } = useDownloadButton({ baseurlaf, resultDownloadRelativeUrl });
    return (
        <Tooltip
            title={
                <Typography variant={'body1'}>
                    {'Download Request files and Results Report'}
                </Typography>
            }
        >
            <span>
                <IconButton
                    data-testid='DownloadButtonAtomTestId'
                    ref={ref}
                    onClick={handleDownload}
                    disabled={
                        status === enableEqualToStatus ? false : true
                    }
                >
                    <GetAppIcon />
                </IconButton>
            </span>
        </Tooltip>
    )
});

DownloadButtonAtom.propTypes = {
    baseurlaf: PropTypes.string.isRequired,
    enableEqualToStatus: PropTypes.string.isRequired,
    resultDownloadRelativeUrl: PropTypes.string.isRequired,
    status: PropTypes.string.isRequired,
}

DownloadButtonAtom.defaultProps = {
    resultDownloadRelativeUrl: '',
    status: '',
}

export default DownloadButtonAtom;