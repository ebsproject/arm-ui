import PropTypes from 'prop-types';

/**
 * Function to download a file from server
 * @param {object} param0                           - Parameter to call the function
 * @param {string} param0.baseurlaf                 - URL base to get the file on server
 * @param {string} param0.resultDownloadRelativeUrl - URL to get a specific file
 * @returns {object} return0                        - Object to implement on component
 * @returns {function} return0.handleDownload       - Function to open the new URL on new windows
 * @returns {string} return0.urlDownload            - URL to found the object
 */
export const useDownloadButton = ({ baseurlaf, resultDownloadRelativeUrl }) => {
  const urlDownload = `${baseurlaf.slice(-1) !== '/' ? `${baseurlaf}/` : baseurlaf}${
    resultDownloadRelativeUrl.substring(0, 1) === '/'
      ? resultDownloadRelativeUrl.substring(1)
      : resultDownloadRelativeUrl
  }`;

  const handleDownload = () => {
    let windowObjectReference = window.open(urlDownload, '_blank');
  };

  return {
    handleDownload,
    urlDownload,
  };
};

useDownloadButton.propTypes = {
  baseurlaf: PropTypes.string.isRequired,
  resultDownloadRelativeUrl: PropTypes.string.isRequired,
};
