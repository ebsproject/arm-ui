import { unmountComponentAtNode } from 'react-dom';
import DownloadButtonAtom from './DownloadButton';
import * as hooks from './hooks/useDownloadButton';
import { render, cleanup, screen, fireEvent } from '@testing-library/react';
import '@testing-library/dom';
import '@testing-library/jest-dom/extend-expect';

let container = null;
beforeEach(() => {
  container = document.createElement('div');
  document.body.appendChild(container);
});
afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
  jest.clearAllMocks();
});

let props = {
  status: 'complete',
  enableEqualToStatus: 'complete',
  resultDownloadRelativeUrl: '/download.xmls',
  baseurlaf: 'localhost',
};
const urlValid = 'http://localhost/v1/file/download.jpg';
const id = 'DownloadButtonAtomTestId';
describe('Mount component DownloadButton', () => {
  it('Mount and search by testId', () => {
    render(<DownloadButtonAtom {...props}> </DownloadButtonAtom>, container);
    expect(screen.getByTestId(id)).toBeInTheDocument();
  });
});
describe('Search properties on DownloadButton', () => {
  it('Search button enable', () => {
    render(<DownloadButtonAtom {...props}> </DownloadButtonAtom>, container);
    const button = screen.getByTestId(id);
    expect(button).not.toBeDisabled();
  });

  it('Search button disable', () => {
    props.enableEqualToStatus = 'NotComplete';
    render(<DownloadButtonAtom {...props}> </DownloadButtonAtom>, container);
    const button = screen.getByTestId(id);
    expect(button).toBeDisabled();
  });

  it('Click on button', () => {
    // setup
    const mockHandleDownload = jest.fn();
    props.enableEqualToStatus = 'complete';
    props.onclick = mockHandleDownload;
    const spy = jest
      .spyOn(hooks, 'useDownloadButton')
      .mockImplementation(() => ({ handleDownload: mockHandleDownload }));
    // test
    render(<DownloadButtonAtom {...props}> </DownloadButtonAtom>, container);
    const button = screen.getByTestId(id);
    fireEvent.click(button);

    expect(button).not.toBeDisabled();
    expect(mockHandleDownload).toHaveBeenCalled();
    // Cleanup
    spy.mockRestore();
  });
});

describe('Test window open event', () => {
  it('Test that url parameter is valid url ', () => {
    //setup
    const mockedOpen = jest.fn((url) => url);
    const originalOpen = window.open;
    window.open = mockedOpen;
    //test
    const { handleDownload } = hooks.useDownloadButton({
      baseurlaf: 'http://localhost/v1',
      resultDownloadRelativeUrl: '/file/download.jpg',
    });
    handleDownload();

    expect(mockedOpen).toBeCalled();
    expect(mockedOpen.mock.calls[0][0]).toBe(urlValid);
    expect(mockedOpen.mock.results[0].value).toBe(urlValid);

    // Cleanup
    window.open = originalOpen;
  });
});

describe('Test use download hook', () => {
  it('URL base have not on finish slash / ', () => {
    const { urlDownload } = hooks.useDownloadButton({
      baseurlaf: 'http://localhost/v1',
      resultDownloadRelativeUrl: '/file/download.jpg',
    });
    expect(urlDownload).toBe(urlValid);
  });

  it('URL base had on finish slash / ', () => {
    const { urlDownload } = hooks.useDownloadButton({
      baseurlaf: 'http://localhost/v1/',
      resultDownloadRelativeUrl: '/file/download.jpg',
    });
    expect(urlDownload).toBe(urlValid);
  });

  it('URL download had on finish slash / ', () => {
    const { urlDownload } = hooks.useDownloadButton({
      baseurlaf: 'http://localhost/v1/',
      resultDownloadRelativeUrl: '/file/download.jpg',
    });
    expect(urlDownload).toBe(urlValid);
  });

  it('URL download have not on finish slash / ', () => {
    const { urlDownload } = hooks.useDownloadButton({
      baseurlaf: 'http://localhost/v1/',
      resultDownloadRelativeUrl: 'file/download.jpg',
    });
    expect(urlDownload).toBe(urlValid);
  });

  it('URL download and URL base have not on finish slash / ', () => {
    const { urlDownload } = hooks.useDownloadButton({
      baseurlaf: 'http://localhost/v1',
      resultDownloadRelativeUrl: 'file/download.jpg',
    });
    expect(urlDownload).toBe(urlValid);
  });
});
