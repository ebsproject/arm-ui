import React from 'react';
import ReactDOM, { unmountComponentAtNode } from 'react-dom';
// Component to be Test
import NoticeButton from './noticebutton';
// Test Library
import { render, cleanup, screen } from '@testing-library/react';
import '@testing-library/dom';
import '@testing-library/jest-dom/extend-expect';
import { act } from 'react-dom/test-utils';

let container = null;
beforeEach(() => {
  container = document.createElement('div');
  document.body.appendChild(container);
});
afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

const timerAPI = {
  clearInterval,
  clearTimeout,
  setInterval,
  setTimeout,
};
if (typeof setImmediate === 'function') {
  timerAPI.setImmediate = setImmediate;
}
if (typeof clearImmediate === 'function') {
  timerAPI.clearImmediate = clearImmediate;
}
// Props to send component to be rendered
const props = {
  helptext: '',
  color: 'primary',
  functiondata: jest.fn(),
  requestorid: '1',
  handleRefresh: jest.fn(),
  classes: {
    failure: '',
    complete: '',
    inproccess: '',
    pending: '',
  },
  staterequest: {
    failure: 'faiuler',
    complete: 'complete',
    inprogress: 'inprogess',
    pending: 'pending',
  },
  seconds: 1,
  localstorageid: 'test',
};

test('Report name', async () => {
  const div = document.createElement('div');
  await act(async () => {
    ReactDOM.render(<NoticeButton {...props}></NoticeButton>, div);
  });
});

describe('Mount component Notice Button', () => {
  it('Must be search by test Id', async () => {
    await act(async () => {
      render(<NoticeButton {...props}></NoticeButton>, container);
    });
    expect(screen.getByTestId('ButtonIconToolbarTestId')).toBeInTheDocument();
  });
});
