import React, { Fragment, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS
import {
  Badge,
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  IconButton,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Tooltip,
  Typography,
} from '@material-ui/core';
import MailIcon from '@material-ui/icons/Mail';
import ButtonIconToolbarAtom from 'components/ui/atoms/buttons/ButtonIconToolbar';

/**
 * @description         - Button to display on the toolbar in the main grid with functionality to inject information about the request and status into the storage location
 * @param {ref}         ref - Rreference made by React.forward
 * @param {object}      classes - Classes to put spesific style with makestyle
 * @param {string}      color - Color to aplicate to the button
 * @param {func}        functiondata - Function to search the request that was created
 * @param {func}        handleRefresh - Fuction to refresh the grid with new data
 * @param {string}      helptext Text to display into the button
 * @param {string}      localstorageid - ID to save the variable into the local storage
 * @param {string}      requestorid - Requestor Id to using to search information
 * @param {number}      seconds - Time to launch the service to get the new request
 * @param {object}      staterequest - Information about the color of the bages column
 * @property {number}   notification - Flag to display the number of notification
 * @property {array}    notificationData - Notificacion to display
 * @property {bool}     openDialog - Flag to open the list of history
 * @property {number}   time - Time to launche the interval to search changes into request
 * @property {string}   data-testid - Id to use inside noticebutton.test.js file.
 * @property {func}     handleNotifice - Function to get the notification of new request
 * @property {func}     handleDialogClose - Dialogol componet is closed and NotificationData is saved into local storage
 * @property {func}     handleNotificationClear - NotificationData is cleaned  and saved into local storage
 * @property {func}     clearNotification - NotificationData is saved into local storage
 * @property {func}     useEffect - Mount componet to callback the data every 2 minute
 * @returns {JSX}       Button to display notification when request change
 */
const NoticeButtonAtom = React.forwardRef(({
  classes,
  color,
  functiondata,
  handleRefresh,
  helptext,
  localstorageid,
  requestorid,
  seconds,
  staterequest,
}, ref) => {

  const [notification, setNotification] = useState(0);
  const [notificationData, setNotificationData] = useState([]);
  const [openDialog, setOpenDialog] = useState(false);
  const time = 1000 * seconds;

  const handleNotifice = () => {
    setOpenDialog(true);
  };
  const handleDialogClose = () => {
    setOpenDialog(false);
    clearNotification();
  };
  const handleNotificationClear = () => {
    setOpenDialog(false);
    clearNotification();
    handleRefresh();
  };
  const clearNotification = () => {
    if (notificationData.length > 0) {
      const localData = JSON.parse(window.localStorage.getItem(localstorageid));
      const newLocalData = [...new Set([...localData, ...notificationData])];
      window.localStorage.setItem(localstorageid, JSON.stringify(newLocalData));
      setNotificationData([]);
      setNotification(0);
    }
  };

  useEffect(() => {
    const interval = setInterval(() => {
      functiondata({
        requestorId: requestorid,
        crop: '',
        organization: '',
        status: '',
        pageSize: 10,
        page: 0,
      }).then((res) => {
        if (res.status === 200) {
          const serverData = res.data.map((data) =>
            `${data.requestId}|${data.status}`.toString(),
          );
          const localData = JSON.parse(window.localStorage.getItem(localstorageid));
          if (localData !== undefined && localData !== null) {
            const dataIsNew = serverData.filter((data) => {
              return localData.indexOf(data) == -1;
            });
            if (
              dataIsNew !== null &&
              dataIsNew !== undefined &&
              dataIsNew.length > 0
            ) {
              setNotificationData(dataIsNew);
              setNotification(dataIsNew.length);
            }
          } else {
            window.localStorage.setItem(localstorageid, JSON.stringify(serverData));
          }
        }
      });
    }, time);
    return () => {
      clearInterval(interval);
    };
  }, [
    setNotification,
    notification,
    time,
    requestorid,
    functiondata,
    setNotificationData,
    localstorageid,
  ]);

  return (
    <Fragment>
      <ButtonIconToolbarAtom
        functiononclick={handleNotifice}
        helptext={helptext}
        color={color}
        icon={<Badge badgeContent={notification} classes={{
          badge: classes.customBadge
        }} color='primary'>
          <MailIcon className="fill-current text-white" />
        </Badge>}
        id={`${requestorid}_NotificacionButtonID`}
      >
      </ButtonIconToolbarAtom>
      <Dialog
        maxWidth='xl'
        onClose={handleDialogClose}
        aria-labelledby='notification-dialog'
        open={openDialog}
      >
        <DialogContent dividers>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell width={100}>Status</TableCell>
                <TableCell width={150}>Request ID</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {notificationData.map((data, index) => {
                if (data !== undefined && data !== null && data !== '') {
                  const datas = data.split('|');
                  const key = `${index}-notification`;
                  let color = classes.failure;
                  if (datas[1] === staterequest.complete) color = classes.complete;
                  if (datas[1] === staterequest.inprogress)
                    color = classes.inproccess;
                  if (datas[1] === staterequest.pending) color = classes.pending;
                  return (
                    <TableRow hover role='button' key={key}>
                      <TableCell>
                        <Box
                          className={color}
                          alignItems='center'
                          textAlign='center'
                        >
                          {`${datas[1]}`.toString()}
                        </Box>
                      </TableCell>
                      <TableCell>{datas[0]}</TableCell>
                    </TableRow>
                  );
                }
              })}
            </TableBody>
          </Table>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleNotificationClear}>Clear</Button>
        </DialogActions>
      </Dialog>
    </Fragment>
  );
});
// Type and required properties
NoticeButtonAtom.propTypes = {
  classes: PropTypes.object.isRequired,
  color: PropTypes.string.isRequired,
  functiondata: PropTypes.func.isRequired,
  handleRefresh: PropTypes.func.isRequired,
  helptext: PropTypes.string.isRequired,
  localstorageid: PropTypes.string.isRequired,
  requestorid: PropTypes.string.isRequired,
  seconds: PropTypes.number.isRequired,
  staterequest: PropTypes.object.isRequired,
};
// Default properties
NoticeButtonAtom.defaultProps = {};

export default NoticeButtonAtom;
