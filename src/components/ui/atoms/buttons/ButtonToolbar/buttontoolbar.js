import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS
import { Button, Tooltip, Typography } from '@material-ui/core';

//MAIN FUNCTION
/**
 * @description         Create a button with a text when mouse is over it
 * @param {string}      classbutton - Object with spesific style with makestyle
 * @param {func}        functiononclick - Parent function launched when the user click in the component
 * @param {string}      helptext - Text to display when mouse is over it
 * @param {boolean}     isenable - Disable or enable the button
 * @param {string}      title - Text to display into the button component
 * @param {ref}         ref - Reference made by React.forward
 * @property {string}   data-testid  - Id to use inside buttontoolbar.test.js file.
 * @returns {component} Button to display into grid
 */
const ButtonToolbarAtom = React.forwardRef(
  ({ functiononclick, helptext, isenable, title }, ref) => {
    //const { children, helptext, classbutton, functiononclick, title, ...rest } = props;

    return (
      <Tooltip
        title={
          <Fragment>
            <Typography variant={'body1'}>{helptext}</Typography>
          </Fragment>
        }
        data-testid={'ButtonToolbarTestId'}
        ref={ref}
      >
        <Button
          className={'bg-ebs-green-default hover:bg-ebs-green-900 border-l-8 text-white'}
          variant='contained'
          size='medium'
          onClick={functiononclick}
          disabled={!isenable}
        >
          {title}
        </Button>
      </Tooltip>
    );
  },
);
// Type and required properties
ButtonToolbarAtom.propTypes = {
  functiononclick: PropTypes.func.isRequired,
  helptext: PropTypes.string.isRequired,
  isenable: PropTypes.bool,
  title: PropTypes.string.isRequired,
};
// Default properties
ButtonToolbarAtom.defaultProps = {
  isenable: true,
};

export default ButtonToolbarAtom;
