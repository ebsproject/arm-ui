import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS AND ATOMS TO USE
import { FormControl, MenuItem, Select, Typography } from '@material-ui/core';

//MAIN FUNCTION
/**
 * @description           Dropdown list generic when data only have 1 item is automatic selected
 * @param {object}        classes - Classes to put specific style with makeStyle.
 * @param {func}          convertToItems - Convert data into new array with 2 attributes {value, label}.
 * @param {array}         data - Information to display in the dropdown list.
 * @param {string}        id - Id to pun in the first component.
 * @param {function}      onChange - Get 2 parameters {selectedOption, data} to get value when change dropdown list.
 * @param {string}        title - Display how labe next to dropdown list.
 * @param {object}        ref - reference made by React.forward.
 * @property {string}     data-testid - Id to use inside autoselect.test.js file.
 * @returns {JSX}         Dropdown list for select items.
 */
const AutoSelectMolecule = React.forwardRef(
  ({
    classes,
    convertToItems,
    data,
    id,
    onChange,
    title,
  }, ref) => {
    const items = data.map(convertToItems);
    const [selectedOption, setSelectedOption] = useState('');
    const isMountedFirst = useRef(false);

    const handleOnChange = (event) => {
      setSelectedOption(event.target.value);
      onChange({ selectedOption: event.target.value, data });
    };

    useEffect(() => {
      let isMounted = true;
      if (isMounted) {
        let newSelected = '';
        let change = 0;
        switch (items.length){
          case 0:
            if(selectedOption !== ''){
              newSelected = '';
              change = 1;
            }
            break;
          case 1:
            if(selectedOption === ''){
              newSelected = items[0].value;
              change = 2;
            }
            if(selectedOption !== '' && items.findIndex(element => element.value === selectedOption) === -1){
              newSelected = items[0].value;
              change = 3;
            }
            break;
          default:
            if(selectedOption !== '' && items.findIndex(element => element.value === selectedOption) === -1){
              newSelected = '';
              change = 4;
            }
            break;
        }
        if(change > 0){
          setSelectedOption(newSelected);
          onChange({ selectedOption: newSelected, data });
        }
      }
      return () => {
        isMounted = false;
      };
    }, [items]);

    useEffect(() => {
      isMountedFirst.current = true;
    }, [isMountedFirst])

    return (
      <div ref={ref} data-testid={'AutoSelectTestId'} id={id}>
        <FormControl variant='outlined' size='small' id={`${id}Form`}>
          <Typography variant='body1' id={`${id}Label`}>{title}</Typography>
          <Select
            value={selectedOption}
            onChange={handleOnChange}
            className={classes.select}
            id={`${id}Select`}
          >
            {items.map((item) => {
              return (
                <MenuItem value={item.value} key={item.value}>
                  {item.label}
                </MenuItem>
              );
            })}
          </Select>
        </FormControl>
      </div>
    );
  },
);
// Type and required properties
AutoSelectMolecule.propTypes = {
  classes: PropTypes.object.isRequired,
  convertToItems: PropTypes.func.isRequired,
  data: PropTypes.array.isRequired,
  id: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
};
// Default properties
AutoSelectMolecule.defaultProps = {};

export default AutoSelectMolecule;
