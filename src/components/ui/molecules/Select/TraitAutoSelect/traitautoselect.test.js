import React from 'react';
import ReactDOM from 'react-dom';
// Component to be Test
import TraitAutoSelect from './traitautoselect';
// Test Library
import { render, cleanup } from '@testing-library/react';
import '@testing-library/dom';
import '@testing-library/jest-dom/extend-expect';
import * as reactRedux from 'react-redux';

afterEach(cleanup);
// Props to send component to be rendered
const props = {
  classes:  { formSelect: 'formSelect' },
  convertToItems: jest.fn(),
  data: [],
  onChange: jest.fn(),
  selectAllLabel: '',
  title: '',
  previoulyValue: [],
};
// mock to use dispatch
const mockDispatch = jest.fn();
jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  useDispatch: () => mockDispatch,
}));
const useSelectorMock = jest.spyOn(reactRedux, 'useSelector');
const useDispatchMock = jest.spyOn(reactRedux, 'useDispatch');

test('Report name', () => {
  const div = document.createElement('div');
  ReactDOM.render(<TraitAutoSelect {...props}></TraitAutoSelect>, div);
});

test('Render correctly', () => {
  const { getByTestId } = render(<TraitAutoSelect {...props}></TraitAutoSelect>);
  expect(getByTestId('TraitAutoSelectTestId')).toBeInTheDocument();
});
