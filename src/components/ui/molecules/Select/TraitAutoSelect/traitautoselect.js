import React, { useEffect, useReducer, useState } from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS AND ATOMS TO USE
import { FormControl, Grid, TextField, Typography } from '@material-ui/core';
import Autocomplete, { createFilterOptions } from '@material-ui/lab/Autocomplete';
import { makeStyles } from '@material-ui/core/styles';
import { useDispatch } from 'react-redux';
import { traitReducer } from './reducer/traitReducer';
import { doAddSelected, doDropdowNewData, doRemoveSelected, doReportChanges } from './reducer/traitAction';

//MAIN FUNCTION
/**
 * @description             Droplist to multiple select items
 * @param {classes}         classes - Classes to put specific style with makeStyle.
 * @param {function}        convertToItems - function to map data an convert into {value, label}
 * @param {array}           data - Data to selected by the user
 * @param {string}          id - ID
 * @param {function}        onChange - Save selected data into parent
 * @param {string}          selectAllLabel - Title to put in option select all items
 * @param {string}          title - Title to display into the component
 * @param  {string}         ref - Reference made by React.forward
 * @property {string}       data-testid Id to use inside traitautoselect.test.js file.
 */
const TraitAutoSelectMolecule = React.forwardRef(
  (
    {
      classes,
      convertToItems,
      data,
      id,
      onChange,
      selectAllLabel,
      title,
    },
    ref,
  ) => {
    const STATE_INITIAL = {
      isChangeBeginning: false,
      items: data.map(convertToItems),
      selectedOptions: [],
    }
    const [state, dispatch] = useReducer(traitReducer, STATE_INITIAL);
    const { items, selectedOptions, isChangeBeginning } = state;

    const getOptionLable = (value) => value.label;
    const getOptionSelected = (option, value) => option.value === value.value;


    const allSelected = items.length === selectedOptions.length;
    const handleToggleOption = (selectedOptions) =>
      dispatch(doAddSelected({ selectedOptions }));
    const handleClearOptions = () => dispatch(doRemoveSelected());

    const handleSelectAll = (isSelected) => {
      if (isSelected) {
        dispatch(doAddSelected({ selectedOptions: items }));
      } else {
        handleClearOptions();
      }
    };
    const handleToggleSelectAll = () => {
      handleSelectAll && handleSelectAll(!allSelected);
    };
    const filter = createFilterOptions();

    const handleChange = (event, selectedOptions, reason) => {
      if (reason === 'select-option' || reason === 'remove-option') {
        if (selectedOptions.find((option) => option.value === 'select-all')) {
          handleToggleSelectAll();
          let result = [];
          result = items.filter((el) => el.value !== 'select-all');
          return onChange({ selectedOptions: result, data });
        } else {
          handleToggleOption && handleToggleOption(selectedOptions);
          return onChange({
            selectedOptions,
            data,
          });
        }
      } else if (reason === 'clear') {
        handleClearOptions && handleClearOptions();
        return onChange({ selectedOptions: [], data });
      }
    };

    useEffect(() => {
      let isMounted = true;
      if (isMounted) {
        dispatch(doDropdowNewData({ items: data.map(convertToItems) }));
      }
      return () => {
        isMounted = false;
      };
    }, [data]);

    useEffect(() => {
      let isMounted = true;
      if (isMounted && isChangeBeginning) {
        onChange({ selectedOptions, data });
        dispatch(doReportChanges());
      }
      return () => {
        isMounted = false
      }
    }, [isChangeBeginning])


    return (
      <div
        id={`${id}Div`}
        data-testid={'TraitAutoSelectTestId'}
      >
        <FormControl variant='outlined' size='small' id={`${id}Form`}>
          <Typography variant='body1' id={`${id}Label`}>{title}</Typography>
          <Autocomplete
            className={classes.select}
            filterOptions={(options, params) => {
              const filtered = filter(options, params);
              return [{ label: selectAllLabel, value: 'select-all' }, ...filtered];
            }}
            filterSelectedOptions
            getOptionLabel={getOptionLable}
            getOptionSelected={getOptionSelected}
            id={`${id}Autocomplete`}
            multiple
            onChange={handleChange}
            options={items}
            renderInput={(params) => (
              <TextField {...params} variant='outlined' placeholder='Traits' />
            )}
            size='small'
            value={selectedOptions}
          />
        </FormControl>
      </div>
    );
  },
);
// Type and required properties
TraitAutoSelectMolecule.propTypes = {
  classes: PropTypes.object.isRequired,
  convertToItems: PropTypes.func.isRequired,
  data: PropTypes.array.isRequired,
  onChange: PropTypes.func.isRequired,
  selectAllLabel: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
};
// Default properties
TraitAutoSelectMolecule.defaultProps = {
  title: 'Dropdown list',
  selectAllLabel: 'Select all',
};

export default TraitAutoSelectMolecule;
