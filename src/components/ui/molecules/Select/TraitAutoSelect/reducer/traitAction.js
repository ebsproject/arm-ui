import PropTypes from 'prop-types';

export const doDropdowNewData = ({items}) =>({
    type: 'dropdown/newData',
    payload: {items}
})
export const doAddSelected = ({selectedOptions}) => ({
    type: 'add/selected',
    payload: {selectedOptions}
})
export const doRemoveSelected = () => ({
    type: 'remove/selected',
    payload: {},
})
export const doReportChanges = () => ({
    type: 'report/changes',
    payload: {},
})

doDropdowNewData.PropTypes = {
    data: PropTypes.array.isRequired,
}
doAddSelected.PropTypes = {
    selectedOptions: PropTypes.array.isRequired,
}
