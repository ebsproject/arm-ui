export const traitReducer = (state, action) => {

    const {
        isChangeBeginning,
        items,
        selectedOptions,
    } = state
    switch (action.type) {
        case 'dropdown/newData':
            let isChangeItems = false;
            let newSelectedOptions = selectedOptions;
            let newIsChangeBeginning = isChangeBeginning;
            const {items: newItems} = action.payload;
            if (newItems.length === 0) {
                newSelectedOptions = []
            }
            if (selectedOptions.length > 0) {
                selectedOptions.forEach(selected => {
                    if (newItems.findIndex(item => item.value === selected.value) === -1) {
                        isChangeItems = true;
                    }
                })
                if (isChangeItems) {
                    newSelectedOptions = [];
                }
                if (!isChangeItems && newSelectedOptions.length > 0) {
                    newIsChangeBeginning = true;
                }
            }
            if (newItems.length === 1 && selectedOptions.length === 0) {
                newSelectedOptions = newItems;
                newIsChangeBeginning = true;
            }
            return {
                ...state,
                items: newItems,
                selectedOptions: newSelectedOptions,
                isChangeBeginning: newIsChangeBeginning,
            }
        case 'add/selected':
            return {
                ...state,
                selectedOptions: action.payload.selectedOptions,
            }
        case 'remove/selected':
            return {
                ...state,
                selectedOptions: [],
            }
        case 'report/changes':
            return {
                ...state,
                isChangeBeginning: false,
            }
        default:
            return state;
    }
}