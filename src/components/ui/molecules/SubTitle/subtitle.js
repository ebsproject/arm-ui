import React from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS AND ATOMS TO USE
import { Grid, Typography } from '@material-ui/core'
import { Alert } from '@material-ui/lab'

//MAIN FUNCTION
/**
 * @description           Navigation bar with subtitle in the organism
 * @param {string}        titleText - Text to put into subtitle
 * @param {string}        severity - Severity to put icon in textbox to help user
 * @param {string}        message - Text to put into textbox to help user
 * @param {string}        messageerror - Text to put into textbox to help user and automatic put the severity in error
 * @property {string}     messageDisplayed - Message to display in box to help user
 * @property {string}     severityDisplayed - Put the severity into a message in box to help user
 * @property {string}     data-testid - Id to use inside subtitle.test.js file.
 * @returns {JSX}         Sub title with message
 */
const SubTitleMolecule = React.forwardRef((props, ref) => {
  const { children, titleText, message, messageerror, severity, ...rest } = props

  let messageDisplayed = message
  let severityDisplayed = severity

  if (messageerror !== '') {
    messageDisplayed = messageerror
    severityDisplayed = 'error'
  }

  return (
    <Grid
      container
      ref={ref}
      data-testid={'SubTitleTestId'}
      justifyContent={'flex-start'}
      spacing={3}
    >
      <Grid item>
        <Typography variant='h4'>{titleText}</Typography>
      </Grid>
      <Grid item>
        {messageDisplayed !== '' ? (
          <Alert severity={severityDisplayed}><Typography component={'span'} variant={'body1'}> {messageDisplayed} </Typography></Alert>
        ) : null}
      </Grid>
    </Grid>
  )
})
// Type and required properties
SubTitleMolecule.propTypes = {
  titleText: PropTypes.string.isRequired,
  severity: PropTypes.oneOf(['error','warning','info']),
  message: PropTypes.string,
  messageerror: PropTypes.string.isRequired,
}
// Default properties
SubTitleMolecule.defaultProps = {
  titleText: '',
  messageerror: '',
}

export default SubTitleMolecule
