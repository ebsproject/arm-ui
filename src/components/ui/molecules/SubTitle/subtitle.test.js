import React from 'react'
import ReactDOM from 'react-dom'
// Component to be Test
import SubTitle from './subtitle'
// Test Library
import { render, cleanup } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'

afterEach(cleanup)
// Props to send component to be rendered
const props = {
  titleText: '',
  message: '',
  messageerror: '',
  severity: 'error',
}

test('Report name', () => {
  const div = document.createElement('div')
  ReactDOM.render(<SubTitle {...props}></SubTitle>, div)
})

test('Render correctly', () => {
  const { getByTestId } = render(<SubTitle {...props}></SubTitle>)
  expect(getByTestId('SubTitleTestId')).toBeInTheDocument()
})
