  import TablePaginationGrid from './TablePaginationGrid';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

const props = {
  canNextPage: jest.fn(),
      canPreviousPage: jest.fn(),
      gotoPage: jest.fn(),
      nextPage: false,
      itemsCount: 1,
      pageDefault: 1,
      pageIndex: 1,
      pageOptions: [1,2],
      pageRowPerPage: [1,2],
      pageSize: 1,
      previousPage: false,
      setPageSize: jest.fn(),
      spacingIndex: 0,
}
test('TablePaginationGrid is in the DOM', () => {
  render(<TablePaginationGrid {...props}></TablePaginationGrid>)
  expect(screen.getByTestId('TablePaginationGridTestId')).toBeInTheDocument();
})
