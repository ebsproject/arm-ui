import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
// GLOBALIZATION COMPONENT
import { FormattedMessage } from 'react-intl';
import { Core } from '@ebs/styleguide';
// CORE COMPONENTS
//const { Box, Typography } = Core;
import {
  Box,
  FormControl,
  Grid,
  MenuItem,
  Select,
  Typography,
} from '@material-ui/core';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import LastPageIcon from '@material-ui/icons/LastPage';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import { IconButton } from '@material-ui/core';

//MAIN FUNCTION
/**
 * @description           Table pagination for Table Experiment
 * @param {function}      changePage - Parent function to call when change page
 * @param {function}      changeRowsPerPage - Parent function to call when change row per page
 * @param {integer}       page - Parent page
 * @param {integer}       pageDefault - Defaul page for start
 * @param {array}         pageRowPerPage - Array of page size for select by the user
 * @param {integer}       pageSize - Total of element by page in the parent
 * @param {integer}       pageOptions - Information from all item in the table
 * @param {string}        ref - Reference made by React.forward
 * @properties {string}   data-testid: Id to use inside TablePaginationGrid.test.js file.
 * @returns {JSX}         Box with pagination table component
 */

const TablePaginationGridMolecule = React.forwardRef(
  (
    {
      canNextPage,
      canPreviousPage,
      gotoPage,
      nextPage,
      itemsCount,
      pageDefault,
      pageIndex,
      pageOptions,
      pageRowPerPage,
      pageSize,
      previousPage,
      setPageSize,
      spacingIndex,
    },
    ref,
  ) => {

    const handleFirstPageButtonClick = (event) => {
      gotoPage(pageDefault);
    };

    const handleBackButtonClick = (event) => {
      previousPage();
    };

    const handleNextButtonClick = (event) => {
      nextPage();
    };

    const handleLastPageButtonClick = (event) => {
      gotoPage(pageOptions.length -1);
    }; 

    const handleChangePageIndex = ({ index }) => {
      gotoPage(index);
    };

    const handleChangeRowPerPage = (event) => {
      setPageSize(event.target.value);
    };

    return (
      <Box
        //border={2}
        //borderColor={'#00d1ff'}
        //borderRadius={15}
        data-testid={'TablePaginationGridTestId'}
        display={'flex'}
        justifyContent={'center'}
        padding={1}
        width={1}
      >
        <Grid
          alignItems='center'
          container
          direction='row'
          justifyContent='space-between'
          spacing={2}
          ref={ref}
        >
          <Grid item>
            <Grid container alignItems='center' direction='row'>
              <Grid item>
                <IconButton
                  aria-label='first page'
                  disabled={!canPreviousPage}
                  id={'TablePaginationActionFirstpageId'}
                  onClick={handleFirstPageButtonClick}
                  size='small'
                >
                  <FirstPageIcon />
                </IconButton>
                <IconButton
                  aria-label='previous page'
                  disabled={!canPreviousPage}
                  id={'TablePaginationActionPreviouspageId'}
                  onClick={handleBackButtonClick}
                  size='small'
                  style={{ borderRadius: 0 }}
                >
                  <KeyboardArrowLeft />
                </IconButton>
                {pageOptions.map((item) => {
                  const enable = item - pageIndex + spacingIndex >= 0 && item - pageIndex + spacingIndex <= spacingIndex * 2 ? true : false;
                  if (enable) {
                    return (
                      <IconButton
                        color={pageIndex === item ? 'secondary' : 'primary'}
                        id={`TablePaginationActionIndex${item}pageId`}
                        key={`${item}_indexPage`}
                        onClick={() => {
                          handleChangePageIndex({ index: item });
                        }}
                        size='small'
                        style={{ borderRadius: 0 }}
                      >
                        {pageIndex === item ? <strong>{item + 1}</strong> : item + 1}
                      </IconButton>)
                  } else {
                    return null;
                  }
                })}
                <IconButton
                  aria-label='next page'
                  disabled={!canNextPage}
                  id={'TablePaginationActionNextpageId'}
                  onClick={handleNextButtonClick}
                  size='small'
                  style={{ borderRadius: 0 }}
                >
                  <KeyboardArrowRight />
                </IconButton>
                <IconButton
                  aria-label='last page'
                  disabled={!canNextPage}
                  id={'TablePaginationActionLastpageId'}
                  onClick={handleLastPageButtonClick}
                  size='small'
                  style={{ borderRadius: 0 }}
                >
                  <LastPageIcon />
                </IconButton>
              </Grid>
              <Grid item>
                <Grid
                  container
                  spacing={1}
                  direction='row'
                  justifyContent='flex-start'
                  alignItems='center'
                >
                  <Grid item>
                    <Typography variant='body1'>Go to page: </Typography>
                  </Grid>
                  <Grid item>
                    <FormControl variant='outlined' size='small'>
                      <Select
                        id='tablePagination_indexPage'
                        value={pageIndex}
                        onChange={(event) =>
                          handleChangePageIndex({ index: event.target.value })
                        }
                      >
                        {pageOptions.map((item) => (
                          <MenuItem value={item} key={`page_index${item}`}>
                            {item + 1}
                          </MenuItem>
                        ))}
                      </Select>
                    </FormControl>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
          <Grid item>
            <Grid
              container
              direction='row'
              justifyContent='flex-end'
              alignItems='center'
              spacing={2}
            >
              <Grid item>
                <Typography variant='body1'>
                  Showing {<strong>{pageIndex * pageSize + 1}</strong>} - {' '}
                  {
                    <strong>
                      {Math.min(itemsCount, (pageIndex * pageSize ) +  pageSize)}
                    </strong>
                  }{' '}
                  of {<strong>{itemsCount}</strong>} items
                </Typography>
              </Grid>
              <Grid item>
                <Grid
                  container
                  spacing={1}
                  direction='row'
                  justifyContent='flex-start'
                  alignItems='center'
                >
                  <Grid item>
                    <Typography variant='body1'>Rows per page:</Typography>
                  </Grid>
                  <Grid item>
                    <FormControl variant='outlined' size='small'>
                      <Select
                        id='tablePagination_rowPerPage'
                        value={pageSize}
                        onChange={handleChangeRowPerPage}
                      >
                        {pageRowPerPage.map((item) => (
                          <MenuItem value={item} key={`row_per_page${item}`}>
                            {item}
                          </MenuItem>
                        ))}
                      </Select>
                    </FormControl>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Box>
    );
  },
);
// Type and required properties
TablePaginationGridMolecule.propTypes = {};
// Default properties
TablePaginationGridMolecule.defaultProps = {
  spacingIndex: 2,
};

export default TablePaginationGridMolecule;
