  import TableHead from './TableHead';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

const props = {
  allColumns: [],
  classes: {
    button: '',
  },
  filter: {
    title: '',
    helpText: '',
  },
  filterenablerow: {
    responseVariableOnlyFloat: { isActive: true, isEnable: true },
    sameExperiemnt: { isActive: true, isEnable: true }
  },
  functionresetfilterrow: jest.fn(),
  handelClearRowAndSearch: jest.fn(),
  setting: {
    title: '',
    helpText: '',
  },
  title: '',
  selectedRows: [],
  permissions: {
    desingExperiment:
    {
      label: ''
    }
  },
}
test('TableHead is in the DOM', () => {
  render(<TableHead {...props}></TableHead>)
  expect(screen.getByTestId('TableHeadTestId')).toBeInTheDocument();
})
