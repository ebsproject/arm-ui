import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
// GLOBALIZATION COMPONENT
import { FormattedMessage } from 'react-intl';
import { Core } from '@ebs/styleguide';

// CORE COMPONENTS

import { Box, Grid, Typography } from '@material-ui/core';
import DialogButtonAtom from 'components/ui/atoms/DialogButton';
import TableFilterButtonMolecule from 'components/ui/molecules/TableExperiment/TableFilterButton';
import { TYPE_DATA } from 'utils/config';
//MAIN FUNCTION
/**
 * @description         Head of table and button
 * @param {array}       allColumns -All columns to hiden or show
 * @param {object}      classes -Classes to put specific style with makeStyle. 
 * @param {array}       filter -Array to put filters
 * @param {object}      filterenablerow -Information about 2 filter 
 * @param {function}    functionresetfilterrow -Function to reste filter to original
 * @param {array}       permissions -Definition all permission of user
 * @param {array}       setting -Information about the buttons and other components
 * @param {array}       selectedRows -Array of row selecteds
 * @property {string}   data-testid: Id to use inside TableHead.test.js file.
 */
const TableHeadMolecule = React.forwardRef(
  (
    {
      allColumns,
      classes,
      filter,
      filterenablerow,
      functionresetfilterrow,
      permissions,
      selectedRows,
      setting,
    },
    ref,
  ) => {
    const occurrences = selectedRows.filter(selected => selected.typeData === TYPE_DATA.occurence);
    return (
      <Box
        data-testid={'TableHeadTestId'}
        display={'flex'}
        justifyContent={'center'}
        padding={1}
        ref={ref}
        width={1}
      >
        <Grid
          container
          direction='row'
          justifyContent='space-between'
          alignItems='stretch'
        >
          <Grid item xs={10} id={'TableExperimentTitleId'}>
            <Grid container direction='column' justifyContent='space-between' alignItems='flex-start'>
              <Grid item>
                <Typography style={{fontSize: 'medium'}} variant='body2'>
                  <b>{occurrences.length === 0 ? 'No' : occurrences.length}</b>
                  {` items selected.`}
                </Typography>
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={2} id={'TableExperimentMenuId'}>
            <Grid container direction='row-reverse' spacing={1}>
              <Grid item>
                <DialogButtonAtom
                  title={setting.title}
                  helptext={setting.helpText}
                  classes={classes}
                >
                  {allColumns.map((column) => {
                    if (!column.systemColumns)
                      return (
                        <div key={`checkHiddeColumn_${column.id}`}>
                          <label>
                            <input
                              type='checkbox'
                              {...column.getToggleHiddenProps()}
                              disabled={column.canVisible === false ? true : false}
                            />{' '}
                            {typeof (column.Header) === 'function' ? column.Header() : column.Header}
                          </label>
                        </div>
                      );
                  })}
                </DialogButtonAtom>
              </Grid>
              <Grid item>
                <TableFilterButtonMolecule
                  classes={classes}
                  helptext={filter.helpText}
                  title={filter.title}
                  permissions={permissions}
                  filterenablerow={filterenablerow}
                  functionresetfilterrow={functionresetfilterrow}
                ></TableFilterButtonMolecule>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Box>
    );
  },
);
// Type and required properties
TableHeadMolecule.propTypes = {};
// Default properties
TableHeadMolecule.defaultProps = {};

export default TableHeadMolecule;
