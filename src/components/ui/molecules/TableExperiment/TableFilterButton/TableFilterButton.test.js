import TableFilterButton from './TableFilterButton';
import React from 'react'
import ReactDOM from 'react-dom';
import { render, cleanup } from '@testing-library/react';
import '@testing-library/dom';
import '@testing-library/jest-dom/extend-expect';
   
afterEach(cleanup)

const props = {
  helptext: '', 
  title: '',
  permissions: {
    desingExperiment:
    {
      label: ''
    }
  },
  functionresetfilterrow: jest.fn(), 
  columns: [],
  classes: {
    class: 'class',
    button: '',
  },
  filterenablerow: {
    sameExperiemnt: {
      isActive: true,
      isEnable: false
    },
    responseVariableOnlyFloat: {
      isActive: true,
      isEnable: true,
    },
  },
};

test('TableFilterButton is in the DOM', () => {
  const div = document.createElement('div');
  ReactDOM.render(<TableFilterButton {...props}></TableFilterButton>, div);
  //expect(screen.getByTestId('TableFilterButtonTestId')).toBeInTheDocument();
})
