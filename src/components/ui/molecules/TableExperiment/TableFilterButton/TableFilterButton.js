import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
// GLOBALIZATION COMPONENT
import { Core } from '@ebs/styleguide';
// CORE COMPONENTS
const { Typography } = Core;
import ButtonIconToolbarAtom from '../../../atoms/buttons/ButtonIconToolbar';
import FilterListIcon from '@material-ui/icons/FilterList';
import {
  Box,
  Button,
  Dialog,
  DialogContent,
  DialogTitle,
  Grid,
  IconButton,
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import ItemCheckAtom from '../../../atoms/ItemCheck';
import ButtonToolbarAtom from '../../../atoms/buttons/ButtonToolbar';

//MAIN FUNCTION
/**
 * @description           Button to select filter to select the occurrences
 * @param {string}        title - title of the dialog component
 * @param {string}        helptext - Helper text to button to open dialog component
 * @param {object}        classes - Classes to put spesific style with makestyle
 * @param {object}        filterenablerow - Object to enable or disable selection row
 * @param {object}        filterenablerow.sameExperiemnt - Filter to select only one experiment
 * @param {boolean}       filterenablerow.sameExperiemnt.isActive - Activate or disable filter
 * @param {boolean}       filterenablerow.sameExperiemnt.isEnalbe - Enable component to activate or disable it
 * @param {object}        filterenablerow.responseVariableOnlyFloat - Filter to select only respose variable (trait) type float
 * @param {boolean}       filterenablerow.responseVariableOnlyFloat.isActive - Activate or disable filter
 * @param {boolean}       filterenablerow.responseVariableOnlyFloat.isEnalbe - Enable component to activate or disable it
 * @param {function}      functionresetfilterrow - Parent function to reset filter row
 * @param {string}        ref - reference made by React.forward
 * @property {string}     data-testid - Id to use inside TableFilterButton.test.js file.
 * @returns {componet}    Button to display filter component
 */
const TableFilterButtonMolecule = React.forwardRef(
  ({ 
    classes, 
    helptext, 
    title,
    permissions,
    filterenablerow, 
    functionresetfilterrow, 
  }, ref) => {
    const [open, setOpen] = useState(false);
    const [isEnableSave, setIsEnableSave] = useState(false);
    const [filter, setFilter] = useState([]);
    const [isFillFilter, setIsFillFilter] = useState(true);

    useEffect(() => {
      if (isFillFilter) {
        setFilter([
          {
            id: 'sameExperiemnt',
            previous: filterenablerow.sameExperiemnt.isActive,
            new: null,
          },
          {
            id: 'responseVariableOnlyFloat',
            previous: filterenablerow.responseVariableOnlyFloat.isActive,
            new: null,
          },
        ]);
        setIsFillFilter(false);
      }
    }, [filter, setFilter, filterenablerow,isFillFilter, setIsFillFilter]);

    const handleClose = () => {
      setOpen(false);
    };
    const handleButtonClick = () => {
      setOpen(true);
    };
    const handleButtonCheck = (field, isCheck) => {
      const checked =!isCheck;
      setFilter((item) => {
        const pos = item.findIndex((x) => x.id === field);
        if (pos > -1) item[pos].new = checked;
        return item;
      });
      let newFilter = filter.filter((item) => {
        if( item.id === field ) {
          if (item.previous !== checked ) return item;
        }else {
          if (item.new !== null && item.new !== item.previous) return item;
        }
      });
      setIsEnableSave(newFilter.length > 0 ? true : false);
    };
    const handleSave = () => {
      setIsFillFilter(true);
      setOpen(false);
      functionresetfilterrow({filter});
    };

    return (
      <Box component='div' ref={ref} data-testid={'TableFilterButtonTestId'} id={'TableFilterButtonId'}>
        <ButtonIconToolbarAtom
          helptext={helptext}
          functiononclick={handleButtonClick}
          color={'primary'}
          classbutton={classes.button}
          id={'TableFilterButtonFilterListId'}
          icon={<FilterListIcon  className="fill-current text-white" />}
        >
        </ButtonIconToolbarAtom>
        <Dialog
          onClose={handleClose}
          aria-labelledby='dialog-configuration-column'
          open={open}
          fullWidth={true}
          maxWidth={'sm'}
          id={'TableFilterButtonDialogId'}
        >
          <DialogTitle id='dialog-configuration-column-title'>
            <Grid
              container
              direction='row'
              justifyContent='space-between'
              alignItems='flex-start'
            >
              <Grid item>
                <Typography variant='h5'>{title}</Typography>
              </Grid>
              <Grid item>
                <IconButton
                  edge='end'
                  color='inherit'
                  onClick={handleClose}
                  aria-label='close'
                >
                  <CloseIcon />
                </IconButton>
              </Grid>
            </Grid>
          </DialogTitle>
          <DialogContent dividers>
            <ItemCheckAtom
              label={`${permissions?.desingExperiment.label}`}
              field={'sameExperiemnt'}
              ischeck={true}
              handlechangecolumns={() => {}}
              iscancheck={false}
              classes={classes}
            ></ItemCheckAtom>
            <ItemCheckAtom
              label={'Select only occurrences that have traits that are continuous variables'}
              field={'responseVariableOnlyFloat'}
              ischeck={filterenablerow.responseVariableOnlyFloat.isActive}
              handlechangecolumns={handleButtonCheck}
              iscancheck={filterenablerow.responseVariableOnlyFloat.isEnable}
              classes={classes}
            ></ItemCheckAtom>
            <Grid
              container
              direction='row-reverse'
              justifyContent='space-between'
              alignItems='flex-end'
            >
              <Grid item>
                <ButtonToolbarAtom
                  helptext={'Save new setting to filter'}
                  classbutton={classes.button}
                  functiononclick={handleSave}
                  title={'Save'}
                  isenable={isEnableSave}
                ></ButtonToolbarAtom>
              </Grid>
            </Grid>
          </DialogContent>
        </Dialog>
      </Box>
    );
  },
);
// Type and required properties
TableFilterButtonMolecule.propTypes = {
  title: PropTypes.string.isRequired,
  helptext: PropTypes.string.isRequired,
  classes: PropTypes.object.isRequired,
  filterenablerow: PropTypes.shape({
    sameExperiemnt: PropTypes.shape({
      isActive: PropTypes.bool.isRequired,
      isEnable: PropTypes.bool.isRequired,
    }).isRequired,
    responseVariableOnlyFloat: PropTypes.shape({
      isActive: PropTypes.bool.isRequired,
      isEnable: PropTypes.bool.isRequired,
    }).isRequired,
  }).isRequired,
  functionresetfilterrow:  PropTypes.func.isRequired,
};
// Default properties
TableFilterButtonMolecule.defaultProps = {};

export default TableFilterButtonMolecule;
