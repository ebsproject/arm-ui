import React from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS AND ATOMS TO USE
import { Grid, IconButton, Snackbar } from '@material-ui/core'
import ReportProblemIcon from '@material-ui/icons/ReportProblem'
import CloseIcon from '@material-ui/icons/Close'

//MAIN FUNCTION
/**
 * Display a alert message into the table
 * @param props: component properties
 * @param ref: reference made by React.forward
 */
const CellStatusMolecule = React.forwardRef((props, ref) => {
  const { children, isError, severity, message, ...rest } = props
  /**Flag to indicate if the alert is open */
  const [open, setOpen] = React.useState(false)

  /**Event when user click in close icon */
  const handelClick = () => {
    setOpen(true)
  }
  /**Function when the alert is closed */
  const handleClose = (event, reason) => {
    setOpen(false)
  }

  return (
    /**
     * @prop data-testid: Id to use inside cellstatus.test.js file.
     */
    <div ref={ref} data-testid={'CellStatusTestId'} id={'CellStatusId'}>
      {isError ? (
        <>
          <IconButton
            size='small'
            aria-label='close'
            color='inherit'
            onClick={handelClick}
            id={'CellStatusReportProblemId'}
          >
            <ReportProblemIcon fontSize='small' />
          </IconButton>
          <Snackbar
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'left',
            }}
            open={open}
            autoHideDuration={6000}
            onClose={handleClose}
            message={message}
            severity={severity}
            id={'CellStatusSnackBarId'}
            action={
              <React.Fragment>
                <IconButton
                  size='small'
                  aria-label='close'
                  color='inherit'
                  onClick={handleClose}
                >
                  <CloseIcon fontSize='small' />
                </IconButton>
              </React.Fragment>
            }
          />
        </>
      ) : null}
    </div>
  )
})
// Type and required properties
CellStatusMolecule.propTypes = {
  /**Flag to activate the alert */
  isError: PropTypes.bool.isRequired,
  /**Icon to display into the alert */
  severity: PropTypes.string.isRequired,
  /**Message to display into the alert */
  message: PropTypes.string.isRequired,
}
// Default properties
CellStatusMolecule.defaultProps = {
  isError: false,
  severity: 'warning',
  message: '',
}

export default CellStatusMolecule
