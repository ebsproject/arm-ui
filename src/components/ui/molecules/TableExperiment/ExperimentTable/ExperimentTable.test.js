import ExperimentTableMolecule from './ExperimentTable';
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'

afterEach(cleanup)
const props = {
  columns: [],
  classes: {button : ''},
  data: [],
  fetchData: jest.fn(),
  functionGetTrait: jest.fn(),
  itemsCount: 0,
  loading: true,
  pageCount: 1,
  pageIndex: 1,
  selectedRows: [],
  selectedRowsIds: [],
  setAddItemSelected: jest.fn(),
  rowStyle: {},
  //Head
  filter: {
    title: '',
    helpText: '',
  },
  filterenablerow: {
    responseVariableOnlyFloat: { isActive: true, isEnable: true },
    sameExperiemnt: { isActive: true, isEnable: true }
  },
  functionresetfilterrow: jest.fn(),
  handelClearRowAndSearch: jest.fn(),
  setting: {
    title: '',
    helpText: '',
  },
  title: '',
  permissions: {
    desingExperiment:
    {
      label: ''
    }
  },
}
test('Table is in the DOM', () => {
  render(<ExperimentTableMolecule {...props}></ExperimentTableMolecule>)
  expect(screen.getByTestId('TableTestId')).toBeInTheDocument();
})
