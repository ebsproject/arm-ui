import React, { useEffect, useMemo, useRef } from 'react';
import PropTypes from 'prop-types';
// GLOBALIZATION COMPONENT
//import { Core } from '@ebs/styleguide';
// CORE COMPONENTS
//const { Box, Typography } = Core;
import { FormattedMessage } from 'react-intl';
import { Grid, LinearProgress, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, TableSortLabel, TextField } from '@material-ui/core';
import { useTable, usePagination, useExpanded, useFilters, useBlockLayout, useSortBy } from 'react-table'
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import KeyboardArrowUp from '@material-ui/icons/KeyboardArrowUp';
import KeyboardArrowDown from '@material-ui/icons/KeyboardArrowDown';
import TablePaginationGridMolecule from '../TablePaginationGrid'
import IndeterminateCheckboxMolecule from '../IndeterminateCheckbox'
import { TYPE_DATA } from 'utils/config';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import RadioButtonUncheckedIcon from '@material-ui/icons/RadioButtonUnchecked';
import RadioButtonCheckedIcon from '@material-ui/icons/RadioButtonChecked';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import TableHeadMolecule from 'components/ui/molecules/TableExperiment/TableHead';
import CellStatusMolecule from 'components/ui/molecules/TableExperiment/CellStatus';


const PAGE_SIZE_DEFUALT = 10;
const PAGE_DEFUALT = 0;
const PAGE_ROW_PER_PAGE = [PAGE_SIZE_DEFUALT, 25, 50, 100];

function DefaultColumnFilter({
  column: { filterValue, preFilteredRows, setFilter },
}) {
  const debounceRef = useRef()

  const onQueryChanged = (event) => {
    if (debounceRef.current)
      clearTimeout(debounceRef)
    debounceRef.current = setTimeout(() => {
      setFilter(event.target.value || undefined);
    }, 1000)
  }
  return (
    <TextField
      onChange={(event) => onQueryChanged(event)}
      placeholder={``}
    />
  );
}

//MAIN FUNCTION
/**
 * @description         Display a table to get experiment
 * @param {object}      classes -Classes to put specific style with makeStyle. 
 * @param {array}       columns -Array of definition to columns
 * @param {array}       data -Array with all data to rows
 * @param {function}    fetchData -Function to get new data to table
 * @param {function}    functionGetTrait -Validate if exist traint in common
 * @param {number}      itemsCount -Total of element in the API
 * @param {boolean}     loading -Is loading data
 * @param {number}      pageCount -Total pages
 * @param {number}      pageIndex -Page now
 * @param {object}      rowStyle -Style for occurrences rows
 * @param {array}       selectedRows -Occurrences selecteds
 * @param {array}       selectedRowsIds -Id occurrences selecteds
 * @param {function}    setAddItemSelected -Function to sava selected rows
 * @param {array}       filter -Array to put filters
 * @param {object}      filterenablerow -Information about 2 filter 
 * @param {function}    functionresetfilterrow -Function to reste filter to original
 * @param {array}       permissions -Definition all permission of user
 * @param {array}       setting -Information about the buttons and other components
 * @property {string}   data-testid: Id to use inside Table.test.js file.
 */
const ExperimentTableMolecule = React.forwardRef(({
  classes,
  columns,
  data,
  fetchData,
  functionGetTrait,
  itemsCount,
  loading,
  pageCount: controlledPageCount,
  pageIndex: controlledPageIndex,
  rowStyle,
  selectedRows,
  selectedRowsIds,
  selectedIndeterminateRowsIds,
  setAddItemSelected,
  message,
  //Head
  filter,
  filterenablerow,
  functionresetfilterrow,
  permissions,
  setting,
}, ref) => {
  const defaultColumn = useMemo(
    () => ({
      Filter: DefaultColumnFilter,
    }),
    []
  );
  // Use the state and functions returned from useTable to build your UI
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    rows, // Instead of using 'rows', we'll use page,
    allColumns,
    // which has only the rows for the active page

    // The rest of these things are super handy, too ;)
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    setPageSize,
    state: { pageIndex, pageSize, filters },
  } = useTable(
    {
      columns,
      data,
      defaultColumn,
      initialState: {
        pageIndex: PAGE_DEFUALT,
        pageSize: PAGE_SIZE_DEFUALT,
        minWidth: 50,
        width: 120,
        maxWidth: 400,
        hiddenColumns: columns
          .filter(
            (column) => column.isVisible !== undefined && column.isVisible === false,
          )
          .map((column) => column.accessor),
      },
      manualPagination: true,
      autoResetSortBy: false,
      // manualSortBy: true,
      manualFilters: true,
      autoResetPage: false,
      pageCount: controlledPageCount,
    },
    useBlockLayout,
    useFilters,
    useSortBy,
    useExpanded,
    usePagination,
    (hooks) => {
      hooks.visibleColumns.push(columns => [
        {
          id: 'experimentIndex',
          Header: '#',
          Cell: ({ row, experimentIndex, pageIndex }) =>
          row.original.typeData === TYPE_DATA.experiment ? (
              <span>
                {experimentIndex+(pageIndex*10)}
              </span>
            ) : null,
          canVisible: false,
          systemColumns: true,
          minWidth: 15,
          maxWidth: 15,
          disableSortBy: true,
        },
        {
          id: 'actionCustom',
          Header: 'Action',
          Cell: ({ 
            row, 
            functionGetTrait,
            selectedRowsIds,
            selectedRows,
            rowsById,
           }) => {
            /**
             * @property {bool}  showLog -Hidden or display console log about the disabel or enable the check componet
             */
            const showLog = false;
            let isDisable = false;
            const checkedState = selectedRowsIds.includes(row.original['id']);
            const checkedIndeterminate = !checkedState && selectedIndeterminateRowsIds.includes(row.original['id'])  ? true : false;
            const parentRow = row.depth > 0 ? rowsById[row.id.split('.')[0]].original : null;

            // const getTrait = functionGetTrait({ row: row.original, selectedRows });
            const isIncludesEqualExperimentDesingType = selectedRows.findIndex(item => item.experimentDesignType === row.original.experimentDesignType) > -1 ? true : false;
            const isIncludeEqualExperimentDBId = selectedRows.findIndex(item => item.experimentDbId === row.original.experimentDbId) > -1 ? true : false;
            if (showLog)
              console.log(`${row.original.id}`, row.original.traitsFilter);
            //Rules for disable check when do not have  trait in filter
            if (!isDisable && !row.original.isTraitsFilter) {
              isDisable = true;
              if (showLog)
                console.log(`Rule-general !isTraitFilter`);
            }
            if (permissions.desingExperiment.multiValue) {
              // Disable if one experiment has diferent Experiment desing Type
              if (!isDisable && selectedRows.length > 0 && !isIncludesEqualExperimentDesingType) {
                isDisable = true;
                if (showLog)
                  console.log(`Rule-general selectedRows>0 && !isIncludesEqualExperimentDesingType`);
              }
            } else {
              if (!isDisable && selectedRows.length > 0 && !isIncludeEqualExperimentDBId) {
                isDisable = true;
                if (showLog)
                  console.log(`Rule-general selectedRows>0 && !isIncludeEqualExperimentDBId`);
              }
            }
            // if (row.original.typeData === TYPE_DATA.occurence) {
            //   // Check will disable if it don't have same trait that other selected
            //   if (!isDisable && !getTrait.isIntersection) {
            //     isDisable = true;
            //     if (showLog)
            //       console.log(`Rule-occurrence !isIntersection`);
            //   }
            // }
            // if (row.original.typeData === TYPE_DATA.experiment) {
            //   // Check will disable if it don't have same trait in all occurrences of it experiment
            //   if (!isDisable && !getTrait.isIntersection) {
            //     isDisable = true;
            //     if (showLog)
            //       console.log(`Rule-experiment !isIntersection`);
            //   }
            // }

            const icon =
              row.original.typeData === TYPE_DATA.experiment
                && !permissions.desingExperiment.multiValue ?
                <RadioButtonUncheckedIcon />
                : <CheckBoxOutlineBlankIcon />;
            const iconCheck = row.original.typeData === TYPE_DATA.experiment
              && !permissions.desingExperiment.multiValue ?
              <RadioButtonCheckedIcon />
              : <CheckBoxIcon />;
            return (
              <div {...row.getRowProps()}>
                <IndeterminateCheckboxMolecule
                  checkedIndeterminate={checkedIndeterminate}
                  checkedState={checkedState}
                  functionGetTrait={functionGetTrait}
                  icon={icon}
                  iconCheck={iconCheck}
                  id={row.original['id']}
                  isDisable={isDisable}
                  onChange={setAddItemSelected}
                  parentRow={parentRow}
                  permissions={permissions}
                  row={row.original}
                  selectedRows={selectedRows}
                />
              </div>
            )
          },
          canVisible: false,
          systemColumns: true,
          minWidth: 40,
          maxWidth: 40,
          disableSortBy: true,
        },
        {
          id: 'message',
          minWidth: 20,
          maxWidth: 20,
          Header: 'Message',
          Cell: ({ 
            row, 
            functionGetTrait,
            selectedRows, 
          }) => {
            /**
             * @property {bool}  showLog -Hidden or display console log about the disabel or enable the check componet
             */
            const showLog = false;
            let isError = false;
            let message = '';
            // const getTrait = functionGetTrait({ row: row.original, selectedRows });
            const isIncludesEqualExperimentDesingType = selectedRows.findIndex(item => item.experimentDesignType === row.original.experimentDesignType) > -1 ? true : false;
            const isIncludeEqualExperimentDBId = selectedRows.findIndex(item => item.experimentDbId === row.original.experimentDbId) > -1 ? true : false;

            if (showLog)
              console.log(`${row.original.id}`, row.original.traitsFilter);

            if (!isError && !row.original.isTraitsFilter) {
              //Message if not have taris filter
              isError = true;
              message = 'This experiment does not have any trait data collected for analysis.';
              if (showLog)
                console.log(`Rule-general !isTraitFilter`);
            }
            if (!isError && !row.original.isTraits) {
              //Message if not have taris
              isError = true;
              message = 'This experiment has not response variable (crop traits).';
              if (showLog)
                console.log(`Rule-general !isTrait`);
            }
            if (permissions.desingExperiment.multiValue) {
              if (!isError && selectedRows.length > 0 && !isIncludesEqualExperimentDesingType) {
                //Multi experiment - message if it not have same experiment desing
                isError = true;
                message = 'The experiments can be selected with the same experiment design';
                if (showLog)
                  console.log(`Rule-general selectedRows>0 !isIncludesEqualExperimentDesingType`);
              }
            } else {
              if (!isError && selectedRows.length > 0 && !isIncludeEqualExperimentDBId) {
                //Single experiment - disable if it not have same experiment
                isError = true;
                message = 'Only one experiment can be selected per request';
                if (showLog)
                  console.log(`Rule-general selectedRows>0 !isIncludeEqualExperimentDBId`);
              }
            }
            // if (row.original.typeData === TYPE_DATA.occurence) {
            //   if (!isError && !getTrait.isIntersection) {
            //     //Occurrence - message if trait have same that other selected
            //     isError = true;
            //     message = 'This occurrence has not Crop trait in common';
            //     if (showLog)
            //       console.log(`Rule-occurrence !isIntersection`);
            //   }
            // }
            // if (row.original.typeData === TYPE_DATA.experiment) {
            //   // Experiment - message if all occurrences don't have any trait in intersection
            //   if (!isError && !getTrait.isIntersection) {
            //     isError = true;
            //     message = 'All occurrences of experiment have not Crop trait in common';
            //     if (showLog)
            //       console.log(`Rule-experiment !isIntersection`);
            //   }
            // }
            return <CellStatusMolecule isError={isError} message={message} />;
          },
          canVisible: false,
          systemColumns: true,
        },
        {
          id: 'expander',
          Header: '',
          Cell: ({ row }) =>
            row.canExpand ? (
              <span
                {...row.getToggleRowExpandedProps({
                  style: {
                    paddingLeft: `${row.depth * 2}rem`,
                  },
                })}
              >
                {row.isExpanded ? <KeyboardArrowDown /> : <KeyboardArrowRight />}
              </span>
            ) : null,
          canVisible: false,
          systemColumns: true,
          minWidth: 25,
          maxWidth: 25,
          disableSortBy: true,
        },
        ...columns,
      ])
    }
  )

  useEffect(() => {
    let isMounted = true;
    isMounted && fetchData && fetchData({ limit: pageSize, page: pageIndex, searchParameters: filters });
    return () => {
      isMounted = false;
    }
  }, [fetchData, pageIndex, pageSize, filters]);
  let index = 0;
  //Style to row into experiment and occurrences
  const getRowPropsCustom = (row) => {
    return {
      style: {
        background:
          row.original.typeData === TYPE_DATA.experiment
            ? 'white'
            : rowStyle.rowOccurrence.backgroundColor,
      },
    };
  };
  return (
    <Grid
      container
      ref={ref}
      data-testid={'TableTestId'}
      id={'TableId'}
      direction={'column'}
      spacing={1}
    >
      <Grid item xs={12}>
        <TableHeadMolecule
          allColumns={allColumns}
          classes={classes}
          filter={filter}
          filterenablerow={filterenablerow}
          functionresetfilterrow={functionresetfilterrow}
          setting={setting}
          permissions={permissions}
          selectedRows={selectedRows}
        />
      </Grid>
      <Grid item xs={12}>
        <TableContainer id={'TableExperimentTableId'}>

          <Table {...getTableProps()} style={{ borderTop: '1px solid red' }}>
            <TableHead>
              {headerGroups.map(headerGroup => (
                <TableRow {...headerGroup.getHeaderGroupProps()}>
                  {headerGroup.headers.map(column => {
                    const { onClick: onClickSort } = column.getSortByToggleProps();
                    return (
                      <TableCell
                        {...column.getHeaderProps()}
                        size={'small'}
                        variant={'head'}
                      >
                        {column.render('Header')}
                        {column.canSort ? (
                          <TableSortLabel
                            active={column.isSorted}
                            direction={column.isSortedDesc ? 'desc' : 'asc'}
                            onClick={onClickSort}
                          >
                          </TableSortLabel>) : null}
                        <div>
                          {column.canFilter ? column.render('Filter') : null}
                        </div>
                      </TableCell>
                    )
                  })}
                </TableRow>
              ))}
            </TableHead>
            <TableBody {...getTableBodyProps()}>
              {!loading ?
                rows.map((row, i) => {
                  if(row.original.typeData === TYPE_DATA.experiment)
                    index++
                  prepareRow(row)
                  return (
                    <TableRow {...row.getRowProps(getRowPropsCustom(row))}>
                      {
                        row.cells.map(cell => {
                          return (
                            <TableCell {...cell.getCellProps()}>
                              {cell.render('Cell', {
                                experimentIndex: index,
                                functionGetTrait: functionGetTrait,
                                selectedRowsIds: selectedRowsIds,
                                selectedRows: selectedRows,
                                selectedIndeterminateRowsIds: selectedIndeterminateRowsIds,
                                pageIndex: controlledPageIndex,
                              })}
                            </TableCell>
                          )
                        })
                      }
                    </TableRow>
                  )
                }) : null
              }
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
      <Grid item xs={12}>
        {loading && <LinearProgress />}
      </Grid>
      <Grid item xs={12}>
        {message !== '' && <div>{message}</div>}
      </Grid>
      <Grid item id={'TableExperimentTablePaginationId'} xs={12}>
        <TablePaginationGridMolecule
          canPreviousPage={canPreviousPage}
          canNextPage={canNextPage}
          pageOptions={pageOptions}
          itemsCount={itemsCount}
          gotoPage={gotoPage}
          nextPage={nextPage}
          previousPage={previousPage}
          setPageSize={setPageSize}
          pageIndex={pageIndex}
          pageSize={pageSize}
          pageDefault={PAGE_DEFUALT}
          pageRowPerPage={PAGE_ROW_PER_PAGE}
        />
      </Grid>
    </Grid>
  )
})
// Type and required properties
ExperimentTableMolecule.propTypes = {}
// Default properties
ExperimentTableMolecule.defaultProps = {}

export default ExperimentTableMolecule
