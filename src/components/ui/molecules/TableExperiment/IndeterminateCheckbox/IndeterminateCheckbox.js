import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
// GLOBALIZATION COMPONENT
import { FormattedMessage } from 'react-intl';
import { TYPE_DATA } from 'utils/config';
import { Checkbox, Grid } from '@material-ui/core';

//import { Core } from '@ebs/styleguide';
// CORE COMPONENTS
//const { Box, Typography } = Core;

//MAIN FUNCTION
/**
 * @description             Check to selected the row
 * @param {boolean}         checkedState - Check or uncheck the component
 * @param {boolean}         checkedIndeterminate - If is indeterminate or not the component
 * @param {string}          id - ID to identify the row
 * @param {boolean}         isDisable - Disable the check
 * @param {func}            onChange - Function to save check
 * @param {object}          parentRow - Information of parent row
 * @param {object}          row - Information of row
 * @param {string}          ref - reference made by React.forward
 * @properties {string}     data-testid - Id to use inside IndeterminateCheckbox.test.js file.
 * @returns {JSX}           Check to selected the row
*/
const IndeterminateCheckboxMolecule = React.forwardRef(
  (
    {
      checkedIndeterminate,
      checkedState,
      functionGetTrait,
      icon,
      iconCheck,
      id,
      isDisable,
      onChange,
      parentRow,
      row,
      selectedRows,
      ...rest
    },
    ref,
  ) => {
    const [checked, setChecked] = useState(false)

    useEffect(() => {
      if (checkedState !== checked)
        setChecked(checkedState)
    }, [checkedState])

    const handleClick = () => {
      const checkedValidate = onChange({
        checked,
        functionGetTrait,
        id,
        parentRow,
        row,
        selectedRows,
      })
      setChecked(prev => (checkedValidate));

    }
    return (
      <Checkbox
        indeterminate={checkedIndeterminate}
        icon={icon}
        checkedIcon={iconCheck}
        data-testid={'IndeterminateCheckboxTestId'}
        disabled={isDisable}
        onChange={() => { handleClick() }}
        checked={checked}
      />
    )
  },
);

// Type and required properties
IndeterminateCheckboxMolecule.propTypes = {
  checkedState: PropTypes.bool.isRequired,
  checkedIndeterminate: PropTypes.bool.isRequired,
  id: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]).isRequired,
  isDisable: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired,
  parentRow: PropTypes.object,
  row: PropTypes.object.isRequired,
};
// Default properties
IndeterminateCheckboxMolecule.defaultProps = {};

export default IndeterminateCheckboxMolecule;
