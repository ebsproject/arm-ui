import IndeterminateCheckbox from './IndeterminateCheckbox';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
import { unmountComponentAtNode } from 'react-dom';
import { act } from 'react-dom/test-utils';

let container = null;
beforeEach(() => {
  container = document.createElement('div');
  document.body.appendChild(container);
});
afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

const props = {
  checkedState: false,
  id: '1',
  isDisable: false,
  onChange: jest.fn(),
  parentRow: {},
  row: {},
  checkedIndeterminate: true,
}
test('IndeterminateCheckbox is in the DOM', () => {
  render(<IndeterminateCheckbox {...props}></IndeterminateCheckbox>)
  expect(screen.getByTestId('IndeterminateCheckboxTestId')).toBeInTheDocument();
})

describe('Funcionality of IndeterminateCheckbox', ()=>{
    props.isDisable = true;
    it('Disable checked', async () => {
    await act(async () => {
      render(<IndeterminateCheckbox {...props}></IndeterminateCheckbox>, container);
    });
    const component  = screen.getByTestId('IndeterminateCheckboxTestId');
    expect(component).toBeInTheDocument();
  });
})

// describe('Add request tad - test', () => {
//   it('Must be search by test Id', async () => {
//     await act(async () => {
//       render(<AddRequestTab {...props}></AddRequestTab>, container);
//     });
//     expect(screen.getByTestId('AddRequestTabTestId')).toBeInTheDocument();
//   });
// });