  import TabPanel from './TabPanel';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

const props = {
  children: <div></div>, 
  step: 0,
  index: 0,
}
test('TabPanel is in the DOM', () => {
  render(<TabPanel {...props}></TabPanel>)
  expect(screen.getByTestId('TabPanelTestId')).toBeInTheDocument();
})
