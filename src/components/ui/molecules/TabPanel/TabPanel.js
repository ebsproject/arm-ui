import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
// GLOBALIZATION COMPONENT
import { FormattedMessage } from 'react-intl';
import { Core } from '@ebs/styleguide';
import { Box, Collapse } from '@material-ui/core';
// CORE COMPONENTS

//MAIN FUNCTION
/**
 * @description          Panel for step or tab
 * @param {number}      step - Step of the tab
 * @param {number}      index - Current step
 * @param {component}   children - Children componet to display in the tab
 * @param {string}      ref - Reference made by React.forward
 * @property {string}   data-testid - Id to use inside TabPanel.test.js file.
 * @returns {component} Return tab to hidden or show
 */
const TabPanelMolecule = React.forwardRef(({ children, step, index }, ref) => {
  const isIn = step === index;
  return (
    <Box ref={ref} data-testid={'TabPanelTestId'} id={'TabPanelId'}>
      <Collapse in={isIn} timeout='auto'>
        {children}
      </Collapse>
    </Box>
  );
});
// Type and required properties
TabPanelMolecule.propTypes = {
  step: PropTypes.number.isRequired,
  index: PropTypes.number.isRequired,
};
// Default properties
TabPanelMolecule.defaultProps = {};

export default TabPanelMolecule;
