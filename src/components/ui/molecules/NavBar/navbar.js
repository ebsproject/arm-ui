import React from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS AND ATOMS TO USE
import { AppBar, Grid, Typography } from '@material-ui/core'

//MAIN FUNCTION
/**
 * NavBar to display the title
 * @param props: component properties
 * @param ref: reference made by React.forward
 */
const NavBarMolecule = React.forwardRef((props, ref) => {
  const { children, title, ...rest } = props

  return (
    /* 
     @prop data-testid: Id to use inside navbar.test.js file.
     */
    <Grid container ref={ref} data-testid={'NavBarTestId'}>
      <Grid item xs={'auto'} sm={'auto'} md={'auto'} lg={'auto'} xl={'auto'}>
        <AppBar position='static'>
          <Typography variant='h5' color='inherit'>
            {title}
          </Typography>
        </AppBar>
      </Grid>
    </Grid>
  )
})
// Type and required properties
NavBarMolecule.propTypes = {
  /**Title to display */
  title: PropTypes.string.isRequired,
}
// Default properties
NavBarMolecule.defaultProps = {}

export default NavBarMolecule
