import React from 'react'
import ReactDOM from 'react-dom'
// Component to be Test
import NavBar from './navbar'
// Test Library
import { render, cleanup } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'

afterEach(cleanup)
// Props to send component to be rendered
const props = {
  title: 'title',
}

test('Report name', () => {
  const div = document.createElement('div')
  ReactDOM.render(<NavBar {...props}></NavBar>, div)
})

test('Render correctly', () => {
  const { getByTestId } = render(<NavBar {...props}></NavBar>)
  expect(getByTestId('NavBarTestId')).toBeInTheDocument()
})
