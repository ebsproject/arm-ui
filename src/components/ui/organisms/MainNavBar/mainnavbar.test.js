import React from 'react'
import ReactDOM from 'react-dom'
// Component to be Test
import MainNavBar from './mainnavbar'
// Test Library
import { render, cleanup } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'

afterEach(cleanup)
// Props to send component to be rendered
const props = {
  buttons: [
    {
      id: '',
      label: '',
      active: true,
      parameter: '',
    },
  ],
  classes: {
    class: 'class',
  },
}

test('Report name', () => {
  const div = document.createElement('div')
  ReactDOM.render(<MainNavBar {...props}></MainNavBar>, div)
})

test('Render correctly', () => {
  const { getByTestId } = render(<MainNavBar {...props}></MainNavBar>)
  expect(getByTestId('MainNavBarTestId')).toBeInTheDocument()
})
