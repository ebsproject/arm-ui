import React from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS AND MOLECULES TO USE
import { Grid } from '@material-ui/core'
import MainDataGridMolecule from 'components/ui/molecules/MainDataGrid'

//MAIN FUNCTION
/**
 * @description       Main component to contain the data of the request.
 * @param {object}    props - Children properties
 * @param {function}  props.functiondata - Function to get all request created by the user
 * @param {string}    props.title - Text to put into the title
 * @param {array}     props.columnsdata - Array of column into the table
 * @param {array}     props.buttons - Array of object to create a buttons
 * @param {object}    props.staterequest - Object with information about the status of each request [status]
 * @param {onject}    props.requestorprofile - Information of the user
 * @param {function}  props.requestidfunction - Search olny one request by ID
 * @param {object}    props.classes - Classes to put spesific style with makestyle
 * @param {function}  props.profileIdFunction - Function to search one profile user by id
 * @param {reference} ref - reference made by React.forward
 * @property {string} data-testid - Id to use inside maintable.test.js file.
 * @returns           return all component to create a paget to view all request create by the user
 */
const MainTableOrganism = React.forwardRef((props, ref) => {
  const {
    children,
    functiondata,
    title,
    columnsdata,
    buttons,
    staterequest,
    classes,
    requestorprofile,
    requestidfunction,
    profileidfunction,
    ...rest
  } = props

  return (
    <Grid container ref={ref} data-testid={'MainTableTestId'}>
      <Grid item xs={12}>
        <MainDataGridMolecule
          functiondata={functiondata}
          title={title}
          buttons={buttons}
          columns={columnsdata}
          classes={classes}
          staterequest={staterequest}
          requestorprofile={requestorprofile}
          requestidfunction={requestidfunction}
          profileidfunction={profileidfunction}
        />
      </Grid>
    </Grid>
  )
})
// Type and required properties
MainTableOrganism.propTypes = {
  functiondata: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  columnsdata: PropTypes.func.isRequired,
  buttons: PropTypes.array.isRequired,
  staterequest: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
  requestorprofile: PropTypes.object.isRequired,
  requestidfunction: PropTypes.func.isRequired,
  profileidfunction: PropTypes.func.isRequired,
}
// Default properties
MainTableOrganism.defaultProps = {}

export default MainTableOrganism
