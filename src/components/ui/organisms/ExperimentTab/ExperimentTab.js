import React, { useState, useRef, useCallback, useEffect, useReducer } from 'react';
import PropTypes from 'prop-types';
// GLOBALIZATION COMPONENT
import { FormattedMessage } from 'react-intl';
import { Core } from '@ebs/styleguide';
// CORE COMPONENTS AND MOLECULES TO USE
//const { Grid, Typography } = Core;
import { Grid } from '@material-ui/core';
import ExperimentTableMolecule from 'components/ui/molecules/TableExperiment/ExperimentTable';
import { TYPE_DATA } from 'utils/config';
import { experimentReducer } from './reducer/experimentReducer';
import { doFill, doLoading, doMessage, doResetFilterRow, doSelected } from './reducer/experimentAction';

//MAIN FUNCTION
/**
 * @description             Component table and particula button
 * @param {object}          buttons- Information about the objetc in the table
 * @param {object}          classes - Style classes
 * @param {array}           columns Information about the columns
 * @param {object}          filterenablerow- Information for 2 filter for select only responsable variable number and selectn single experiment
 * @param {function}        functiongetdata - Get all row for table
 * @param {function}        functionGetIntersectionTrait - Get interseccion of all responsabe variable
 * @param {function}        functiongettrait - Get all response variable form occurrences
 * @param {function}        functionresetfilterrow - Reset filter to default
 * @param {function}        functionsaveselected - Save selected occurrences
 * @param {object}          style - Style for particular cell
 * @param {object}          user - User information
 * @param {object}          ref - reference made by React.forward
 * @returns {JSX}           Return table
 */
const ExperimentTabOrganism = React.forwardRef(
  (
    {
      buttons,
      classes,
      columns,
      filterenablerow,
      functiongetdata,
      functionGetIntersectionTrait,
      functionGetTrait,
      functionresetfilterrow,
      functionsaveselected,
      style,
      user,
    },
    ref,
  ) => {

    const INITIAL_STATE = {
      pageCount: 0,
      data: [],
      selectedRowsIds: [],
      selectedRows: [],
      selectedIndeterminateRowsIds: [],
      loading: false,
      searchTerm: '',
      pageIndex: 0,
      itemsCount: 0,
      firstMount: true,
      message: '',
    }
    const fetchIdRef = useRef(0);
    const mountedRef = useRef(true);
    const [{
      pageCount,
      data,
      selectedRowsIds,
      selectedRows,
      selectedIndeterminateRowsIds,
      loading,
      searchTerm,
      pageIndex,
      itemsCount,
      firstMount,
      message,
    }, dispatch] = useReducer(experimentReducer, INITIAL_STATE);

    const setAddItemSelected = ({ 
      checked, 
      functionGetTrait: validateGetTrait, 
      id, 
      parentRow, 
      row, 
      selectedRows: checkedRows, 
    }) => {
      const newChecked = !checked;
      let selectedRowsIdsLocal = selectedRowsIds;
      let selectedRowsIntedeterminateIdsLocal = selectedIndeterminateRowsIds;
      let selectedRowsLocal = selectedRows;
      if (newChecked) {
        if (!selectedRowsIds.includes(id)) {
          selectedRowsIdsLocal.push(id);
          selectedRowsLocal.push(row);
          //Search if is a parent row to check all childs rows
          if (row.subRows !== undefined) {
            row.subRows.forEach(element => {
              
              //const getTrait = functionGetTrait({ row: element, selectedRows: checkedRows });
              if (!selectedRowsIds.includes(element.id) && element.isTraitsFilter /*&& getTrait.isIntersection*/) {
                selectedRowsIdsLocal.push(element.id)
                selectedRowsLocal.push(element)
              }
            });
          }
          if (parentRow !== undefined && parentRow !== null) {
            if (!selectedRowsIds.includes(parentRow.id)) {
              const idChild = parentRow.subRows.map((child) => (child.id));
              //Check parent if all child was checked
              if (idChild.every(child => {
                return selectedRowsIds.includes(child)
              })) {
                selectedRowsIdsLocal.push(parentRow.id);
                selectedRowsLocal.push(parentRow.id);
              }
              //If is select owne row child intedeterminate check parent row
              if(!selectedRowsIntedeterminateIdsLocal.includes(parentRow.id)){
                selectedRowsIntedeterminateIdsLocal.push(parentRow.id);
              }
            }
          }
          //Clear intedeterminate intedeterminate if is selected the parent row
          else{
            if(selectedRowsIntedeterminateIdsLocal.includes(row.id) && selectedRowsIds.includes(row.id)){
              selectedRowsIntedeterminateIdsLocal.splice(selectedRowsIntedeterminateIdsLocal.findIndex(idLocal => idLocal === row.id), 1);
            }
          }
          dispatch(doSelected({
            selectedIndeterminateRowsIds: selectedRowsIntedeterminateIdsLocal,
            selectedRows: selectedRowsLocal,
            selectedRowsIds: selectedRowsIdsLocal,
          }));
        }
      } else {
        if (selectedRowsIds.includes(id)) {
          selectedRowsLocal.splice(selectedRows.findIndex(rowLocal => rowLocal.id === id), 1);
          selectedRowsIdsLocal.splice(selectedRowsIds.findIndex(idLocal => idLocal === id), 1);
          if (row.subRows !== undefined) {
            row.subRows.forEach(element => {
              if (selectedRowsIds.includes(element.id)) {
                selectedRowsLocal.splice(selectedRows.findIndex(rowLocal => rowLocal.id === element.id), 1);
                selectedRowsIdsLocal.splice(selectedRowsIds.findIndex(idLocal => idLocal === element.id), 1);
              }
            });
          }
          if (parentRow !== undefined && parentRow !== null) {
            const idChild = parentRow.subRows.map((child) => (child.id));
            const cleanChild = idChild.every(child => {
              return !selectedRowsIds.includes(child)
            });
            //Remove checked of parent if all child was unchecked
            if (selectedRowsIds.includes(parentRow.id)) {
              selectedRowsLocal.splice(selectedRows.findIndex(rowLocal => rowLocal.id === parentRow.id), 1);
              selectedRowsIdsLocal.splice(selectedRowsIds.findIndex(idLocal => idLocal === parentRow.id), 1);
              if(!selectedRowsIntedeterminateIdsLocal.includes(parentRow.id)){
                selectedRowsIntedeterminateIdsLocal.push(parentRow.id);
              }
            }
            //Remove if parent is indeterminate and uncheck all childs
            if(selectedRowsIntedeterminateIdsLocal.includes(parentRow.id) && cleanChild){
              selectedRowsIntedeterminateIdsLocal.splice(selectedRowsIntedeterminateIdsLocal.findIndex(idLocal => idLocal === parentRow.id), 1);
            }
          }
          else{
            if(selectedIndeterminateRowsIds.includes(row.id)){
              selectedRowsIntedeterminateIdsLocal.splice(selectedRowsIntedeterminateIdsLocal.findIndex(idLocal => idLocal === row.id), 1);
            }
          }
          dispatch(doSelected({
            selectedIndeterminateRowsIds: selectedRowsIntedeterminateIdsLocal,
            selectedRows: selectedRowsLocal,
            selectedRowsIds: selectedRowsIdsLocal,
          }));
        }
      }
      handleSaveExperimentLocal({ experimentsSelected: selectedRowsLocal, idExperimentsSelected: selectedRowsIdsLocal });
      return newChecked;
    }

    const fetchAPIData = async ({
      limit,
      page,
      searchParameters,
      filterenablerow,
    }) => {
      try {
        const result = await functiongetdata({
          limit,
          page,
          searchParameters,
          filter: filterenablerow,
          programDbIds: user.data.programs.map((program) => program.programDbId),
        });
        if(mountedRef.current){
          if (result.status === 200) {
            dispatch(doFill({
              //data: result.data.map(item => ({ ...item, selectedRows: selectedRows, selectedRowsIds: selectedRowsIds })),
              data: result.data,
              itemsCount: result.metadata.pagination.totalCount,
              pageCount: result.metadata.pagination.totalPages,
              pageIndex: page,
            }))
          }else{
            dispatch(doMessage({message: result.message}));
          }
        }
        
      } catch (e) {
        //todo: Handle error
      }
    };

    const fetchData = useCallback(
      ({ limit, page, searchParameters }) => {
        const fetchId = ++fetchIdRef.current;
        dispatch(doLoading());
        mountedRef.current = true;
        if (fetchId === fetchIdRef.current) {
          fetchAPIData({
            limit,
            page,
            searchParameters,
            filterenablerow,
          });
        }
      },
      [searchTerm, filterenablerow]
    );

    useEffect(() => {
      let isMounted = true;
      if (isMounted && selectedRowsIds.length > 0) {
        dispatch(doResetFilterRow())
      }
      return () => {
        isMounted = false
      }
    }, [filterenablerow])


    const handleSaveExperimentLocal = ({ experimentsSelected, idExperimentsSelected }) => {
      let experiments = [];
      let traits = [];
      if (experimentsSelected.length !== 0) {
        experimentsSelected.forEach((element) => {
          if (element.typeData === TYPE_DATA.occurence) {
            let experiment = {
              crop: '',
              programCode: '',
              experimentId: 0,
              experimentName: 0,
              occurrences: [],
            };
            experiment.crop = element.cropName;
            experiment.programCode = element.programCode;
            experiment.experimentId = element.experimentDbId;
            experiment.experimentName = element.experimentName;
            experiment.experimentDesignType = element.experimentDesignType;
            experiment.occurrences.push({
              occurrenceId: element.occurrenceDbId,
              occurrenceName: element.occurrenceName,
              locationName: element.location,
              locationId: element.locationDbId,
              isPlotXY: element.isPlotXY,
            });
            const index = experiments.findIndex(
              (item) => item.experimentId == experiment.experimentId,
            );
            if (index > -1) {
              experiments[index].occurrences.push(experiment.occurrences[0]);
            } else {
              experiments.push(experiment);
            }
            traits.push(element.traitsFilter);
          }
        });
      }
      traits = functionGetIntersectionTrait({ traits });
      functionsaveselected({
        experimentsSelected: experiments,
        traitsSelected: traits,
      });
    };

    // useEffect(() => {
    //   return () => {
    //     mountedRef.current = false;
    //   }
    // }, [refreshCount])

    return (
      <Grid
        container
        ref={ref}
        data-testid={'TableExperimentTestId'}
        id={'TableExperimentId'}
        direction={'column'}
        spacing={2}
      >
        <Grid item xs={12}>
          <ExperimentTableMolecule
            classes={classes}
            columns={columns}
            data={data}
            fetchData={fetchData}
            functionGetTrait={functionGetTrait}
            itemsCount={itemsCount}
            loading={loading}
            pageCount={pageCount}
            pageIndex={pageIndex}
            permissions={user.data.permissions}
            rowStyle={style}
            selectedRows={selectedRows}
            selectedRowsIds={selectedRowsIds}
            selectedIndeterminateRowsIds={selectedIndeterminateRowsIds}
            setAddItemSelected={setAddItemSelected}
            message={message}
            //Head
            filter={buttons.filter}
            filterenablerow={filterenablerow}
            functionresetfilterrow={functionresetfilterrow}
            setting={buttons.setting}
          />
        </Grid>
      </Grid>
    );
  },
);
// Type and required properties
ExperimentTabOrganism.propTypes = {
  buttons: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
  columns: PropTypes.array.isRequired,
  filterenablerow: PropTypes.object.isRequired,
  functiongetdata: PropTypes.func.isRequired,
  functionGetIntersectionTrait: PropTypes.func.isRequired,
  functionGetTrait: PropTypes.func.isRequired,
  functionresetfilterrow: PropTypes.func.isRequired,
  functionsaveselected: PropTypes.func.isRequired,
  style: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
};
// Default properties
ExperimentTabOrganism.defaultProps = {};

export default ExperimentTabOrganism;
