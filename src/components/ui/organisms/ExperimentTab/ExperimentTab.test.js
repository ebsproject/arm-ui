import ExperimentTabOrganism from './ExperimentTab';
import React from 'react';
import { render, cleanup, screen } from '@testing-library/react';
import '@testing-library/dom';
import '@testing-library/jest-dom/extend-expect';

afterEach(cleanup);

jest.mock('../../molecules/TableExperiment/ExperimentTable', () => () => {
  return <div></div>
})

const props = {
  buttons: {
    setting: {
      title: '',
      helptext: '',
    },
    filter: {
      title: '',
      helptext: '',
    },
  },
  classes: {
    class: '',
    classbutton: '',
    button: '',
  },
  columns: [
    { Header: 'Occurrence ID', accessor: 'occurrenceDbId', isVisible: false },
  ],
  filterenablerow: {
    sameExperiemnt: {
      isActive: false,
      isEnable: true,
    },
    responseVariableOnlyFloat: {
      isActive: false,
      isEnable: false
    },
  },
  functiongetdata: jest.fn(),
  functionGetIntersectionTrait: jest.fn(),
  functionGetTrait: jest.fn(),
  functionresetfilterrow: jest.fn(),
  functionsaveselected: jest.fn(),
  style: {},
  title: '',
  user: {
    data: {
      permissions: {
        desingExperiment:
        {
          label: ''
        }
      },
    },
  },
};
test('ExperimentTab is in the DOM', () => {
  render(<ExperimentTabOrganism {...props}></ExperimentTabOrganism>);
  expect(screen.getByTestId('TableExperimentTestId')).toBeInTheDocument();
});
