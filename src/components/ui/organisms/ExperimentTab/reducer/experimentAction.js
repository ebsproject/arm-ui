import PropTypes from 'prop-types';

export const doLoading = () => ({
    type: 'loading',
    payload: {},
})
export const doFill = ({ data, pageCount, itemsCount, pageIndex }) => ({
    type: 'fill/data',
    payload: {
        data,
        pageCount,
        itemsCount,
        pageIndex,
        
    }
})
export const doSelected = ({ selectedRowsIds, selectedRows, selectedIndeterminateRowsIds}) => ({
    type: 'selected',
    payload: { selectedRowsIds, selectedRows, selectedIndeterminateRowsIds },
})
export const doResetFilterRow = () => ({
    type: 'clear/filterRow',
    payload: {},
})
export const doMessage = ({ message }) => ({
    type: 'fill/message',
    payload: {
        message,
    }
})


doFill.PropTypes = {
    data: PropTypes.array.isRequired,
    itemsCount: PropTypes.number.isRequired,
    pageCount: PropTypes.number.isRequired,
    pageIndex: PropTypes.number.isRequired,
}

doSelected.PropTypes = {
    selectedRows: PropTypes.array.isRequired,
    selectedRowsIds: PropTypes.array.isRequired,
}

