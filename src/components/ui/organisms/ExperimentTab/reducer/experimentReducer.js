
export const experimentReducer = (state, action) => {
    const { firstMount, selectedRowsIds,selectedIndeterminateRowsIds } = state;
    switch (action.type) {
        case 'loading':
            return {
                ...state,
                loading: true,
            }
        case 'fill/data':
            return {
                ...state,
                loading: false,
                data: action.payload.data,
                pageCount: action.payload.pageCount,
                itemsCount: action.payload.itemsCount,
                pageIndex: action.payload.pageIndex,
                message: '',
            }
        case 'fill/message':
                return {
                    ...state,
                    loading: false,
                    data: [],
                    pageCount: 0,
                    itemsCount: 0,
                    pageIndex: 0,
                    message: action.payload.message,
                }
        case 'selected':
            return {
                ...state,
                selectedIndeterminateRowsIds: action.payload.selectedIndeterminateRowsIds,
                selectedRows: action.payload.selectedRows,
                selectedRowsIds: action.payload.selectedRowsIds,
            }
        case 'clear/filterRow':
            return {
                ...state,
                selectedRows: [],
                selectedRowsIds: [],
            }
        default:
            return state
    }
}