import PropTypes from 'prop-types';

export const doNext = () => ({
    type: 'next',
    payload: {},
})
export const doBack = () => ({
    type: 'back',
    payload: {},
})

export const doDropdownInitial = ({ analysisObjective, traitAnalysisPattern }) => ({
    type: 'dropdown/initial',
    payload: { analysisObjective, traitAnalysisPattern },
})
export const doDropdownExperiment = ({ experimentLocationAnalysisPattern }) => ({
    type: 'dropdown/experiment',
    payload: { experimentLocationAnalysisPattern },
})
export const doDropdownPatternLocation = ({ analysisConfiguration }) => ({
    type: 'dropdown/pattern-location',
    payload: { analysisConfiguration },
})
export const doDropdownAnalysisConfiguration = ({ dropdown, mainModel, spatialAdjusting }) => ({
    type: 'dropdown/analysisConfiguration',
    payload: { dropdown, mainModel, spatialAdjusting },
})

export const doAddOccurrence = ({ experimentsSelected, traitsSelected }) => ({
    type: 'add/occurrence',
    payload: { experimentsSelected, traitsSelected },
})
export const doAddTraits = ({ traits }) => ({
    type: 'add/traits',
    payload: { traits },
})
export const doAddAnalysisObjective = ({ analysisObjective }) => ({
    type: 'add/analysisObjective',
    payload: { analysisObjective },
})
export const doAddTraitAnalysisPattern = ({ traitAnalysisPattern }) => ({
    type: 'add/traitAnalysisPattern',
    payload: { traitAnalysisPattern },
})
export const doAddExperimentLocationAnalysisPattern = ({ experimentLocationAnalysisPattern }) => ({
    type: 'add/experimentLocationAnalysisPattern',
    payload: { experimentLocationAnalysisPattern },
})
export const doAddAnalysisConfiguration = ({ analysisConfiguration }) => ({
    type: 'add/analysisConfiguration',
    payload: { analysisConfiguration },
})
export const doAddMainModel = ({ mainModel }) => ({
    type: 'add/mainModel',
    payload: { mainModel },
})
export const doAddSpatialAdjusting = ({ spatialAdjusting }) => ({
    type: 'add/spatialAdjusting',
    payload: { spatialAdjusting },
})

export const doResetFilterRow = ({ filterEnableRow }) => ({
    type: 'reset/filterRow',
    payload: { filterEnableRow },
})

doDropdownInitial.propTypes = {
    analysisObjective: PropTypes.array.isRequired,
    traitAnalysisPattern: PropTypes.array.isRequired,
}
doDropdownExperiment.protoType = {
    experimentLocationAnalysisPattern: PropTypes.array.isRequired,
}
doDropdownPatternLocation.propTypes = {
    analysisConfiguration: PropTypes.array.isRequired,
}
doDropdownAnalysisConfiguration.PropTypes = {
    dropdown: PropTypes.shape({
        mainModel: PropTypes.array.isRequired,
        spatialAdjusting: PropTypes.array.isRequired,
    }),
    mainModel: PropTypes.object.isRequired,
    spatialAdjusting: PropTypes.object.isRequired,
}

doAddOccurrence.propTypes = {
    experimentsSelected: PropTypes.array.isRequired,
    traitsSelected: PropTypes.array.isRequired,
}
doAddTraits.PropTypes = {
    traits: PropTypes.array.isRequired,
}
doAddAnalysisObjective.PropTypes = {
    analysisObjective: PropTypes.object.isRequired,
}
doAddTraitAnalysisPattern.PropTypes = {
    traitAnalysisPattern: PropTypes.object.isRequired,
}
doAddExperimentLocationAnalysisPattern.PropTypes = {
    experimentLocationAnalysisPattern: PropTypes.object.isRequired,
}
doAddMainModel.PropTypes = {
    mainModel: PropTypes.object.isRequired,
}
doAddSpatialAdjusting.PropTypes = {
    spatialAdjusting: PropTypes.object.isRequired,
}

doResetFilterRow.PropTypes = {
    filterEnableRow: PropTypes.object.isRequired,
}