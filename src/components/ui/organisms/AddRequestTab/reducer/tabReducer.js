import { MESSAGE_SEVERITY } from "utils/config";


export const MESSAGE_SYSTEM = {
    experiments: {
        empty: 'Please select one or more occurrence',
        error: {
            emptyTrait:
                'Occurrences have different triats and it is not possible to select them all at once',
        },
    },
    traits: {
        empty: 'Please select one Response Variables',
    },
    analysisObjective: {
        empty: 'Please select one Analysis Objective',
    },
    traitAnalysisPattern: {
        empty: 'Please select Analysis Pattern',
    },
    experimentLocationAnalysisPattern: {
        empty: 'Please select Experiment Location Analysis Pattern',
    },
    analysisConfiguration: {
        empty: 'Please select Analysis Configuration',
    },
    mainModel: { empty: 'Please select Main Model' },
    spatialAdjusting: { empty: 'Please select Spatial Adjusting' },
    system: {
        continue: 'Please click into next button',
        finish: 'Please click into button Submit Analysis Request',
    },
};


export const tabReducer = (state, action) => {
    const { buttons, information, data, step } = state;
    const {
        analysisConfiguration,
        analysisObjective,
        dropdown,
        experimentLocationAnalysisPattern,
        experiments,
        mainModel,
        spatialAdjusting,
        traitAnalysisPattern,
        traits,
        nameRequest,
    } = data;
    const { cancel, back, next } = buttons;
    let dataChange = {
        message: '',
        nextDisable: true,
        nextText: 'NEXT',
        backDisable: true,
        backText: 'BACK',
        severity: MESSAGE_SEVERITY.warning,
    };

    let newStep = step
    switch (action.type) {
        case 'next':
            newStep++
            dataChange = validateStep({
                step: newStep,
                dataChange,
                experiments,
                dropdown,
                traits,
                analysisObjective,
                traitAnalysisPattern,
                experimentLocationAnalysisPattern,
                analysisConfiguration,
                mainModel,
                spatialAdjusting,
            })
            dataChange.backDisable = false;
            return {
                ...state,
                buttons: {
                    ...buttons,
                    next: {
                        text: dataChange.nextText,
                        disabled: dataChange.nextDisable,
                    },
                    back: {
                        text: dataChange.backText,
                        disabled: dataChange.backDisable,
                    }
                },
                information: {
                    message: dataChange.message,
                    severity: dataChange.severity,
                },
                step: newStep,
            }
        case 'back':
            newStep--
            dataChange = validateStep({
                step: newStep,
                dataChange,
                experiments,
                dropdown,
                traits,
                analysisObjective,
                traitAnalysisPattern,
                experimentLocationAnalysisPattern,
                analysisConfiguration,
                mainModel,
                spatialAdjusting,
            })
            dataChange.backDisable = newStep === 0;
            return {
                ...state,
                buttons: {
                    ...buttons,
                    next: {
                        text: dataChange.nextText,
                        disabled: dataChange.nextDisable,
                    },
                    back: {
                        text: dataChange.backText,
                        disabled: dataChange.backDisable,
                    }
                },
                information: {
                    message: dataChange.message,
                    severity: dataChange.severity,
                },
                step: newStep,
            }
        case 'dropdown/initial':
            return {
                ...state,
                data: {
                    ...data,
                    dropdown: {
                        ...dropdown,
                        analysisObjective: action.payload.analysisObjective,
                        traitAnalysisPattern: action.payload.traitAnalysisPattern,
                    },
                },
            }
        case 'dropdown/experiment':
            return {
                ...state,
                data: {
                    ...data,
                    dropdown: {
                        ...dropdown,
                        experimentLocationAnalysisPattern: action.payload.experimentLocationAnalysisPattern,
                    },
                },
            }
        case 'dropdown/pattern-location':
            return {
                ...state,
                data: {
                    ...data,
                    dropdown: {
                        ...dropdown,
                        analysisConfiguration: action.payload.analysisConfiguration,
                    },
                    nameRequest: updateNameRequest({nameRequest, experimentLocationAnalysisPattern}),
                },
            }
        case 'dropdown/analysisConfiguration':
            return {
                ...state,
                data: {
                    ...data,
                    dropdown: {
                        ...dropdown,
                        mainModel: action.payload.dropdown.mainModel,
                        spatialAdjusting: action.payload.dropdown.spatialAdjusting,
                    },
                    mainModel: action.payload.mainModel,
                    spatialAdjusting: action.payload.spatialAdjusting,
                },
            }
        case 'add/occurrence':
            dataChange = validateStep({
                step,
                dataChange,
                experiments,
                dropdown,
                traits,
                analysisObjective,
                traitAnalysisPattern,
                experimentLocationAnalysisPattern,
                analysisConfiguration,
                mainModel,
                spatialAdjusting,
            });
            if (experiments.length === 0 && action.payload.experimentsSelected.length > 0) {
                dataChange.message = MESSAGE_SYSTEM.system.continue;
                dataChange.severity = MESSAGE_SEVERITY.info;
                dataChange.nextDisable = false;
            }
            if (experiments.length !== 0 && action.payload.experimentsSelected.length === 0) {
                dataChange.message = MESSAGE_SYSTEM.experiments.empty;
                dataChange.severity = MESSAGE_SEVERITY.warning;
                dataChange.nextDisable = true;
            }
            return {
                ...state,
                buttons: {
                    ...buttons,
                    next: {
                        text: dataChange.nextText,
                        disabled: dataChange.nextDisable,
                    },
                },
                information: {
                    message: dataChange.message,
                    severity: dataChange.severity,
                },
                data: {
                    ...data,
                    experiments: action.payload.experimentsSelected,
                    traits: [],
                    experimentLocationAnalysisPattern: null,
                    analysisConfiguration: null,
                    mainModel: null,
                    spatialAdjusting: null,
                    dropdown: {
                        ...dropdown,
                        traits: action.payload.traitsSelected,
                        experimentLocationAnalysisPattern: [],
                        analysisConfiguration: [],
                        mainModel: [],
                        spatialAdjusting: [],
                    },
                    nameRequest: createNewNameRequest({experiments: action.payload.experimentsSelected, traitsDropdown: action.payload.traitsSelected, experimentLocationAnalysisPattern}),
                },
            }
        case 'add/traits':
            dataChange = validateStep({
                step,
                dataChange,
                experiments,
                dropdown,
                traits,
                analysisObjective,
                traitAnalysisPattern,
                experimentLocationAnalysisPattern,
                analysisConfiguration,
                mainModel,
                spatialAdjusting,
            });

            if (traits.length === 0 && action.payload.traits.length > 0) {
                dataChange.message = MESSAGE_SYSTEM.system.continue;
                dataChange.severity = MESSAGE_SEVERITY.info;
                dataChange.nextDisable = false;
            }
            if (traits.length !== 0 && action.payload.traits.length === 0) {
                dataChange.message = MESSAGE_SYSTEM.traits.empty;
                dataChange.severity = MESSAGE_SEVERITY.warning;
                dataChange.nextDisable = true;
            }
            return {
                ...state,
                buttons: {
                    ...buttons,
                    next: {
                        text: dataChange.nextText,
                        disabled: dataChange.nextDisable,
                    },
                },
                information: {
                    message: dataChange.message,
                    severity: dataChange.severity,
                },
                data: {
                    ...data,
                    traits: action.payload.traits,
                    nameRequest: createNewNameRequest({experiments, traits: action.payload.traits, experimentLocationAnalysisPattern })
                },
            }
        case 'add/analysisObjective':
            dataChange = validateStep({
                step,
                dataChange,
                experiments,
                dropdown,
                traits,
                analysisObjective: action.payload.analysisObjective,
                traitAnalysisPattern,
                experimentLocationAnalysisPattern,
                analysisConfiguration,
                mainModel,
                spatialAdjusting,
            });
            return {
                ...state,
                buttons: {
                    ...buttons,
                    next: {
                        text: dataChange.nextText,
                        disabled: dataChange.nextDisable,
                    },
                },
                information: {
                    message: dataChange.message,
                    severity: dataChange.severity,
                },
                data: {
                    ...data,
                    analysisObjective: action.payload.analysisObjective,
                },
            }
        case 'add/traitAnalysisPattern':
            dataChange = validateStep({
                step,
                dataChange,
                experiments,
                dropdown,
                traits,
                analysisObjective,
                traitAnalysisPattern: action.payload.traitAnalysisPattern,
                experimentLocationAnalysisPattern,
                analysisConfiguration,
                mainModel,
                spatialAdjusting,
            });
            return {
                ...state,
                buttons: {
                    ...buttons,
                    next: {
                        text: dataChange.nextText,
                        disabled: dataChange.nextDisable,
                    },
                },
                information: {
                    message: dataChange.message,
                    severity: dataChange.severity,
                },
                data: {
                    ...data,
                    traitAnalysisPattern: action.payload.traitAnalysisPattern,
                },
            }
        case 'add/experimentLocationAnalysisPattern':
            dataChange = validateStep({
                step,
                dataChange,
                experiments,
                dropdown,
                traits,
                analysisObjective,
                traitAnalysisPattern,
                experimentLocationAnalysisPattern: action.payload.experimentLocationAnalysisPattern,
                analysisConfiguration,
                mainModel,
                spatialAdjusting,
            });

            return {
                ...state,
                buttons: {
                    ...buttons,
                    next: {
                        text: dataChange.nextText,
                        disabled: dataChange.nextDisable,
                    },
                },
                information: {
                    message: dataChange.message,
                    severity: dataChange.severity,
                },
                data: {
                    ...data,
                    experimentLocationAnalysisPattern: action.payload.experimentLocationAnalysisPattern,

                },
            }
        case 'add/analysisConfiguration':
            dataChange = validateStep({
                step,
                dataChange,
                experiments,
                dropdown,
                traits,
                analysisObjective,
                traitAnalysisPattern,
                experimentLocationAnalysisPattern,
                analysisConfiguration: action.payload.analysisConfiguration,
                mainModel,
                spatialAdjusting,
            });
            return {
                ...state,
                buttons: {
                    ...buttons,
                    next: {
                        text: dataChange.nextText,
                        disabled: dataChange.nextDisable,
                    },
                },
                information: {
                    message: dataChange.message,
                    severity: dataChange.severity,
                },
                data: {
                    ...data,
                    analysisConfiguration: action.payload.analysisConfiguration,
                    mainModel: null,
                    spatialAdjusting: null,
                },
            }
        case 'add/mainModel':
            dataChange = validateStep({
                step,
                dataChange,
                experiments,
                dropdown,
                traits,
                analysisObjective,
                traitAnalysisPattern,
                experimentLocationAnalysisPattern,
                analysisConfiguration,
                mainModel: action.payload.mainModel,
                spatialAdjusting,
            });
            return {
                ...state,
                buttons: {
                    ...buttons,
                    next: {
                        text: dataChange.nextText,
                        disabled: dataChange.nextDisable,
                    },
                },
                information: {
                    message: dataChange.message,
                    severity: dataChange.severity,
                },
                data: {
                    ...data,
                    mainModel: action.payload.mainModel,
                },
            }
        case 'add/spatialAdjusting':
            dataChange = validateStep({
                step,
                dataChange,
                experiments,
                dropdown,
                traits,
                analysisObjective,
                traitAnalysisPattern,
                experimentLocationAnalysisPattern,
                analysisConfiguration,
                mainModel,
                spatialAdjusting: action.payload.spatialAdjusting,
            });
            return {
                ...state,
                buttons: {
                    ...buttons,
                    next: {
                        text: dataChange.nextText,
                        disabled: dataChange.nextDisable,
                    },
                },
                information: {
                    message: dataChange.message,
                    severity: dataChange.severity,
                },
                data: {
                    ...data,
                    spatialAdjusting: action.payload.spatialAdjusting,
                },
            }
        case 'reset/filterRow':
            dataChange = validateStep({
                step: 0,
                dataChange,
                experiments: [],
                dropdown,
                traits: [],
                analysisObjective,
                traitAnalysisPattern,
                experimentLocationAnalysisPattern,
                analysisConfiguration,
                mainModel,
                spatialAdjusting,
            });
            return {
                ...state,
                buttons: {
                    ...buttons,
                    next: {
                        text: dataChange.nextText,
                        disabled: dataChange.nextDisable,
                    },
                },
                information: {
                    message: dataChange.message,
                    severity: dataChange.severity,
                },
                step: 0,
                filterEnableRow: action.payload.filterEnableRow,
                data: {
                    ...data,
                    experiments: [],
                    dropdown: {
                        ...dropdown,
                        analysisConfiguration: [],
                        experimentLocationAnalysisPattern: [],
                        mainModel: [],
                        spatialAdjusting: [],
                        traits: [],
                    },
                    analysisConfiguration: null,
                    analysisObjective: null,
                    experimentLocationAnalysisPattern: null,
                    mainModel: null,
                    spatialAdjusting: null,
                    traitAnalysisPattern: null,
                    traits: [],
                },
            }
        default:
            return state;
    }
}

const validateStep = ({
    step,
    dataChange,
    experiments,
    dropdown,
    traits,
    analysisObjective,
    traitAnalysisPattern,
    experimentLocationAnalysisPattern,
    analysisConfiguration,
    mainModel,
    spatialAdjusting,
}) => {
    switch (step) {
        case 0:
            if (experiments.length === 0) {
                dataChange.message = MESSAGE_SYSTEM.experiments.empty;
            } else if (experiments.length > 0 && dropdown.traits.length === 0) {
                dataChange.message = MESSAGE_SYSTEM.experiments.error.emptyTrait;
            } else {
                dataChange.message = MESSAGE_SYSTEM.system.continue;
                dataChange.severity = MESSAGE_SEVERITY.info;
                dataChange.nextDisable = false;
            }
            return dataChange;
        case 1:
            if (traits.length === 0) {
                dataChange.message = MESSAGE_SYSTEM.traits.empty;
            } else if (traits.length > 0 && analysisObjective === null) {
                dataChange.message = MESSAGE_SYSTEM.analysisObjective.empty;
            } else {
                dataChange.nextDisable = false;
                dataChange.severity = MESSAGE_SEVERITY.info;
                dataChange.message = MESSAGE_SYSTEM.system.continue;
            }
            return dataChange;
        case 2:
            if (traitAnalysisPattern === null) {
                dataChange.message = MESSAGE_SYSTEM.traitAnalysisPattern.empty;
            } else if (experimentLocationAnalysisPattern === null) {
                dataChange.message = MESSAGE_SYSTEM.experimentLocationAnalysisPattern.empty;
            } else if (analysisConfiguration === null && experiments.length === 1) {
                dataChange.message = MESSAGE_SYSTEM.analysisConfiguration.empty;
            } else if (mainModel === null && experiments.length === 1) {
                dataChange.message = MESSAGE_SYSTEM.mainModel.empty;
            } else if (spatialAdjusting === null && experiments.length === 1) {
                dataChange.message = MESSAGE_SYSTEM.spatialAdjusting.empty;
            } else {
                dataChange.nextDisable = false;
                dataChange.severity = MESSAGE_SEVERITY.info;
                dataChange.message = MESSAGE_SYSTEM.system.finish;
                dataChange.nextText = 'Submit Analysis Request';
            }
            return dataChange;
        default:
            return dataChange;
    }
}

const createNewNameRequest = ({experiments, traits, traitsDropdown, experimentLocationAnalysisPattern}) => {
    const nameRequestPattern = '$experiments$traits-$now';
    const now = new Date();
    const dateString = `${now.toISOString().split('T')[0].replaceAll('-', '')}-${now.toISOString().split('T')[1].split('.')[0].replaceAll(':', '')}`;


    const experimentString = experiments.length === 1 ?
        `${experiments[0].experimentName}-SESL`:
        experiments.length > 0 ? `${experiments[0].programCode}-MESL` : '';

    let traitsString = "";
    if ( traitsDropdown !== undefined && traitsDropdown.length === 1){
        traitsString= `-${traitsDropdown[0].variableAbbrev.substring(0,4)}`
    }else{
        traitsString = traits !== undefined  && traits.length === 1 ?
            `-${traits[0].traitName.substring(0,4)}` :
            ``;
    }

    return nameRequestPattern
        .replace('$now', dateString)
        .replace('$experiments', experimentString)
        .replace('$traits', traitsString);
}


const updateNameRequest = ({nameRequest, experimentLocationAnalysisPattern}) => {
    if (!experimentLocationAnalysisPattern) {
        return nameRequest;
    }
    const code = `-${experimentLocationAnalysisPattern.propertyCode}`
    let pattern = /-[S|M]E[S|M]L/
    return nameRequest.replace(pattern, code)
}