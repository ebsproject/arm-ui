import React, { forwardRef, useEffect, useReducer, useState } from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS AND MOLECULES TO USE
import { Button, Collapse, Grid, Tab, Tabs } from '@material-ui/core';
import SubTitleMolecule from 'components/ui/molecules/SubTitle';
import { useHistory } from 'react-router-dom/cjs/react-router-dom.min';
import TabPanelMolecule from 'components/ui/molecules/TabPanel';
import ExperimentTabOrganism from 'components/ui/organisms/ExperimentTab';
import TraitAutoSelectMolecule from 'components/ui/molecules/Select/TraitAutoSelect/traitautoselect';
import AutoSelectMolecule from 'components/ui/molecules/Select/AutoSelect';
import { TOKEN } from 'utils/config';
import { tabReducer, MESSAGE_SYSTEM } from './reducer/tabReducer';
import { 
  doAddAnalysisConfiguration, 
  doAddAnalysisObjective, 
  doAddExperimentLocationAnalysisPattern, 
  doAddMainModel, 
  doAddOccurrence, 
  doAddSpatialAdjusting, 
  doAddTraitAnalysisPattern, 
  doAddTraits, 
  doBack, 
  doDropdownAnalysisConfiguration, 
  doDropdownExperiment, 
  doDropdownInitial, 
  doDropdownPatternLocation, 
  doNext, 
  doResetFilterRow,
 } from './reducer/tabAction';

//MAIN FUNCTION
/**
 * @description           Form on step to receive the information about the request
 * @param {reference}     ref - Reference made by React.forward
 * @param {object}        values - Values to save the request.
 * @param {object}        values.occurences - Occurrences select by the user.
 * @param {object}        setting -Object with all parameter to other component.
 * @param {object}        selects -Object with all parameter and values to combo box component.
 * @param {object}        experiment -Object with all parameter and value of experiment and occurrences.
 * @param {string}        subtitle -Object with all values to navigation bar component.
 * @param {object}        request -Object to refer to the requestor data.
 * @param {object}        classes -Classes to put specific style with makeStyle.
 * @property {int}        value -UseState Current index tab.
 * @property {array}      tabsArray -List of the tabs.
 * @property {array}      experiment -Star all values in the redux.
 * @property {array}      messages -Get object message into the tabs.
 * @property {string}     severity -Severity icon
 * @property {object}     message -Get the error message.
 * @property {object}     configButton -Get button the configuration to tabs.
 * @function {void}       handleChange -Function to change the current tab.
 * @function {void}       alertMessage -Display a message error to create a new request.
 * @function {void}       useEffect -Validate if exist requestor id.
 * @function {void}       useEffect -To start the component, create all the data to create the request.
 * @property {string}     data-testId -Id to use inside addRequestTab.test.js file.
 * @returns {component}   Tab to select occurrences asn configuration to create the request
 */
const AddRequestTabOrganism = React.forwardRef(
  ({ setting, selects, experiment, subtitle, classes, request }, ref) => {
    const INITLA_STATE = {
      buttons: {
        cancel: {
          text: 'Cancel',
          disabled: false,
          onClick: () => {
            history.push(setting.routes.rotuelist);
          },
        },
        back: {
          text: 'Back',
          disabled: true,
        },
        next: {
          text: 'Next',
          disabled: true,
        },
      },
      information: {
        message: MESSAGE_SYSTEM.experiments.empty,
        severity: 'warning',
      },
      data: {
        experiments: [],
        dropdown: {
          analysisConfiguration: [],
          analysisObjective: [],
          experimentLocationAnalysisPattern: [],
          mainModel: [],
          spatialAdjusting: [],
          traitAnalysisPattern: [],
          traits: [],
        },
        analysisConfiguration: null,
        analysisObjective: null,
        experimentLocationAnalysisPattern: null,
        mainModel: null,
        spatialAdjusting: null,
        traitAnalysisPattern: null,
        traits: [],
        nameRequest: '',
      },
      step: 0,
      filterEnableRow: searchLocationRowFilter(),
    }
    const [addRequestTabState, dispatch] = useReducer(tabReducer, INITLA_STATE);
    const { buttons, information, data, step, filterEnableRow } = addRequestTabState;
    const {
      analysisConfiguration,
      analysisObjective,
      dropdown,
      experimentLocationAnalysisPattern,
      experiments,
      mainModel,
      spatialAdjusting,
      traitAnalysisPattern,
      traits,
      nameRequest,
    } = data;
    const { cancel, back, next } = buttons;
    const history = useHistory();
    const tabsArray = ['Experiments', 'Settings', 'Parameters'];
    const spacingSelect = 2;

    useEffect(() => {
      let isMounted = true;
      const fetchData = async ({ isMounted }) => {
        const fetchAnalysisObjective = await selects.analysisObjective.functionGetData();
        const fetchTraitAnalysisPattern = await selects.traitAnalysisPattern.functionGetData();
        if (fetchAnalysisObjective.status === 200 && fetchTraitAnalysisPattern.status && isMounted) {
          dispatch(doDropdownInitial({
            analysisObjective: fetchAnalysisObjective.data,
            traitAnalysisPattern: fetchTraitAnalysisPattern.data,
          }))
        }
      }
      fetchData({ isMounted }).catch(ex => console.log('Error fetch analysis Objective and trait Analysis Pattern',ex))
      return () => {
        isMounted = false;
      };
    }, []);

    useEffect(() => {
      let isMounted = true;
      const fetchData = async ({ isMounted, experiments }) => {
        const fetchExperiment = await selects
          .experimentLocationAnalysisPattern
          .functionGetData({ experiments });
        if (fetchExperiment.status === 200 && isMounted) {
          dispatch(doDropdownExperiment({ experimentLocationAnalysisPattern: fetchExperiment.data }))
        }
      }
      fetchData({ isMounted, experiments }).catch(ex => console.log(ex))
      return () => {
        isMounted = false;
      };
    }, [experiments]);

    useEffect(() => {
      let isMounted = true;
      const fetchData = async ({
        isMounted,
        traitAnalysisPattern,
        experimentLocationAnalysisPattern,
        experiments,
      }) => {
        const fetchAnalysisConfiguration = await selects.analysisConfiguration
          .functionGetData({
            traitAnalysisPattern,
            experimentLocationAnalysisPattern,
            experiments,
          });
        if (fetchAnalysisConfiguration.status === 200 && isMounted) {
          dispatch(doDropdownPatternLocation({
            analysisConfiguration: fetchAnalysisConfiguration.data
          }));
        }
      }

      fetchData({
        isMounted,
        traitAnalysisPattern,
        experimentLocationAnalysisPattern,
        experiments,
      }).catch(ex => console.log(ex));
      return () => {
        isMounted = false;
      };
    }, [experiments, traitAnalysisPattern, experimentLocationAnalysisPattern]);

    useEffect(() => {
      let isMounted = true;
      const fetchData = async ({ isMounted, analysisConfiguration, experiments }) => {
        const resultMain = await selects.mainModel.functionGetData({
          analysisConfiguration,
        });
        const resultSpatial = await selects.spatialAdjusting.functionGetData({
          experiments,
          analysisConfiguration,
        });
        if (resultMain.status === 200 && resultSpatial.status === 200 && isMounted) {
          const resultMainOne =
            resultMain.data.length === 1
              ? {
                propertyId: resultMain.data[0].propertyId,
                propertyName: resultMain.data[0].propertyName,
                propertyCode: resultMain.data[0].propertyCode,
              }
              : null;
          const resultSpatialOne =
            resultSpatial.data.length === 1
              ? {
                propertyId: resultSpatial.data[0].propertyId,
                propertyName: resultSpatial.data[0].propertyName,
                propertyCode: resultSpatial.data[0].propertyCode,
              }
              : null;

          dispatch(doDropdownAnalysisConfiguration({
            dropdown: {
              mainModel: resultMain.data,
              spatialAdjusting: resultSpatial.data,
            },
            mainModel: resultMainOne,
            spatialAdjusting: resultSpatialOne,
          }));
        }
      };
      fetchData({
        isMounted,
        analysisConfiguration,
        experiments,
      }).catch(console.error);
      return () => {
        isMounted = false;
      };
    }, [experiments, analysisConfiguration]);

    const handleNext = async ({ newStep }) => {
      if (newStep === tabsArray.length) {
        const result = await request.functionSaveRequest({
          newRequestData: {
            analysisConfigPropertyId: analysisConfiguration !== null ? analysisConfiguration.propertyId : null,
            analysisObjectivePropertyId: analysisObjective.propertyId,
            analysisType: selects.analysisType.value,
            configFormulaPropertyId: mainModel !== null ? mainModel.propertyId : null,
            configResidualPropertyId: spatialAdjusting !== null ? spatialAdjusting.propertyId : null,
            crop: experiments[0].crop,
            dataSource: request.userConfiguration.data.dataSource,
            dataSourceAccessToken: TOKEN,
            dataSourceUrl: request.dataSourceUrl,
            experiments: experiments,
            expLocAnalysisPatternPropertyId:
              experimentLocationAnalysisPattern.propertyId,
            institute: request.userConfiguration.data.institute,
            name: nameRequest,
            predictionId: selects.prediction.functionGetData({
              propertyName: experimentLocationAnalysisPattern.propertyName,
            }),
            requestorId: request.userConfiguration.data.requestorId,
            traits: traits,
            traitAnalysisPatternPropertyId: traitAnalysisPattern.propertyId,
          },
        });
        if (result.status === 201) {
          history.push(setting.routes.rotuelist);
        } else {
          alert(result.message);
        }
      } else {
        dispatch(doNext());
      }
    };

    const handleBack = async ({ newStep }) => {
      if (newStep > -1) {
        dispatch(doBack());
      }
    };

    const handleSavedDataOccurrences = ({ experimentsSelected, traitsSelected }) => {
      dispatch(doAddOccurrence({ experimentsSelected, traitsSelected }))
    };

    const handleOnChangeTraits = ({ selectedOptions, data }) => {
      const result = data.filter((item) =>
        selectedOptions.map((select) => select.value).includes(item.variableDbId),
      );
      const selected = result.map((item) => ({
        traitId: item.variableDbId,
        traitName: item.variableAbbrev,
      }));
      dispatch(doAddTraits({ traits: selected }));
    };

    const handleOnChangeAnalysisObjective = ({ selectedOption, data }) => {
      const result = data.filter((item) => selectedOption === item.propertyId);
      const selected =
        result.length === 1
          ? {
            propertyId: result[0].propertyId,
            propertyName: result[0].propertyName,
            propertyCode: result[0].propertyCode,
          }
          : null;
      dispatch(doAddAnalysisObjective({
        analysisObjective: selected,
      }));
    };

    const handleOnChangeTraitAnalysisPattern = ({ selectedOption, data }) => {
      const result = data.filter((item) => selectedOption === item.propertyId);
      const selected =
        result.length === 1
          ? {
            propertyId: result[0].propertyId,
            propertyName: result[0].propertyName,
            propertyCode: result[0].propertyCode,
          }
          : null;
      dispatch(doAddTraitAnalysisPattern({ traitAnalysisPattern: selected }));
    };

    const handleOnChangeExperimentLocationAnalysisPattern = ({
      selectedOption,
      data,
    }) => {
      const result = data.filter((item) => selectedOption === item.propertyId);
      const selected =
        result.length === 1
          ? {
            propertyId: result[0].propertyId,
            propertyName: result[0].propertyName,
            propertyCode: result[0].propertyCode,
          }
          : null;

      dispatch(doAddExperimentLocationAnalysisPattern({
        experimentLocationAnalysisPattern: selected,
      }));
    };

    const handleOnChangeAnalysisConfiguration = ({ selectedOption, data }) => {
      const result = data.filter((item) => selectedOption === item.propertyId);
      const selected =
        result.length === 1
          ? {
            propertyId: result[0].propertyId,
            propertyName: result[0].propertyName,
            propertyCode: result[0].propertyCode,
          }
          : null;
      dispatch(doAddAnalysisConfiguration({ analysisConfiguration: selected }));
    };

    const handleOnChangeMainModel = ({ selectedOption, data }) => {
      const result = data.filter((item) => selectedOption === item.propertyId);
      const selected =
        result.length === 1
          ? {
            propertyId: result[0].propertyId,
            propertyName: result[0].propertyName,
            propertyCode: result[0].propertyCode,
          }
          : null;
      dispatch(doAddMainModel({
        mainModel: selected,
      }));
    };

    const handleOnChangeSpatialAdjusting = ({ selectedOption, data }) => {
      const result = data.filter((item) => selectedOption === item.propertyId);
      const selected =
        result.length === 1
          ? {
            propertyId: result[0].propertyId,
            propertyName: result[0].propertyName,
            propertyCode: result[0].propertyCode,
          }
          : null;
      dispatch(doAddSpatialAdjusting({
        spatialAdjusting: selected,
      }));
    };

    const localStorage_overwriteFilterRow = ({filter}) => {
      let filterRow = searchLocationRowFilter();
      try {
        filter.forEach((element) => {
          if (element.new !== null) filterRow[element.id].isActive = element.new;
        });
      } catch (ex) { }
      localStorage.setItem(
        setting.filterRow.keyStorageLocation,
        JSON.stringify(filterRow),
      );
      dispatch(doResetFilterRow({filterEnableRow: filterRow}));
    };

    function searchLocationRowFilter() {
      const stringJson = localStorage.getItem(setting.filterRow.keyStorageLocation);
      return stringJson !== undefined && stringJson !== null && stringJson !== ''
        ? JSON.parse(stringJson)
        : setting.filterRow.configuration;
    }

    return (
      <Grid
        ref={ref}
        data-testid={'AddRequestTabTestId'}
        className={classes.grid}
        container
        id={'AddRequestTabId'}
      >
        <Grid item xs={9} className={classes.gridSubtitle}>
          <SubTitleMolecule
            titleText={subtitle}
            message={information.message}
            messageerror={''}
            severity={information.severity}
          />
        </Grid>
        <Grid item xs={3} className={classes.gridButton}>
          <Grid
            container
            direction='row'
            justifyContent='flex-end'
            spacing={1}
            id={'AddRequestTabHeadId'}
          >
            <Grid item>
              <Button
                className='bg-ebs-green-default hover:bg-ebs-green-900 border-l-8 text-white'
                size={'small'}
                variant={'contained'}
                disabled={cancel.disabled}
                onClick={cancel.onClick}
              >
                {cancel.text}
              </Button>
            </Grid>
            <Grid item>
              <Button
                className='bg-ebs-green-default hover:bg-ebs-green-900 border-l-8 text-white'
                variant={'contained'}
                size={'small'}
                color={'secondary'}
                disabled={back.disabled}
                onClick={() => {
                  handleBack({ newStep: step - 1 });
                }}
              >
                {back.text}
              </Button>
            </Grid>
            <Grid item>
              <Button
                className='bg-ebs-green-default hover:bg-ebs-green-900 border-l-8 text-white'
                variant={'contained'}
                size={'small'}
                color={'primary'}
                disabled={next.disabled}
                onClick={() => {
                  handleNext({ newStep: step + 1 });
                }}
              >
                {next.text}
              </Button>
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={12} className={classes.gridTable} id={'AddRequestTabTabsId'}>
          <Tabs
            value={step}
            indicatorColor='primary'
            textColor='primary'
            //onChange={handleChange}
            aria-label='Add New Request'
          >
            {tabsArray.map((tab, index) => {
              const label = `${index + 1} ${tab}`.toString();
              return <Tab value={index} label={label} disabled key={label} style={{ fontSize: 'medium' }} />;
            })}
          </Tabs>
          <TabPanelMolecule step={step} index={0}>
            <ExperimentTabOrganism
              buttons={experiment.buttons}
              classes={classes}
              columns={experiment.columns}
              filterenablerow={filterEnableRow} //todo: return filter
              functiongetdata={experiment.functionGetExperiment}
              functionGetIntersectionTrait={
                selects.responseVariableTrait.functionGetData
              }
              functionGetTrait={experiment.functionGetTrait}
              functionresetfilterrow={localStorage_overwriteFilterRow}
              functionsaveselected={handleSavedDataOccurrences}
              style={experiment.style}
              user={request.userConfiguration}
            />
          </TabPanelMolecule>
          <TabPanelMolecule step={step} index={1}>
            <Grid container direction='column' justifyContent='space-between' alignItems='flex-start' spacing={spacingSelect}>
              <Grid item>
                <TraitAutoSelectMolecule
                  convertToItems={(item) => ({
                    value: item.variableDbId,
                    label: item.variableAbbrev,
                  })}
                  classes={classes}
                  data={dropdown.traits}
                  id={'traitId'}
                  onChange={handleOnChangeTraits}
                  selectAllLabel={'SELECT ALL'}
                  title={'Response variables'}
                />
              </Grid>
              <Grid item>
                <AutoSelectMolecule
                  convertToItems={(item) => ({
                    value: item.propertyId,
                    label: item.propertyName,
                  })}
                  classes={classes}
                  data={dropdown.analysisObjective}
                  id={'analysisObjectiveId'}
                  onChange={handleOnChangeAnalysisObjective}
                  title={selects.analysisObjective.title}
                />
              </Grid>
            </Grid>
          </TabPanelMolecule>
          <TabPanelMolecule step={step} index={2}>
            <Grid container direction='column' justifyContent='space-between' alignItems='flex-start' spacing={spacingSelect}>
              <Grid item>
                <AutoSelectMolecule
                  convertToItems={(item) => ({
                    value: item.propertyId,
                    label: item.propertyName,
                  })}
                  classes={classes}
                  data={dropdown.traitAnalysisPattern}
                  id={'traitAnalysisPatternId'}
                  onChange={handleOnChangeTraitAnalysisPattern}
                  title={selects.traitAnalysisPattern.title}
                />
              </Grid>
              <Grid item>
                <AutoSelectMolecule
                  convertToItems={(item) => ({
                    value: item.propertyId,
                    label: item.propertyName,
                  })}
                  classes={classes}
                  data={dropdown.experimentLocationAnalysisPattern}
                  id={'experimentLocationAnalysisPatternId'}
                  onChange={handleOnChangeExperimentLocationAnalysisPattern}
                  title={selects.experimentLocationAnalysisPattern.title}
                />
              </Grid>
              <Grid item>
                <Collapse in={experiments.length === 1 ? true : false} timeout='auto' unmountOnExit>
                  <Grid container direction='column' justifyContent='space-between' alignItems='flex-start' spacing={spacingSelect}>
                    <Grid item>
                      <AutoSelectMolecule
                        convertToItems={(item) => ({
                          value: item.propertyId,
                          label: item.propertyName,
                        })}
                        classes={classes}
                        data={dropdown.analysisConfiguration}
                        id={'analysisConfigurationId'}
                        onChange={handleOnChangeAnalysisConfiguration}
                        title={selects.analysisConfiguration.title}
                      />
                    </Grid>
                    <Grid item>
                      <AutoSelectMolecule
                        convertToItems={(item) => ({
                          value: item.propertyId,
                          label: item.propertyName,
                        })}
                        classes={classes}
                        data={dropdown.mainModel}
                        id={'mainModelId'}
                        onChange={handleOnChangeMainModel}
                        title={selects.mainModel.title}
                      />
                    </Grid>
                    <Grid item>
                      <AutoSelectMolecule
                        convertToItems={(item) => ({
                          value: item.propertyId,
                          label: item.propertyName,
                        })}
                        classes={classes}
                        data={dropdown.spatialAdjusting}
                        id={'spatialAdjustingId'}
                        onChange={handleOnChangeSpatialAdjusting}
                        title={selects.spatialAdjusting.title}
                      />
                    </Grid>
                  </Grid>
                </Collapse>
              </Grid>
            </Grid>
          </TabPanelMolecule>
        </Grid>
      </Grid>
    );
  },
);
// Type and required properties
AddRequestTabOrganism.propTypes = {
  classes: PropTypes.object.isRequired,
  experiment: PropTypes.object.isRequired,
  request: PropTypes.object.isRequired,
  selects: PropTypes.object.isRequired,
  setting: PropTypes.object.isRequired,
  subtitle: PropTypes.string.isRequired,
};
// Default properties
AddRequestTabOrganism.defaultProps = {
  subtitle: '',
};

export default AddRequestTabOrganism;
