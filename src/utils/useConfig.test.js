import { useConfig } from './useConfig';

jest.mock('@ebs/layout', () => {
    return {
        getUserProfile: jest.fn(() => (
            {
                "permissions": {
                    "applications": [
                        {
                            "prefix": "test1",
                            "products": [
                                {
                                    "name": "test1",
                                    "actions": [
                                        {
                                            'General': [
                                                "One",
                                                "Two",
                                            ]
                                        }
                                    ],
                                    "dataActions": [
                                        "Three",
                                        "Four",
                                    ]
                                }
                            ]
                        },
                    ]
                }
            }
        ))
    }
})

const { getPermissionAplication, canPermissionAplicacion } = useConfig();

describe('Get list of permission', () => {
    it('Get permison from user ', () => {
        const result = {
            actions: [{
                'General': [
                    "One",
                    "Two",
                ]
            }], dataActions: ['Three', 'Four']
        };
        const permission = getPermissionAplication({ prefix: 'test1', product: 'test1' });
        expect(permission).toStrictEqual(result)
    });
    it('There is no permission for the user', () => {
        const permission = getPermissionAplication({ prefix: 'test2', product: 'test2' });
        expect(permission).toStrictEqual(null)

    })

});

describe('Get one particular permission', () => {
    it('Ther is one particular permission', () => {
        const permission = canPermissionAplicacion({ prefix: 'test1', product: 'test1', type: 'actions', actionName: 'One' });
        expect(permission).toStrictEqual(true)

    })
    it('There is no one particular permission', () => {
        const permission = canPermissionAplicacion({ prefix: 'test1', product: 'test1', type: 'actions', actionName: 'Sevent' });
        expect(permission).toStrictEqual(false)

    })
});