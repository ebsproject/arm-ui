/**
 * @description - Const name obejctive
 */
export const AF_PROPERTIES_ANALYSIS_OBJECTIVE = 'objective';
/**
 * @description - Const name trait pattern
 */
export const AF_PROPERTIES_ANALYSIS_PATTERN = 'trait_pattern';
/**
 * @description - Const name experiment location analysis pattern
 */
export const AF_PROPERTIES_EXPERIMENT_LOCATION_ANALYSIS_PATTERN =
  'exptloc_analysis_pattern';
/**
 * @description - Const name detail main model formulas
 */
export const AF_PROPERTIES_ANALYSIS_CONFIGURATION_DETAIL_MAINMODEL = 'formulas';
/**
 * @description - Const name spacial adjusting
 */
export const AF_PROPERTIES_ANALYSIS_CONFIGURATION_DETAIL_SPACIALADJUSTING =
  'residuals';
/**
 * @description - ID of the database that defines the value 'Sinlge experiment and single location'
 */
export const ID_EXPERIMENT_LOCATION_ANALYSIS_PATTERN_SINGLE_E__SINGLE_L = 15;
/**
 * @description - ID of the database that defines the value 'Sinlge experiment and multi location'
 */
export const ID_EXPERIMENT_LOCATION_ANALYSIS_PATTERN_SINGLE_E__MULTI_L = 16;
/**
 * @description - ID of the database that defines the value 'Multi experiment and single location'
 */
export const ID_EXPERIMENT_LOCATION_ANALYSIS_PATTERN_MULTI_E__SINGLE_L = 17;
/**
 * @description - ID of the database that defines the value 'Multi experiment and multi location'
 */
export const ID_EXPERIMENT_LOCATION_ANALYSIS_PATTERN_MULTI_E__MULTI_L = 18;
/**
 * @description - ID of the database that defines the value 'Sinlge location'
 */
export const ID_PREDICTION_ELAP_SINLGE_LOCATION = 19;
/**
 * @description - ID of the database that defines the value 'Multi location'
 */
export const ID_PREDICTION_ELAP_MULTI_LOCATION = 20;
/**
 * @description - ID of the database that defines the value 'Univariante'
 */
export const ID_TRAIT_ANALYSIS_PATTERN_UNIVARIANTE = 10;
/**
 * @description - ID of the database that defines the value 'No spatial adjustment'
 */
export const ID_SPATIAL_ADJUSTED_NO_SPATIAL_ADJUSTMENT = '155';
export const NAME_SPATIAL_ADJUSTED_NO_SPATIAL_ADJUSTMENT ='No spatial adjustment'.toUpperCase();
/**
 * @description - Status of the request FAILURE
 */
export const STATE_REQUEST_FAILURE = 'FAILURE';
/**
 * @description - Status of the request PENDING
 */
export const STATE_REQUEST_PENDING = 'PENDING';
/**
 * @description - Status of the request DONE
 */
export const STATE_REQUEST_COMPLETE = 'DONE';
/**
 * @description - Status of the request IN-PROGRESS
 */
export const STATE_REQUEST_INPROGRESS = 'IN-PROGRESS';

/**
 * @description           Conver the Location from data base to alias 'multi' or single location
 * @param {object}        param0 - Parameter to conver propertyName
 * @param {object}        param0.experimentLocationAnalysisPattern - Value in string of experiment location analysis pattern
 * @returns {object}      result - Object to save teh result
 * @returns {string}      result.experiment [sinlge|multi] - If existe one experiment or multiple experiment
 * @returns {string}      result.location [sinlge|multi] - If existe one location or multiple location
 */
export function getExperimentOrLocationFromExperimentLocationAnalysisPattern({
  propertyName,
}) {
  let result = {
    experiment: '',
    location: '',
  };
  if (propertyName && propertyName !== '') {
    const experiment  = propertyName
      .toLowerCase()
      .trim()
      .split('-')[0]
      .trim()
      .split(' ')[0];
    result.experiment = experiment === 'multiple' ? 'multi' : experiment;
    const location = propertyName
      .toLowerCase()
      .trim()
      .split('-')[1]
      .trim()
      .split(' ')[0];
    result.location = location === 'multiple' ? 'multi' : location;
  }
  return result;
}

/**
 * @description       - Get the prediccion by only the location value
 * @param {string}    experimentLocationAnalysisPattern - Value in string of experiment location analysis pattern
 * @returns {string}  - Return ID if is single or multi in location parameter
 */
export function getPredictionByExpLoc({ propertyName }) {
  const location =
    propertyName !== ''
      ? getExperimentOrLocationFromExperimentLocationAnalysisPattern(propertyName)
          .location
      : '';
  return location === 'single'
    ? ID_PREDICTION_ELAP_SINLGE_LOCATION
    : ID_PREDICTION_ELAP_MULTI_LOCATION;
}

/**
 * @description       - Get the URL to get a especific propertie
 * @param {string}    propertyRoot [objective|trait_pattern|exptloc_analysis_pattern] - Property to search
 * @param {number}    page [0|number] - Pagination parameter to get page, by default 0
 * @param {string}    pageSize [100|number] - Pagination parameter to get number of element into a page, by default 100
 * @param {boolean}   isActive [true|false] - Paramter to search property active or not, by default it is active
 * @returns {string}  - URL to search the property
 */
export function GetUrlAFPropertiesSearch({
  propertyRoot,
  page = 0,
  pageSize = 100,
  isActive = true,
}) {
  let url =
    `properties?propertyRoot=${propertyRoot}&isActive=${isActive}&page=${page}&pageSize=${pageSize}`.toString();
  return url;
}

/**
 * @description       - Get the URL to get a analysis configuration.
 * @param {object}    - Parameter to build url to get Analysis configuration
 * @param {string}    traitPattern - Parameter traint pattern to search Analysis configuration
 * @param {string}    expAnalysisPattern - Parameter experiment analysis pattern to search Analysis configuration,
 * @param {string}    locAnalysisPattern - Parameter location analysis pattern to search Analysis configuration,
 * @param {string}    design - Parameter desing to search Analysis configuration,
 * @param {string}    engine [ASREML|SOMMER] - Parameter engine to search Analysis configuration,
 * @param {number}    page [0|number] - Pagination parameter to get page, by default 0
 * @param {string}    pageSize [100|number] - Pagination parameter to get number of element into a page, by default 100
 * @returns {string}  - URL to search the analysis configuration
 */
export function getUrlAFAnalysisConfigurationSearch({
  traitPattern,
  expAnalysisPattern,
  locAnalysisPattern,
  design,
  engine,
  page = 0,
  pageSize = 100,
}) {
  let parameters = new Array();
  let url = 'analysis-configs';
  if (traitPattern !== '') {
    parameters.push(`traitPattern=${traitPattern}`.toString());
  }
  if (expAnalysisPattern !== '') {
    parameters.push(`experimentAnalysisPattern=${expAnalysisPattern}`.toString());
  }
  if (locAnalysisPattern !== '') {
    parameters.push(`locationAnalysisPattern=${locAnalysisPattern}`.toString());
  }
  if (design !== '') {
    parameters.push(`design=${design}`.toString());
  }
  if (engine !== '' && engine !== undefined) {
    parameters.push(`engine=${engine}`.toString());
  }
  if (page !== '') {
    parameters.push(`page=${page}`.toString());
  }
  if (pageSize !== '') {
    parameters.push(`pageSize=${pageSize}`.toString());
  }
  if (parameters.length > 0) {
    url += '?' + parameters.join('&');
  }
  return url;
}

/**
 * @description       - Get the URL to get an ID  analysis configuration
 * @param {object}    default - Parameter to search the ID analysis configuration
 * @param {number}    default.analysisConfigurationId - ID of analysis configuration
 * @param {number}    default.analysisConfigurationDetail [formula|residual] - String to search in formula or residual
 * @returns {string}  - URL to search ID analysis configuration
 */
export function getUrlAFAnalysisConfigurationDetail({
  analysisConfigurationId,
  analysisConfigurationDetail,
}) {
  let url =
    `analysis-configs/${analysisConfigurationId}/${analysisConfigurationDetail}`.toString();
  return url;
}

/**
 * @description       - Get the URL to save the request (POST)
 * @returns {string}  - String ewith URL to request
 */
export function getUrlAFSaveNewRequest() {
  let url = `requests`.toString();
  return url;
}

/**
 * @description       - Get the URL to get the requests (GET)
 * @param {object}    default - Parameter to create the URL
 * @param {string}    default.requestorId - Id of requestor
 * @param {string}    default.crop - Name of crop
 * @param {string}    default.organization - Name of organization
 * @param {string}    default.status [PENDING|IN-PROGRESS|DONE|FAILURE] - Status of the request
 * @param {number}    page - Pagination parameter to get page
 * @param {string}    pageSize - Pagination parameter to get number of element into a page
 * @returns {string}  - URL to search the requests
 */
export function getUrlAFGETRequest({
  requestorId,
  crop,
  organization,
  status,
  page,
  pageSize,
}) {
  let url = `requests`.toString();
  let parameters = new Array();
  if (requestorId !== '') {
    parameters.push(`requestorId=${requestorId}`.toString());
  }
  if (crop !== '') {
    parameters.push(`crop=${crop}`.toString());
  }
  if (organization !== '') {
    parameters.push(`organization=${organization}`.toString());
  }
  if (status !== '') {
    parameters.push(`status=${status}`.toString());
  }
  if (page !== '') {
    parameters.push(`page=${page}`.toString());
  }
  if (pageSize !== '') {
    parameters.push(`pageSize=${pageSize}`.toString());
  }
  if (parameters.length > 0) {
    url += '?' + parameters.join('&');
  }
  return url;
}

/**
 * @description - Get the URL to get the request by ID (GET)
 * @returns {string} ulr - part of the url to get request by ID
 */
export function getUrlAFRequestID(requestID) {
  let url = `requests/${requestID}`.toString();
  return url;
}
