import PropTypes from 'prop-types';
import { getUserProfile } from '@ebs/layout';

export const useConfig = () => {

    const canPermissionAplicacion = ({ prefix, product, type, actionName }) => {
        const action = getPermissionAplication({ prefix, product });
        if (action === null) return false
        if (action.length === 0) return false
        for( const element of action[type] ){
            for (const [key, value] of Object.entries(element)) {
                const name = value.find(act => act === actionName) || '';
                if (name !== '') return true;
            }
        }
        return false;
    }

    const getPermissionAplication = ({ prefix, product }) => {
        const profile = getUserProfile();
        const { permissions: { applications } } = profile;
        const action = applications
            .find(application => application.prefix = prefix)
            .products.find(prod => prod.name === product) || null;
        return action === null ? null : { actions: action.actions, dataActions: action.dataActions };
    }

    canPermissionAplicacion.prototype = {
        prefix: PropTypes.string.isRequired,
        product: PropTypes.string.isRequired,
        type: PropTypes.oneOf(['actions', 'dataActions']).isRequired,
        actionName: PropTypes.string.isRequired,
    }

    getPermissionAplication.prototype = {
        prefix: PropTypes.string.isRequired,
        product: PropTypes.string.isRequired,
    }

    return {
        //Methods
        canPermissionAplicacion,
        getPermissionAplication,
    }
}

export default useConfig;