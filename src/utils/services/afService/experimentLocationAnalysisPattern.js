import clientAxios from 'utils/client/clientAxiosAF';
import { DATA_SET } from 'utils/config';
import {
  AF_PROPERTIES_EXPERIMENT_LOCATION_ANALYSIS_PATTERN,
  ID_EXPERIMENT_LOCATION_ANALYSIS_PATTERN_SINGLE_E__SINGLE_L,
  ID_EXPERIMENT_LOCATION_ANALYSIS_PATTERN_SINGLE_E__MULTI_L,
  GetUrlAFPropertiesSearch,
  ID_EXPERIMENT_LOCATION_ANALYSIS_PATTERN_MULTI_E__MULTI_L,
  ID_EXPERIMENT_LOCATION_ANALYSIS_PATTERN_MULTI_E__SINGLE_L,
} from 'utils/helpers/afHelper';

/**
 * @description       - Search in the API experiment location analysis pattern
 * @param {array}     param0 - Parameter to search in the API
 * @param {array}     param0.experiments - Array with experiments and occurrences
 * @returns {object}  - Information from API
 * @returns {number}  return0.status [200|201|400]  - Number of status GET - 200 => success, POST 201 => succes, 400 => Not found and 500 => error
 * @returns {string}  return0.message - Message of error
 * @returns {array}   return0.data - Data get of API
 * @returns {object}  return0.metadata - General information
 * @returns {object}  return0.metadata.pagination - Information about the pagination
 * @returns {number}  return0.metadata.pagination.totalPages - Total of pages
 * @returns {number}  return0.metadata.pagination.totalCount - Total of element in global request
 */
export async function getExperimentLocationAnalysisPattern({ experiments }) {
  let dataSet = DATA_SET();
  const parameter = {};
  const url = GetUrlAFPropertiesSearch({
    propertyRoot: AF_PROPERTIES_EXPERIMENT_LOCATION_ANALYSIS_PATTERN,
  });
  try {
    if (experiments.length !== 0) {
      let result = await clientAxios.get(url, parameter);
      dataSet.data = filterData({ experiments, data: result.data.result.data });
    }
    dataSet.status = 200;
  } catch (ex) {
    dataSet.status = 500;
    dataSet.message = 'Unable to view Experiment location analisys pattern.';
  }
  return dataSet;
}

/**
 * @description         Filte result according to multi | single experiment and multi | sinlge location
 * @param {object}      param0 - Object to filter [param0.data]
 * @param {array}       param0.experiments - New occurrences
 * @param {array}       param0.data - All new occurrences
 * @returns {array}     Array with experiemnt and occurrences
 */
function filterData({ experiments, data }) {
  const locations = [...new Set(experiments.map(experiment => experiment.occurrences.map(occurrence => occurrence.locationId)).flat())];
  let validId = [];
  if (experiments.length === 1) {
    validId.push(ID_EXPERIMENT_LOCATION_ANALYSIS_PATTERN_SINGLE_E__SINGLE_L)
    if(locations.length > 1)
      validId.push(ID_EXPERIMENT_LOCATION_ANALYSIS_PATTERN_SINGLE_E__MULTI_L)
  } else if (experiments.length > 1) {
    validId.push(ID_EXPERIMENT_LOCATION_ANALYSIS_PATTERN_MULTI_E__SINGLE_L)
    if(locations.length > 1)
       validId.push(ID_EXPERIMENT_LOCATION_ANALYSIS_PATTERN_MULTI_E__MULTI_L)
  }
  return data.filter((item) => validId.includes(item.propertyId));
}
