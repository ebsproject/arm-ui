import clientAxios from 'utils/client/clientAxiosAF';
import {
  getUrlAFSaveNewRequest,
  getUrlAFGETRequest,
  getUrlAFRequestID,
  STATE_REQUEST_COMPLETE,
  STATE_REQUEST_INPROGRESS,
  STATE_REQUEST_PENDING,
  STATE_REQUEST_FAILURE,
} from 'utils/helpers/afHelper';
import { DATA_SET, MULTIPLE_IN_GRID } from 'utils/config';
import { getUserProfileById } from '../coreService/user';
import { dateFormat, DATE_FORMAT_VALUE } from 'utils/helpers/baseHelper';
import { Box, Typography } from '@material-ui/core';
import { Fragment } from 'react';
import { StatusTagAtom } from 'components/ui/atoms/tags';

/**
 * @description -Function to FETCH all request that was created.
 * @returns {object} dataSet -Object with all information from request.
 * @returns {object} dataSet.status -Object with head inforamtion from request.
 * @returns {int} dataSet.status.status -Https request status.
 * @returns {string} dataSet.status.message -Error message.
 * @returns {array} dataSet.data -Data from request.
 * @returns {object} dataSet.metadata -Object with metadata inforamtion from request.
 * @returns {object} dataSet.pagination -Object with pagination inforamtion from request.
 * @returns {int} dataSet.pagination.totalPages -Total of page in the request.
 * @returns {int} dataSet.pagination.pageSize -Element by page.
 */
export async function getRequestList({
  requestorId,
  crop,
  organization,
  status,
  pageSize,
  page,
}) {
  let dataSet = DATA_SET();
  page = page === 0 ? 0 : page - 1;
  const url = getUrlAFGETRequest({
    requestorId: requestorId,
    crop: crop,
    organization: organization,
    status: status,
    pageSize: pageSize,
    page: page,
  });
  try {
    const result = await clientAxios.get(url);
    dataSet.data = await convertRequestData(result.data.result.data);
    dataSet.metadata.pagination.pageSize = pageSize;
    dataSet.status = 200;
    dataSet.metadata.pagination.totalPages = result.data.metadata.pagination.totalPages;
    dataSet.metadata.pagination.totalElements = result.data.metadata.pagination.totalCount;
  } catch (ex) {
    console.log('Error', ex);
    dataSet.message = `Unable to view list of request: ${ex.toString()}`;
    dataSet.status = 500;
  }
  return dataSet;
}

/**
 * @description     - Function to FETCH and save the new request data.
 * @param {object}  param0.newRequestData - New request to save in the service
 * @param {string}  param0.newRequestData.dataSource - Data source
 * @param {string}  param0.newRequestData.crop - Crop
 * @param {string}  param0.newRequestData.institute - Institute
 * @param {number}  param0.newRequestData.analysisType - ID analysis type
 * @param {number}  param0.newRequestData.requestorId - ID requestor
 * @param {string}  param0.newRequestData.dataSourceUrl - URL to get the data
 * @param {string}  param0.newRequestData.dataSourceAccessToken - Barer token for access
 * @param {array}   param0.newRequestData.experiments - Array of experiments
 * @param {number}  param0.newRequestData.experiments[].experimentId - ID of experiment
 * @param {string}  param0.newRequestData.experiments[].experimentName - Name of experiment
 * @param {array}   param0.newRequestData.experiments[].occurrences - Array of occurrences
 * @param {number}  param0.newRequestData.experiments[].occurrences[].occurrenceId - ID of occurrence
 * @param {string}  param0.newRequestData.experiments[].occurrences[].occurrenceName - Name of occurrence
 * @param {string}  param0.newRequestData.experiments[].occurrences[].locationName - Name of location of occurrence
 * @param {number}  param0.newRequestData.experiments[].occurrences[].locationId - ID of location
 * @param {array}   param0.newRequestData.traits - Array of traits
 * @param {number}  param0.newRequestData.traits[].traitId - ID of trait
 * @param {string}  param0.newRequestData.traits[].traitName - Name of trait
 * @param {number}  param0.newRequestData.traits[].analysisObjectivePropertyId - ID of analysis objective property
 * @param {number}  param0.newRequestData.analysisConfigPropertyId - ID of analysis configuration
 * @param {number}  param0.newRequestData.expLocAnalysisPatternPropertyId - ID of experiment location analysis pattern,
 * @param {number}  param0.newRequestData.configFormulaPropertyId - ID of main model
 * @param {number}  param0.newRequestData.configResidualPropertyId - ID of spatial adjusting
 * @param {number}  param0.newRequestData.predictionId - ID pf prediction
 * @returns {object}  return0.status  - Object status about the request
 * @returns {number}  return0.status.status [200|201|400]  - Number of status GET - 200 => success, POST 201 => succes, 400 => Not found and 500 => error
 * @returns {string}  return0.status.message - Message of error
 * @returns {array}   return0.data - Data get of API
 * @returns {object}  return0.metadata - General information
 * @returns {object}  return0.metadata.pagination - Information about the pagination
 * @returns {number}  return0.metadata.pagination.totalPages - Total of pages
 * @returns {number}  return0.metadata.pagination.totalCount - Total of element in global request
 */
export async function postSaveNewRequest({ newRequestData }) {
  let dataSet = DATA_SET();
  const url = getUrlAFSaveNewRequest();
  const parameter = newRequestData;
  try {
    let result = await clientAxios.post(url, parameter);
    dataSet.status = 201;
  } catch (ex) {
    if (ex.response !== undefined) {
      dataSet.status =
        ex.response.status !== undefined ? ex.response.status : 500;
    } else {
      dataSet.status = 500;
    }
    dataSet.message = ex.toString();
  }
  return dataSet;
}

/**
 * @description       - Function to FETCH to get a specific request by ID.
 * @param {string}    requestID - ID of request to search
 * @returns {object}  - Information about API
 * @returns {object}  return0.status  - Object status about the request
 * @returns {number}  return0.status.status [200|201|400]  - Number of status GET - 200 => success, POST 201 => succes, 400 => Not found and 500 => error
 * @returns {string}  return0.status.message - Message of error
 * @returns {array}   return0.data - Data get of API
 * @returns {object}  return0.metadata - General information
 * @returns {object}  return0.metadata.pagination - Information about the pagination
 * @returns {number}  return0.metadata.pagination.totalPages - Total of pages
 * @returns {number}  return0.metadata.pagination.totalCount - Total of element in global request
 */
export async function getRequestID(requestID) {
  let dataSet = DATA_SET();
  const url = getUrlAFRequestID(requestID);
  try {
    const result = await clientAxios.get(url);
    dataSet.data = await convertRequestData([result.data.result]);
    dataSet.status.status = 200;
  } catch (ex) {
    dataSet.message = `Unable to view request: ${ex.toString()}`;
    dataSet.status = 500;
  }
  return dataSet;
}

/**
 * @description                 - Array with Request Column IDs and Properties
 * @param {array}               param0.classes - Classes to put spesific style with makestyle
 * @returns {array}             - Array of definition fo columns
 * @returns {string|component}  return0[].Header - Title of the column
 * @returns {string}            return0[].accessor - Name of column in data rows
 * @returns {boolean}           return0[].disableFilters - Disable filter in the column
 * @returns {boolean}           return0[].disableGlobalFilter - Disable global filter in the column
 * @returns {boolean}           return0[].hidden - Hidden column
 * @returns {string}            return0[].csvHeader - Name of column to exporte a csv
 * @returns {number}            return0[].width - Default width of column
 * @returns {function}          return0[].Filter - Function to filter that return a component with params({ column: { filterValue, setFilter, preFilteredRows, id } })
 * @returns {function}          return0[].Cell - Function to display a component in a cell taht retrun a component with params ({ value })
 */
export const getRequestListColumns = ({ classes }) => {
  return [
    {
      Header: 'Request Id',
      accessor: 'requestId',
      disableFilters: false,
      disableGlobalFilter: true,
      hidden: true,
      csvHeader: 'Request Id',
      width: 250,
    },
    {
      Header: 'Request name',
      accessor: 'name',
      disableFilters: false,
      disableGlobalFilter: true,
      hidden: false,
      csvHeader: 'Request Id',
      width: 350,
    },
    {
      Header: 'Status',
      accessor: 'status',
      disableFilters: false,
      disableGlobalFilter: false,
      hidden: false,
      csvHeader: 'Status',
      width: 200,
      Filter: ({ column: { filterValue, setFilter, preFilteredRows, id } }) => {
        setFilter;
        return (
          <Fragment>
            <select
              value={filterValue}
              onChange={(e) => {
                setFilter(e.target.value || undefined);
              }}
            >
              <option value=''></option>
              <option value={STATE_REQUEST_COMPLETE}>
                {STATE_REQUEST_COMPLETE}
              </option>
              <option value={STATE_REQUEST_INPROGRESS}>
                {STATE_REQUEST_INPROGRESS}
              </option>
              <option value={STATE_REQUEST_PENDING}>{STATE_REQUEST_PENDING}</option>
              <option value={STATE_REQUEST_FAILURE}>{STATE_REQUEST_FAILURE}</option>
            </select>
          </Fragment>
        );
      },
      Cell: ({ cell, value, ...others }) => {
        if (cell.isAggregated > 0) {
          return <Fragment></Fragment>;
        } else {
          return (
            <Fragment>
              <StatusTagAtom classes={classes} text={`${value}`.toString()} />
            </Fragment>
          );
        }
      },
    },
    {
      Header: 'Requestor ID',
      accessor: 'requestorId',
      disableFilters: true,
      disableGlobalFilter: true,
      hidden: true,
      csvHeader: 'Requestor ID',
    },
    {
      Header: 'Institute',
      accessor: 'institute',
      disableFilters: false,
      disableGlobalFilter: false,
      hidden: false,
      csvHeader: 'Institute',
      width: 100,
    },
    {
      Header: 'Crop',
      accessor: 'crop',
      disableFilters: false,
      disableGlobalFilter: false,
      hidden: false,
      csvHeader: 'Crop',
      width: 100,
    },
    {
      Header: 'Created-Text',
      accessor: 'createdOn',
      disableFilters: true,
      disableGlobalFilter: true,
      hidden: true,
    },
    {
      Header: 'Created',
      accessor: 'createdOnFormtat',
      disableFilters: false,
      disableGlobalFilter: false,
      hidden: false,
      csvHeader: 'Created',
      width: 120,
    },
    {
      Header: 'Analysis Type',
      accessor: 'analysisType',
      disableFilters: false,
      disableGlobalFilter: false,
      hidden: false,
      csvHeader: 'Analysis Type',
      width: 130,
    },
    {
      Header: 'resultDownloadRelativeUrl',
      accessor: 'resultDownloadRelativeUrl',
      disableFilters: true,
      disableGlobalFilter: true,
      hidden: true,
    },
    {
      Header: 'Experiments',
      accessor: 'experimentsGrid',
      disableFilters: false,
      disableGlobalFilter: false,
      hidden: false,
      csvHeader: 'Experiments',
      width: 110,
    },
    {
      Header: 'Location',
      accessor: 'locationGrid',
      disableFilters: false,
      disableGlobalFilter: false,
      hidden: false,
      csvHeader: 'Location',
      width: 80,
    },
    {
      Header: 'Trait',
      accessor: 'traitGrid',
      disableFilters: false,
      disableGlobalFilter: false,
      hidden: false,
      csvHeader: 'Trait',
      width: 80,
    },
    {
      Header: 'Model',
      accessor: 'model',
      disableFilters: false,
      disableGlobalFilter: false,
      hidden: false,
      csvHeader: 'Model',
      width: 200,
    },
    {
      Header: 'Spatial',
      accessor: 'spatial',
      disableFilters: false,
      disableGlobalFilter: false,
      hidden: false,
      csvHeader: 'Spatial',
      width: 200,
    },
    {
      Header: 'Objective',
      accessor: 'objective',
      disableFilters: false,
      disableGlobalFilter: false,
      hidden: false,
      csvHeader: 'Objective',
      width: 80,
    },
  ];
};

/**
 * @description     - Convert of data from API to show into grid
 * @param {array}   datas - Data from API
 * @returns {array} - Data converted to show into grid
 */
async function convertRequestData(datas) {
  const requestorsAll = await searchRequestor(datas);
  let newDatas = datas.map((data) => {
    //####### Only for not collapse page adding other atributes
    //# validate if exist experiments
    if (!data.hasOwnProperty('experiments')) {
      data.experiments = [
        {
          experimentName: '',
          occurrences: [{ occurrenceName: '', locationName: '' }],
        },
      ];
    }
    //# validate if exist traits
    if (!data.hasOwnProperty('traits')) {
      data.traits = [{ traitName: '' }];
    }
    //# validate if exist config
    if (!data.hasOwnProperty('configFormulaProperty')) {
      data.configFormulaProperty = { label: '' };
    }
    if (!data.hasOwnProperty('analysisObjectiveProperty')) {
      data.analysisObjectiveProperty = { propertyName: '' };
    }
    if (!data.hasOwnProperty('configResidualProperty')) {
      data.configResidualProperty = { label: '' };
    }
    if (!data.hasOwnProperty('expLocAnalysisPatternProperty')) {
      data.expLocAnalysisPatternProperty = { propertyName: '' };
    }
    if (!data.hasOwnProperty('traitAnalysisPatternProperty')) {
      data.traitAnalysisPatternProperty = { propertyName: '' };
    }
    const locationDistinc = searchDistincLocationName(data.experiments);
    data.locationGrid =
      locationDistinc.length === 1 ? locationDistinc[0] : MULTIPLE_IN_GRID;
    data.experimentsGrid =
      data.experiments.length === 1
        ? data.experiments[0].experimentName
        : MULTIPLE_IN_GRID;
    data.traitGrid =
      data.traits.length === 1 ? data.traits[0].traitName : MULTIPLE_IN_GRID;
    data.model = data.configFormulaProperty.label;
    data.spatial = data.configResidualProperty.label;
    data.objective = data.analysisObjectiveProperty.propertyName;
    const requestoSearch = requestorsAll.find(
      (requestor) => requestor.requestorId === data.requestorId,
    );
    data.requestorName =
      requestoSearch !== undefined ? requestoSearch.requestorName : '';
    data.createdOnFormtat = dateFormat(data.createdOn, DATE_FORMAT_VALUE.monthENUS);
    return data;
  });
  return newDatas;
}

/**
 * @description     - Filter unique to location into array of experiment
 * @param {array}   experiments - Array of experiment
 * @returns {array} - List of location unique
 */
function searchDistincLocationName(experiments) {
  let locationDistinc = [];
  for (const experiment of experiments) {
    for (const occurrence of experiment.occurrences) {
      if (
        locationDistinc.findIndex(
          (location) => location === occurrence.locationName,
        ) === -1
      ) {
        locationDistinc.push(occurrence.locationName);
      }
    }
  }

  return locationDistinc;
}

/**
 * @description - Search all information from every requestor ID
 * @param {array} datas - Array with attribute requestorID
 * @returns {arraya} requestorsAll - Array with every requestor information
 */
async function searchRequestor(datas) {
  let requestorId = [];
  let requestorsAll = [];
  datas.forEach((data) => {
    requestorId.push(data.requestorId);
  });
  requestorId = [...new Set(requestorId)];
  for (const requestor of requestorId) {
    const requestProfile = await getUserProfileById(requestor);
    requestorsAll.push(requestProfile.data);
  }
  return requestorsAll;
}
