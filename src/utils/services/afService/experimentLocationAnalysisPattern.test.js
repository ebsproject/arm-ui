import { ID_EXPERIMENT_LOCATION_ANALYSIS_PATTERN_MULTI_E__MULTI_L, ID_EXPERIMENT_LOCATION_ANALYSIS_PATTERN_MULTI_E__SINGLE_L, ID_EXPERIMENT_LOCATION_ANALYSIS_PATTERN_SINGLE_E__MULTI_L, ID_EXPERIMENT_LOCATION_ANALYSIS_PATTERN_SINGLE_E__SINGLE_L } from 'utils/helpers/afHelper';
import { getExperimentLocationAnalysisPattern } from './experimentLocationAnalysisPattern'


jest.mock('utils/client/clientAxiosAF', () => {
  return {
    get: jest
      .fn(params =>
      (
        {
          data: {
            metadata: {
              pagination: {
                currentPage: 0,
                pageSize: 100,
                totalCount: 4,
                totalPages: 1
              }
            },
            result: {
              data: [
                {
                  createdBy: '',
                  createdOn: '',
                  isActive: true,
                  label: null,
                  modifiedBy: '',
                  modifiedOn: '',
                  propertyCode: 'MESL',
                  propertyId: 17,
                  propertyName: 'Multiple Experiment - Single Location',
                  statement: '',
                  type: 'catalog_item'
                },
                {
                  createdBy: '',
                  createdOn: '',
                  isActive: true,
                  label: null,
                  modifiedBy: '',
                  modifiedOn: '',
                  propertyCode: 'MEML',
                  propertyId: 18,
                  propertyName: 'Multiple Experiment - Multiple Location',
                  statement: '',
                  type: 'catalog_item'
                },
                {
                  createdBy: '',
                  createdOn: '',
                  isActive: true,
                  label: null,
                  modifiedBy: '',
                  modifiedOn: '',
                  propertyCode: 'SEML',
                  propertyId: 16,
                  propertyName: 'Single Experiment - Multiple Location',
                  statement: '',
                  type: 'catalog_item'
                },
                {
                  createdBy: '',
                  createdOn: '',
                  isActive: true,
                  label: null,
                  modifiedBy: '',
                  modifiedOn: '',
                  propertyCode: 'SESL',
                  propertyId: 15,
                  propertyName: 'Single Experiment - Single Location',
                  statement: '',
                  type: 'catalog_item'
                }]
            }
          }
        }
      )
      )
  }
})

const experiments = {
  sEsL: [
    {
      crop: 'Maize',
      experimentId: 245,
      experimentName: 'KE-AYT-2021-DS-001',
      occurrences: [
        {
          occurrenceId: 3401,
          occurrenceName: 'KE-AYT-2021-DS-001-001',
          locationName: 'TEST_1',
          locationId: 3401,
          isPlotXY: true,
        },
      ],
      experimentDesignType: 'RCBD',
    },
  ],
  sEmL: [
    {
      crop: 'M',
      experimentId: 1,
      experimentName: '1',
      occurrences: [
        {
          occurrenceId: 11,
          occurrenceName: '11',
          locationName: '21',
          locationId: 21,
          isPlotXY: true,
        },
        {
          occurrenceId: 12,
          occurrenceName: '12',
          locationName: '22',
          locationId: 22,
          isPlotXY: true,
        },
      ],
      experimentDesignType: 'RCBD',
    },
  ],
  mEsL: [
    {
      crop: 'M',
      experimentId: 1,
      experimentName: '1',
      occurrences: [
        {
          occurrenceId: 11,
          occurrenceName: '11',
          locationName: '111',
          locationId: 111,
          isPlotXY: true,
        },
      ],
      experimentDesignType: 'RCBD',
    },
    {
      crop: 'M',
      experimentId: 2,
      experimentName: '2',
      occurrences: [
        {
          occurrenceId: 21,
          occurrenceName: '21',
          locationName: '111',
          locationId: 111,
          isPlotXY: true,
        },
      ],
      experimentDesignType: 'RCBD',
    },
  ],
  mEmL: [
    {
      crop: 'M',
      experimentId: 1,
      experimentName: '1',
      occurrences: [
        {
          occurrenceId: 11,
          occurrenceName: '11',
          locationName: '111',
          locationId: 111,
          isPlotXY: true,
        },
      ],
      experimentDesignType: 'RCBD',
    },
    {
      crop: 'M',
      experimentId: 2,
      experimentName: '2',
      occurrences: [
        {
          occurrenceId: 21,
          occurrenceName: '21',
          locationName: '211',
          locationId: 211,
          isPlotXY: true,
        },
      ],
      experimentDesignType: 'RCBD',
    },
  ],
}

test('Test service get experiment Location Analysis Pattern - single experiment single location', async () => {
  const result = await getExperimentLocationAnalysisPattern({
    experiments: experiments.sEsL,
  });
  expect(result.status).toBe(200);
  expect(result.data).toEqual(expect.arrayContaining([expect.objectContaining({propertyId: ID_EXPERIMENT_LOCATION_ANALYSIS_PATTERN_SINGLE_E__SINGLE_L})]));
  expect(result.data).not.toEqual(expect.arrayContaining([expect.objectContaining({propertyId: ID_EXPERIMENT_LOCATION_ANALYSIS_PATTERN_SINGLE_E__MULTI_L})]));
  expect(result.data).not.toEqual(expect.arrayContaining([expect.objectContaining({propertyId: ID_EXPERIMENT_LOCATION_ANALYSIS_PATTERN_MULTI_E__SINGLE_L})]));
  expect(result.data).not.toEqual(expect.arrayContaining([expect.objectContaining({propertyId: ID_EXPERIMENT_LOCATION_ANALYSIS_PATTERN_MULTI_E__MULTI_L})]));
});

test('Test service get experiment Location Analysis Pattern - single experiment multi location ', async () => {
  const result = await getExperimentLocationAnalysisPattern({
    experiments: experiments.sEmL,
  });
  expect(result.status).toBe(200);
  expect(result.data).toEqual(expect.arrayContaining([expect.objectContaining({propertyId: ID_EXPERIMENT_LOCATION_ANALYSIS_PATTERN_SINGLE_E__MULTI_L})]));
  expect(result.data).toEqual(expect.arrayContaining([expect.objectContaining({propertyId: ID_EXPERIMENT_LOCATION_ANALYSIS_PATTERN_SINGLE_E__SINGLE_L})]));
  expect(result.data).not.toEqual(expect.arrayContaining([expect.objectContaining({propertyId: ID_EXPERIMENT_LOCATION_ANALYSIS_PATTERN_MULTI_E__SINGLE_L})]));
  expect(result.data).not.toEqual(expect.arrayContaining([expect.objectContaining({propertyId: ID_EXPERIMENT_LOCATION_ANALYSIS_PATTERN_MULTI_E__MULTI_L})]));
});

test('Test service get experiment Location Analysis Pattern - multi experiment sinlge location', async () => {
  const result = await getExperimentLocationAnalysisPattern({
    experiments: experiments.mEsL,
  });
  expect(result.status).toBe(200);
  expect(result.data).toEqual(expect.arrayContaining([expect.objectContaining({propertyId: ID_EXPERIMENT_LOCATION_ANALYSIS_PATTERN_MULTI_E__SINGLE_L})]));
  expect(result.data).not.toEqual(expect.arrayContaining([expect.objectContaining({propertyId: ID_EXPERIMENT_LOCATION_ANALYSIS_PATTERN_MULTI_E__MULTI_L})]));
  expect(result.data).not.toEqual(expect.arrayContaining([expect.objectContaining({propertyId: ID_EXPERIMENT_LOCATION_ANALYSIS_PATTERN_SINGLE_E__SINGLE_L})]));
  expect(result.data).not.toEqual(expect.arrayContaining([expect.objectContaining({propertyId: ID_EXPERIMENT_LOCATION_ANALYSIS_PATTERN_SINGLE_E__MULTI_L})]));
});

test('Test service get experiment Location Analysis Pattern - multi experiment multi location', async () => {
  const result = await getExperimentLocationAnalysisPattern({
    experiments: experiments.mEmL,
  });
  expect(result.status).toBe(200);
  expect(result.data).toEqual(expect.arrayContaining([expect.objectContaining({propertyId: ID_EXPERIMENT_LOCATION_ANALYSIS_PATTERN_MULTI_E__SINGLE_L})]));
  //expect(result.data).not.toEqual(expect.arrayContaining([expect.objectContaining({propertyId: ID_EXPERIMENT_LOCATION_ANALYSIS_PATTERN_MULTI_E__MULTI_L})]));
  expect(result.data).toEqual(expect.arrayContaining([expect.objectContaining({propertyId: ID_EXPERIMENT_LOCATION_ANALYSIS_PATTERN_MULTI_E__MULTI_L})]));
  expect(result.data).not.toEqual(expect.arrayContaining([expect.objectContaining({propertyId: ID_EXPERIMENT_LOCATION_ANALYSIS_PATTERN_SINGLE_E__SINGLE_L})]));
  expect(result.data).not.toEqual(expect.arrayContaining([expect.objectContaining({propertyId: ID_EXPERIMENT_LOCATION_ANALYSIS_PATTERN_SINGLE_E__MULTI_L})]));
});