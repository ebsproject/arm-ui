import clientAxios from 'utils/client/clientAxiosAF';
import { DATA_SET } from 'utils/config';
import {
  getExperimentOrLocationFromExperimentLocationAnalysisPattern,
  getUrlAFAnalysisConfigurationSearch,
} from 'utils/helpers/afHelper';

/**
 * @description         Search the Analysis Configuration
 * @param {object}      param0 - Parameter to search
 * @param {object}      param0.traitAnalysisPattern - Trait Analysis pattern
 * @param {object}      param0.experimentLocationAnalysisPattern - Experiment Location Analysis Pattern
 * @param {array}       param0.occurrences - List of occurrences
 * @returns {object}    Information from API
 * @returns {number}    return0.status [200|201|400]  - Number of status GET - 200 => success, POST 201 => succes, 400 => Not found and 500 => error
 * @returns {string}    return0.message - Message of error
 * @returns {array}     return0.data - Data get of API
 * @returns {object}    return0.metadata - General information
 * @returns {object}    return0.metadata.pagination - Information about the pagination
 * @returns {number}    return0.metadata.pagination.totalPages - Total of pages
 * @returns {number}    return0.metadata.pagination.totalCount - Total of element in global request
 */
export async function getAnalysisConfiguration({
  experimentLocationAnalysisPattern,
  experiments,
  traitAnalysisPattern,
}) {
  let dataSet = DATA_SET();
  const parameter = {};
  if (
    traitAnalysisPattern !== null &&
    experimentLocationAnalysisPattern !== null &&
    experiments.length > 0
  ) {
    const traitPattern = traitAnalysisPattern.propertyName.toLowerCase();
    const expAnalysisPattern =
      getExperimentOrLocationFromExperimentLocationAnalysisPattern({
        propertyName: experimentLocationAnalysisPattern.propertyName,
      }).experiment;
    const locAnalysisPattern =
      getExperimentOrLocationFromExperimentLocationAnalysisPattern({
        propertyName: experimentLocationAnalysisPattern.propertyName,
      }).location;
    const design = getDesign({ experiments });
    const url = getUrlAFAnalysisConfigurationSearch({
      traitPattern: traitPattern,
      expAnalysisPattern: expAnalysisPattern,
      locAnalysisPattern: locAnalysisPattern,
      design: design,
    });
    try {
      let result = await clientAxios.get(url, parameter);
      dataSet.data = result.data.result.data;
      dataSet.status = 200;
    } catch (ex) {
        dataSet.status = 500;
        dataSet.message = 'Unable to view Analysis configuration.';
    }
  }
  return dataSet;
}

/**
 * @description       Find in the occurrences all desing
 * @param {object}    param0 - Parametor to search
 * @param {array}     param0.experiments - Array with all experiment and occurrences
 * @returns {string}  Desing of occurences
 */
function getDesign({ experiments }) {
  let design = '';
  if (experiments.length > 0) {
    design = experiments[0].experimentDesignType;
    const arrayDesign = design.split('-');
    let arrayUpperDesing = [];
    if (arrayDesign.length > 0) {
      arrayDesign.forEach((element) => {
        arrayUpperDesing.push(
          `${element.charAt(0).toUpperCase()}${element.slice(1)}`.toString(),
        );
      });
      design = arrayUpperDesing.join('-');
    }
  }
  return design;
}
