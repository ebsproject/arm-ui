import {getSpatialAdjusting} from './spacialAdjusting';


const data = {
  simple: [
    {
      createdBy: null,
      createdOn: '2021-10-06T01:27:24.112888',
      description: 'Autoregressive order 1 spatial structure (AR1row x AR1col)',
      isActive: true,
      label: '(AR1row x AR1col)',
      modifiedBy: null,
      modifiedOn: null,
      propertyCode: 'residual_opt1',
      propertyId: '152',
      propertyName: '(AR1row x AR1col)',
      statement: 'ar1(row).ar1(col)',
      type: 'catalog_item',
    },
    {
      createdBy: null,
      createdOn: '2021-10-06T01:27:24.112888',
      description:
        'Autoregressive order 1 spatial structure for columns (IDrow x AR1col)',
      isActive: true,
      label: '(IDrow x AR1col)',
      modifiedBy: null,
      modifiedOn: null,
      propertyCode: 'residual_opt2',
      propertyId: '153',
      propertyName: '(IDrow x AR1col)',
      statement: 'idv(row).ar1(col)',
      type: 'catalog_item',
    },
    {
      createdBy: null,
      createdOn: '2021-10-06T01:27:24.112888',
      description:
        'Autoregressive order 1 spatial structure for rows (AR1row x IDcol)',
      isActive: true,
      label: '(AR1row x IDcol)',
      modifiedBy: null,
      modifiedOn: null,
      propertyCode: 'residual_opt3',
      propertyId: '154',
      propertyName: '(AR1row x IDcol)',
      statement: 'ar1(row).idv(col)',
      type: 'catalog_item',
    },
    {
      createdBy: null,
      createdOn: '2021-10-06T01:27:24.112888',
      description: null,
      isActive: true,
      label: 'No spatial adjustment',
      modifiedBy: null,
      modifiedOn: null,
      propertyCode: 'residual_opt4',
      propertyId: '155',
      propertyName: 'No spatial adjustment',
      statement: 'units',
      type: 'catalog_item',
    },
  ],
  multi: [
    {
      createdBy: null,
      createdOn: '2021-10-06T01:27:24.112888',
      description:
        'Autoregressive order 1 spatial structure (AR1row x AR1col), by location',
      isActive: true,
      label: '(AR1row x AR1col) by location',
      modifiedBy: null,
      modifiedOn: null,
      propertyCode: 'residual_opt5',
      propertyId: '156',
      propertyName: '(AR1row x AR1col) by location',
      statement: 'sat(loc).ar1(row).ar1(col)',
      type: 'catalog_item',
    },
    {
      createdBy: null,
      createdOn: '2021-10-06T01:27:24.112888',
      description:
        'Autoregressive order 1 spatial structure for columns (IDrow x AR1col), by location',
      isActive: true,
      label: '(IDrow x AR1col) by location',
      modifiedBy: null,
      modifiedOn: null,
      propertyCode: 'residual_opt6',
      propertyId: '157',
      propertyName: '(IDrow x AR1col) by location',
      statement: 'sat(loc).idv(row).ar1(col)',
      type: 'catalog_item',
    },
    {
      createdBy: null,
      createdOn: '2021-10-06T01:27:24.112888',
      description:
        'Autoregressive order 1 spatial structure for rows (AR1row x IDcol), by location',
      isActive: true,
      label: '(AR1row x IDcol) by location',
      modifiedBy: null,
      modifiedOn: null,
      propertyCode: 'residual_opt7',
      propertyId: '158',
      propertyName: '(AR1row x IDcol) by location',
      statement: 'sat(loc).ar1(row).idv(col)',
      type: 'catalog_item',
    },
    {
      createdBy: '1',
      createdOn: '2021-10-06T01:27:24.633081',
      description:
        'No spatial adjustment - heterogeneous error variance across locations, block diagonal',
      isActive: true,
      label: 'No spatial adjustment - heterogeneous error variance across locations',
      modifiedBy: null,
      modifiedOn: null,
      propertyCode: 'residual_opt8',
      propertyId: '192',
      propertyName:
        'No spatial adjustment - heterogeneous error variance across locations',
      statement: 'sat(loc).idv(units)',
      type: 'catalog_item',
    },
  ],
};

jest.mock('utils/client/clientAxiosAF', () => {
  return {
    get: jest
      .fn()
      .mockImplementationOnce(() => {
        return {
          data: {
            result: {
              data: data.simple,
            },
            metadata: {
              pagination: {
                totalPages: 0,
                totalCount: 0,
              },
            },
          },
        };
      }).mockImplementationOnce(() => {
        return {
          data: {
            result: {
              data: data.simple,
            },
            metadata: {
              pagination: {
                totalPages: 0,
                totalCount: 0,
              },
            },
          },
        };
      }).mockImplementationOnce(() => {
        return {
          data: {
            result: {
              data: data.multi,
            },
            metadata: {
              pagination: {
                totalPages: 0,
                totalCount: 0,
              },
            },
          },
        };
      }).mockImplementationOnce(() => {
        return {
          data: {
            result: {
              data: data.multi,
            },
            metadata: {
              pagination: {
                totalPages: 0,
                totalCount: 0,
              },
            },
          },
        };
      })
    };
});

describe('Test for Spatial Adjusting single', () => {
  it('Search spatial adjusting with PlotXY === true', async () =>{
    const result = await getSpatialAdjusting({
      experiments: [{experimentDbId: 1, occurrences: [{occurrenceDbId: 1, isPlotXY: true }] }],
      analysisConfiguration: { propertyId: 0 },
    });
    expect(result.data).toBe(data.simple);
  });
  it('Search spatial adjusting with PlotXY === true', async () =>{
    const result = await getSpatialAdjusting({
      experiments: [{experimentDbId: 1, occurrences: [{occurrenceDbId: 1, isPlotXY: false }] }],
      analysisConfiguration: { propertyId: 0 },
    });
    expect(result.data).not.toBe(data.simple);
    expect(result.data.length).toBe(1);
  });
});

describe('Test for Spatial Adjusting multi', () => {
  it('Search spatial adjusting with PlotXY === true', async () =>{
    const result = await getSpatialAdjusting({
      experiments: [{experimentDbId: 1, occurrences: [{occurrenceDbId: 1, isPlotXY: true }] }],
      analysisConfiguration: { propertyId: 0 },
    });
    expect(result.data).toBe(data.multi);
  });
  it('Search spatial adjusting with PlotXY === true', async () =>{
    const result = await getSpatialAdjusting({
      experiments: [{experimentDbId: 1, occurrences: [{occurrenceDbId: 1, isPlotXY: false }] }],
      analysisConfiguration: { propertyId: 0 },
    });
    expect(result.data).not.toBe(data.multi);
    expect(result.data.length).toBe(1);
  });
});
