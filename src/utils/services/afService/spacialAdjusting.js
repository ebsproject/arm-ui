import clientAxios from 'utils/client/clientAxiosAF';
import { DATA_SET } from 'utils/config';
import {
  AF_PROPERTIES_ANALYSIS_CONFIGURATION_DETAIL_SPACIALADJUSTING,
  getUrlAFAnalysisConfigurationDetail,
  NAME_SPATIAL_ADJUSTED_NO_SPATIAL_ADJUSTMENT,
} from 'utils/helpers/afHelper';

/**
 * @description           Search Spatial adjusting from API
 * @param {array}         experiments - List of occurrences
 * @param {number}        analysisConfiguration - ID of analysis configuration
 * @returns {object}      Information from API
 * @returns {number}      return0.status [200|201|400]  - Number of status GET - 200 => success, POST 201 => succes, 400 => Not found and 500 => error
 * @returns {string}      return0.message - Message of error
 * @returns {array}       return0.data - Data get of API
 * @returns {object}      return0.metadata - General information
 * @returns {object}      return0.metadata.pagination - Information about the pagination
 * @returns {number}      return0.metadata.pagination.totalPages - Total of pages
 * @returns {number}      return0.metadata.pagination.totalCount - Total of element in global request
 */
export async function getSpatialAdjusting({ experiments, analysisConfiguration }) {
  let dataSet = DATA_SET;
  const parameter = {};
  if (analysisConfiguration !== null && experiments.length > 0) {
    const url = getUrlAFAnalysisConfigurationDetail({
      analysisConfigurationId: analysisConfiguration.propertyId,
      analysisConfigurationDetail:
        AF_PROPERTIES_ANALYSIS_CONFIGURATION_DETAIL_SPACIALADJUSTING,
    });
    try {
      const result = await clientAxios.get(url, parameter);
      dataSet.data = result.data.result.data;
      //filter
      const occurenceWithPlotXYFalse = experiments
        .map((experiment) => experiment.occurrences)
        .reduce((a, b) => b)
        .filter((occurrence) => occurrence.isPlotXY === false);
      if (occurenceWithPlotXYFalse.length > 0) {
        const filterData = dataSet.data.filter((spatial) =>
          spatial.propertyName
            .toUpperCase()
            .includes(NAME_SPATIAL_ADJUSTED_NO_SPATIAL_ADJUSTMENT)
        );
        if (filterData.length > 0) {
          dataSet.data = filterData;
        }
      }
      dataSet.status = 200;
    } catch (ex) {
      dataSet.status = 500;
      dataSet.message = 'Unable to view Spacial Adjusting.';
    }
  }
  return dataSet;
}
