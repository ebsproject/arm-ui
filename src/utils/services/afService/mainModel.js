import clientAxios from 'utils/client/clientAxiosAF';
import { DATA_SET } from 'utils/config';
import {
  AF_PROPERTIES_ANALYSIS_CONFIGURATION_DETAIL_MAINMODEL,
  getUrlAFAnalysisConfigurationDetail,
} from 'utils/helpers/afHelper';

/**
 * @description         Find main model with the ID analysis configuration and occurrences
 * @param {object}      param0 - Parameter to search in the apy
 * @param {number}      param0.analysisConfiguration - ID of analysis configuration
 * @returns {object}    Information from API
 * @returns {number}    return0.status [200|201|400]  - Number of status GET - 200 => success, POST 201 => succes, 400 => Not found and 500 => error
 * @returns {string}    return0.message - Message of error
 * @returns {array}     return0.data - Data get of API
 * @returns {object}    return0.metadata - General information
 * @returns {object}    return0.metadata.pagination - Information about the pagination
 * @returns {number}    return0.metadata.pagination.totalPages - Total of pages
 * @returns {number}    return0.metadata.pagination.totalCount - Total of element in global request
 */
export async function getMainModel({ analysisConfiguration }) {
  let dataSet = DATA_SET();
  const parameter = {};
  if(analysisConfiguration !== null){
    const url = getUrlAFAnalysisConfigurationDetail({
      analysisConfigurationId: analysisConfiguration.propertyId,
      analysisConfigurationDetail:
        AF_PROPERTIES_ANALYSIS_CONFIGURATION_DETAIL_MAINMODEL,
    });
    try {
      let result = await clientAxios.get(url, parameter);
      dataSet.data = result.data.result.data;
      dataSet.status = 200;
    } catch (ex) {
      dataSet.status = 500;
      dataSet.message = 'Unable to view Mian Model.';
    }
  }
  return dataSet;
}
