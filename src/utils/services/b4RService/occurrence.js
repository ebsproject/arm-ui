import { traitReducer } from 'components/ui/molecules/Select/TraitAutoSelect/reducer/traitReducer';
import clientAxios from 'utils/client/clientAxiosB4R';
import { DATA_SET, TYPE_DATA } from 'utils/config';
import {
  getUrlB4rOccurrencesSearch,
  getUrlB4rOccurrencesPlotDataSearch,
  SORT_OCCURRENCEDBID_DESC,
  getUrlB4rOccurrencesPlotSearch,
  getUrlVariableDetail,
  getUrlB4rOccurrencesTraitPaSearch,
} from 'utils/helpers/b4rHelper';

/**
 * @description       - Search all occurrences by experiment ID
 * @param {number}    experimentDbId - ID of experiment
 * @param {number}    limit - Max number of element for page
 * @param {number}    page - Number of page
 * @returns {object}  - Information from API
 * @returns {number}  return0.status [200|201|400]  - Number of status GET - 200 => success, POST 201 => succes, 400 => Not found and 500 => error
 * @returns {string}  return0.message - Message of error
 * @returns {array}   return0.data - Data get of API
 * @returns {object}  return0.metadata - General information
 * @returns {object}  return0.metadata.pagination - Information about the pagination
 * @returns {number}  return0.metadata.pagination.totalPages - Total of pages
 * @returns {number}  return0.metadata.pagination.totalCount - Total of element in global request
 */
export async function getOccurrenceByExperimentDbId(
  experimentDbId,
  limit = 10,
  page = 1,
) {
  let dataSet = DATA_SET();
  const parameter = {
    experimentDbId: `${experimentDbId}`.toString(),
  };
  try {
    let url = getUrlB4rOccurrencesSearch({
      limit,
      page,
      sort: SORT_OCCURRENCEDBID_DESC,
    });
    let result = await clientAxios.post(url, parameter);
    dataSet.data = result.data.result.data;
    dataSet.metadata.pagination.totalPages =
      result.data.metadata.pagination.totalPages;
    dataSet.status = 200;
  } catch (ex) {
    //#CHANGE-LOG
    dataSet.status = ex.response.status;
    if (401 === ex.response.status) {
      dataSet.message = 'Unauthorized to view occurrence.';
    } else if (404 === ex.response.status) {
      dataSet.message = 'No occurrence found.';
    } else if (500 <= ex.response.status) {
      dataSet.message = 'Unable to view occurrence.';
    }
  }
  return dataSet;
}

/**
 * @description       - Search all occurrences with Trait an PA coordinates
 * @param {array}     parameters - Parameter to search
 * @param {number}    limit - Max number of element for page
 * @param {number}    page - Number of page
 * @returns {object}  - Information from API
 * @returns {number}  return0.status [200|201|400]  - Number of status GET - 200 => success, POST 201 => succes, 400 => Not found and 500 => error
 * @returns {string}  return0.message - Message of error
 * @returns {array}   return0.data - Data get of API
 * @returns {object}  return0.metadata - General information
 * @returns {object}  return0.metadata.pagination - Information about the pagination
 * @returns {number}  return0.metadata.pagination.totalPages - Total of pages
 * @returns {number}  return0.metadata.pagination.totalCount - Total of element in global request
 */
export async function getOccurrenceTraitPa(
  {
    parameter,
    cropName,
    filter,
  }
) {
  let dataSet = DATA_SET();
  try {
    let url = getUrlB4rOccurrencesTraitPaSearch({
      sort: SORT_OCCURRENCEDBID_DESC,
    });
    let result = await clientAxios.post(url, parameter);
    //dataSet.data = result.data.result.data;
    dataSet.metadata.pagination.totalPages =
      result.data.metadata.pagination.totalPages;
    for (let element of result.data.result.data) {
      //Addinge same name of experiment attributes and occurrences
      element.experimentName = element.experiment;
      element.isOccurrence = true;
      element.typeData = TYPE_DATA.occurence;
      element.id = `${element.experimentDbId}-${element.occurrenceDbId}`;
      element.cropName = cropName;
      element.traits = element.collectedtraits === null ? [] : element.collectedtraits;
      //Filter
      element.traitsFilter = filter.responseVariableOnlyFloat.isActive
        ? element.traits.filter((response) =>
          filter.responseVariableOnlyFloat.valueShouldTrue.includes(
            response.variableDataType.toUpperCase(),
          ),
        )
        : element.traits;
      element.isTraitsFilter = element.traitsFilter.length === 0 ? false : true;
      element.isTraits = element.traits.length === 0 ? false : true;
    }
    dataSet.status = 200;
    dataSet.data = result.data.result.data;
  } catch (ex) {
    //#CHANGE-LOG
    dataSet.status = ex.response.status;
    if (401 === ex.response.status) {
      dataSet.message = 'Unauthorized to view occurrence.';
    } else if (404 === ex.response.status) {
      dataSet.message = 'No occurrence found.';
    } else if (500 <= ex.response.status) {
      dataSet.message = 'Unable to view occurrence.';
    }
  }
  return dataSet;
}

/**
 * @description       - Search plot data from occurrence ID
 * @param {number}    occurrenceDbId - ID of occurrence
 * @returns {object}  - Information from API
 * @returns {object}  return0.status  - Object status about the request
 * @returns {number}  return0.status.status [200|201|400]  - Number of status GET - 200 => success, POST 201 => succes, 400 => Not found and 500 => error
 * @returns {string}  return0.status.message - Message of error
 * @returns {array}   return0.data - Data get of API
 * @returns {object}  return0.metadata - General information
 * @returns {object}  return0.metadata.pagination - Information about the pagination
 * @returns {number}  return0.metadata.pagination.totalPages - Total of pages
 * @returns {number}  return0.metadata.pagination.totalCount - Total of element in global request
 */
export async function getPlotDataByOccurrenceDbId(occurrenceDbId) {
  let dataSet = DATA_SET();
  const parameter = {
    distinctOn: 'variableDbId',
  };
  try {
    let url = getUrlB4rOccurrencesPlotDataSearch(occurrenceDbId);
    const result = await clientAxios.post(url, parameter);
    dataSet.data = result.data.result.data;
    dataSet.metadata.pagination.totalPages =
      result.data.metadata.pagination.totalPages;
    dataSet.status = 200;
    //Search more information from trait
    if (dataSet.data[0].plotData.length > 0) {
      for (let trait of dataSet.data[0].plotData) {
        const resultTrait = await getVariableDetail(trait.variableDbId);
        if (resultTrait.status === 200 && resultTrait.data.length === 1)
          trait.variableDataType = resultTrait.data[0].dataType;
      }
    }
  } catch (ex) {
    //#CHANGE-LOG
    dataSet.status = ex.response.status;
    if (401 === ex.response.status) {
      dataSet.message = 'Unauthorized to view occurrences.';
    } else if (404 === ex.response.status) {
      dataSet.message = 'No occurrences found.';
    } else if (500 <= ex.response.status) {
      dataSet.message = 'Unable to view occurrences.';
    }
  }
  return dataSet;
}

/**
 * @description       - Search more information about the variable in B4R
 * @param {number}    variableID - ID to search detail
 * @returns {object}  - Information from API
 * @returns {object}  return0.status  - Object status about the request
 * @returns {number}  return0.status.status [200|201|400]  - Number of status GET - 200 => success, POST 201 => succes, 400 => Not found and 500 => error
 * @returns {string}  return0.status.message - Message of error
 * @returns {array}   return0.data - Data get of API
 * @returns {object}  return0.metadata - General information
 * @returns {object}  return0.metadata.pagination - Information about the pagination
 * @returns {number}  return0.metadata.pagination.totalPages - Total of pages
 * @returns {number}  return0.metadata.pagination.totalCount - Total of element in global request
 */
async function getVariableDetail(variableID) {
  let dataSet = DATA_SET();
  try {
    const url = getUrlVariableDetail(variableID);
    const result = await clientAxios.get(url, {});
    dataSet.data = result.data.result.data;
    dataSet.metadata.pagination.totalPages =
      result.data.metadata.pagination.totalPages;
    dataSet.status = 200;
  } catch (ex) {
    dataSet.status = 500;
    dataSet.message = 'Unable to view variable.';
  }
  return dataSet;
}

/**
 * @description       - Search plot from occurrence ID
 * @param {number}    occurrenceDbId - ID of occurrence
 * @returns {object}  - Information from API
 * @returns {object}  return0.status  - Object status about the request
 * @returns {number}  return0.status.status [200|201|400]  - Number of status GET - 200 => success, POST 201 => succes, 400 => Not found and 500 => error
 * @returns {string}  return0.status.message - Message of error
 * @returns {array}   return0.data - Data get of API
 * @returns {object}  return0.metadata - General information
 * @returns {object}  return0.metadata.pagination - Information about the pagination
 * @returns {number}  return0.metadata.pagination.totalPages - Total of pages
 * @returns {number}  return0.metadata.pagination.totalCount - Total of element in global request
 */
async function getPlotByOccurrenceDbId(occurrenceDbId) {
  let dataSet = DATA_SET();
  const parameter = {};
  try {
    let url = getUrlB4rOccurrencesPlotSearch(occurrenceDbId);
    let result = await clientAxios.post(url, parameter);
    dataSet.data = result.data.result.data;
    dataSet.metadata.pagination.totalPages =
      result.data.metadata.pagination.totalPages;
    dataSet.status = 200;
  } catch (ex) {
    //#CHANGE-LOG
    dataSet.status = ex.response.status;
    if (401 === ex.response.status) {
      dataSet.message = 'Unauthorized to view occurrences.';
    } else if (404 === ex.response.status) {
      dataSet.message = 'No occurrences found.';
    } else if (500 <= ex.response.status) {
      dataSet.message = 'Unable to view occurrences.';
    }
  }
  return dataSet;
}

/**
 * @description       - Search occurrences with trait information
 * @param {number}    experimentDbId - ID of experiment
 * @returns {object}  - Information from API
 * @returns {number}  return0.status [200|201|400]  - Number of status GET - 200 => success, POST 201 => succes, 400 => Not found and 500 => error
 * @returns {string}  return0.message - Message of error
 * @returns {array}   return0.data - Data get of API
 * @returns {object}  return0.metadata - General information
 * @returns {object}  return0.metadata.pagination - Information about the pagination
 * @returns {number}  return0.metadata.pagination.totalPages - Total of pages
 * @returns {number}  return0.metadata.pagination.totalCount - Total of element in global request
 */
export async function getOccurrenceWithTraitByExperimentDbId(
  {
    experimentDbId,
    filter,
    cropName,
  }
) {
  let dataSet = DATA_SET();
  let result = await getOccurrenceByExperimentDbId(experimentDbId);
  if (result.status === 200) {
    for (let element of result.data) {
      if (dataSet.status === 100) {
        let trait = await getPlotDataByOccurrenceDbId(element.occurrenceDbId);
        let plot = await getPlotByOccurrenceDbId(element.occurrenceDbId);
        if (trait.status === 200) {
          if (trait.data[0].plotData.length > 0) {
            element.traits = trait.data[0].plotData.map((item) => {
              return {
                variableDbId: item.variableDbId,
                variableAbbrev: item.variableAbbrev,
                variableDataType: item.variableDataType,
              };
            });
          } else {
            element.traits = [];
          }
        } else {
          dataSet.status = trait.status;
          dataSet.message = trait.message;
        }
        if (plot.status === 200) {
          if (plot.data[0].plots.length > 0) {
            let fieldXY = plot.data[0].plots.filter((plo) => plo.fieldX !== null);
            element.plots = plot.data[0].plots;
            element.isPlotXY = fieldXY.length > 0 ? true : false;
          } else {
            element.isPlotXY = false;
            element.plots = [];
          }
        } else {
          dataSet.status = plot.status;
          dataSet.message = plot.message;
        }
        //Addinge same name of experiment attributes and occurrences
        element.experimentName = element.experiment;
        element.isOccurrence = true;
        element.typeData = TYPE_DATA.occurence;
        element.id = `${element.experimentDbId}-${element.occurrenceDbId}`;
        element.cropName = cropName;
        //Filter
        element.traitsFilter = filter.responseVariableOnlyFloat.isActive
          ? element.traits.filter((response) =>
            filter.responseVariableOnlyFloat.valueShouldTrue.includes(
              response.variableDataType.toUpperCase(),
            ),
          )
          : element.traits;
        element.isTraitsFilter = element.traitsFilter.length === 0 ? false : true;
        element.isTraits = element.traits.length === 0 ? false : true;
      }
    }
  } else {
    dataSet.status = result.status;
    dataSet.message = result.message;
  }
  if (dataSet.status === 100) {
    dataSet.status = 200;
    dataSet.data = result.data;
  }
  return dataSet;
}

/**
 * @description       - Intersection newtriat with an acumulative traits
 * @param {array}     row - New traits to intersect
 * @param {array}     selectedRows - Array of traits that was intersect
 * @returns {array}   - Array with trati that intersec with other trait
 */
export function intersectionTrait({ row, selectedRows }) { //TODO: Refactor the name
  let result = { isIntersection: false, traits: [] };
  const traitsJoined = selectedRows
    .filter((item) => item.isOccurrence)
    .map((item) => item.traitsFilter);
  const newTrait = row.traitsFilter;
  console.log(JSON.stringify(newTrait));
  // result.traits.push(traitsJoined[0]);
  if (traitsJoined.length === 0) {
    result.isIntersection = true;
    result.traits = newTrait;
  } else {

    let combined = {}
    for (let trait of traitsJoined[0]) {
      combined[trait.variableDbId] = trait
    }
    for (let trait of newTrait) {
      combined[trait.variableDbId] = trait
    }
    result.traits = Object.keys(combined).map((key)=>combined[key]);
    
    result.isIntersection = true; 
    
    // if (
    //   newTrait.filter((item) => result.traits.map((trait) => trait.variableDbId).includes(item.variableDbId)).length > 0
    // ) {
    //   result.isIntersection = true;
    // }
  }
  console.log("Result")
  console.log(JSON.stringify(result)) 
  return result;
}

/**
 * @description     - Find intersection of trait in occurrences
 * @param {array}   occurrences - list of occurrences
 * @returns {array} - Array of trait that intersectioned
 */
export function getIntersectionTraitFromOccurrences(
  { traits }
) {
  //TODO:  This should be renamed 
  let traitsJoined = {}
  if (traits.length > 0) {
    for (let traitsList of traits) {
      for (let trait of traitsList) {
        traitsJoined[trait.variableDbId] = trait
      }
    }    
  }
  return  Object.keys(traitsJoined).map((key)=>traitsJoined[key]);;
  // if (traits.length > 0) {
  //   traitsIntersected = traits[0];
  //   for (let cumulativeTrait of traits) {
  //     traitsIntersected = cumulativeTrait.filter((item) =>
  //       traitsIntersected.map((trait) => trait.variableDbId).includes(item.variableDbId),
  //     );
  //   }
  // }
  // return traitsIntersected;
}

/**
 * @description       - Search all occurrences by parameters
 * @param {number}    param0.limit - Max number of element for page
 * @param {number}    param0.page - Number of page
 * @param {array}     param0.parameters - Array of parameter by id value
 * @returns {object}  - Information from API
 * @returns {number}  return0.status [200|201|400]  - Number of status GET - 200 => success, POST 201 => succes, 400 => Not found and 500 => error
 * @returns {string}  return0.message - Message of error
 * @returns {array}   return0.data - Data get of API
 * @returns {object}  return0.metadata - General information
 * @returns {object}  return0.metadata.pagination - Information about the pagination
 * @returns {number}  return0.metadata.pagination.totalPages - Total of pages
 * @returns {number}  return0.metadata.pagination.totalCount - Total of element in global request
 */
export async function getOccurrence({ limit, page, parameters }) {
  let dataSet = DATA_SET();
  try {
    let url = getUrlB4rOccurrencesSearch({
      limit,
      page,
      sort: SORT_OCCURRENCEDBID_DESC,
    });
    let result = await clientAxios.post(url, parameters);
    dataSet.data = result.data.result.data;
    dataSet.metadata.pagination.totalPages =
      result.data.metadata.pagination.totalPages;
    dataSet.metadata.pagination.totalCount =
      result.data.metadata.pagination.totalCount;
    dataSet.status = 200;
  } catch (ex) {
    //#CHANGE-LOG
    dataSet.status = 500;
    if (401 === ex.response.status) {
      dataSet.message = 'Unauthorized to view occurrence.';
    } else if (404 === ex.response.status) {
      dataSet.message = 'No occurrence found.';
    } else if (500 <= ex.response.status) {
      dataSet.message = 'Unable to view occurrence.';
    }
  }
  return dataSet;
}

/**
 * @description       - Array to define the experiment in experiment table
 * @returns           - Array to define a column
 * @returns {string}  return0.Header - Title of the column
 * @returns {string}  return0.accessor - ID to create operation
 * @returns {boolean} return0.isVisible - Display or hidde column
 * @returns {boolean} return0.canVisible - Enable if can be fidden or show the column by default true
 * @returns {number}  return0.minWidth - Mini width
 */
export const occurrencesColumns = [
  {
    Header: 'Occurrence ID',
    accessor: 'occurrenceDbId',
    isVisible: false,
  },
  {
    Header: 'Experiment ID',
    accessor: 'experimentDbId',
    isVisible: false,
  },
  {
    Header: 'Experiment',
    accessor: 'experiment',
    minWidth: 200,
    canVisible: false,
  },
  {
    Header: 'Location',
    accessor: 'location',
    minWidth: 100,
    canVisible: false,
  },
  {
    Header: 'Occurrence',
    accessor: 'occurrenceName',
    minWidth: 100,
    canVisible: false,
  },
  {
    Header: 'Plant Area',
    accessor: 'plantArea',
    minWidth: 100,
  },
  {
    Header: 'Phase',
    accessor: 'experimentStage',
    minWidth: 100,
  },
  {
    Header: 'Year',
    accessor: 'experimentYear',
  },
  {
    Header: 'Design',
    accessor: 'experimentDesignType',
  },
  {
    Header: 'Entry Type*',
    accessor: 'entryType',
  },
  {
    Header: 'N. Entries',
    accessor: 'entryCount',
  },
  {
    Header: 'Harvest*',
    accessor: 'harvestStatus',
  },
  {
    Header: 'Creator',
    accessor: 'creator',
    minWidth: 100,
  },
];
