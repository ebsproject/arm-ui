import clientAxios from 'utils/client/clientAxiosB4R';
import { DATA_SET, TYPE_DATA } from 'utils/config';
import { getUrlB4rExperimentSearch } from 'utils/helpers/b4rHelper';
import { ExceptionRequestFail } from 'utils/helpers/coreHelper';
import { getOccurrence, getOccurrenceTraitPa, getOccurrenceWithTraitByExperimentDbId } from './occurrence';

/**
 * @description       - Search all experiment from API
 * @param {object}    param0 - Parameter to search in core breeding
 * @param {number}    param0.limit - Max number of element for page
 * @param {number}    param0.page - Number of page
 * @param {array}     param0.searchParameters - List of parameter to search from API
 * @param {object}    param0.filter - Object when informatio to filter data
 * @param {array}     param0.programDbIds - Array with all programs ID
 * @returns {object}  - Information from API
 * @returns {number}  return0.status [200|201|400]  - Number of status GET - 200 => success, POST 201 => succes, 400 => Not found and 500 => error
 * @returns {string}  return0.message - Message of error
 * @returns {array}   return0.data - Data get of API
 * @returns {object}  return0.metadata - General information
 * @returns {object}  return0.metadata.pagination - Information about the pagination
 * @returns {number}  return0.metadata.pagination.totalPages - Total of pages
 * @returns {number}  return0.metadata.pagination.totalCount - Total of element in global request
 */
export async function getExperiment({ limit, page, searchParameters, filter, programDbIds }) {
  const pageFix = page + 1;
  let dataSet = DATA_SET();
  try {
    let searchParameterObject = {};
    searchParameters.forEach((item) => {
      searchParameterObject[item.id] = `${item.value}%`;
    });
    const resultOccurence = await getOccurrence({
      limit,
      page: pageFix,
      parameters: {
        occurrenceStatus: '%planted%|observation',
        experimentType: 'Breeding Trial',
        experimentDesignType: 'RCBD|Alpha-Lattice',
        distinctOn: 'experimentDbId',
        programDbId: programDbIds.join('|'),
        ...searchParameterObject,
      },
    });
    if (resultOccurence.status !== 200) {
      throw new ExceptionRequestFail(
        `Can not get the occurrences - ${resultOccurence.message}`.toString(),
      );
    }
    if (resultOccurence.data.length === 0) {
      throw new ExceptionRequestFail(
        `No filter matches, try again`.toString(),
      )
    }
    const parameter = {
      experimentDbId: resultOccurence.data
        .map((result) => result.experimentDbId)
        .join('|'),
    };
    const url = getUrlB4rExperimentSearch();
    let result = await clientAxios.post(url, parameter);
    dataSet.data = await Promise.all(
      result.data.result.data.map(async (item) => {
        const occurrencesResult = await getOccurrenceTraitPa(
          {
            parameter : {experimentDbId: `${item.experimentDbId}`.toString()},
            cropName: item.cropName,
            filter,
          }
        );
        if (occurrencesResult.status === 200) {
          item.occurrences = occurrencesResult.data;
          const traitsFilter = occurrencesResult.data
            .map((occurrence) => {
              return occurrence.traitsFilter;
            }).flat().filter( (value, index, self) => index === self.findIndex((t) => (
              t.variableDbId === value.variableDbId
            )));
          const traits = occurrencesResult.data
            .map((occurrence) => {
              return occurrence.traitsFilter;
            }).flat().filter( (value, index, self) => index === self.findIndex((t) => (
              t.variableDbId === value.variableDbId
            )));

          item.traits = traits;
          item.traitsFilter = traitsFilter;
          item.isTraits = traits.length === 0
              ? false
              : true;
          item.isTraitsFilter = traitsFilter.length === 0
              ? false
              : true;
        }else {
         throw {message: occurrencesResult.message };
        }
        //Addinge same name of experiment attributes and occurrences
        item.experiment = item.experimentName;
        item.experimentStage = item.stageName;
        item.subRows = item.occurrences;
        item.isExperiment = true;
        item.typeData = TYPE_DATA.experiment;
        item.id = item.experimentDbId;
        return item;
      }),
    );
    dataSet.metadata.pagination.totalPages =
      resultOccurence.metadata.pagination.totalPages;
    dataSet.metadata.pagination.totalCount =
      resultOccurence.metadata.pagination.totalCount;
    dataSet.status = 200;
  } catch (ex) {
    if (ex instanceof ExceptionRequestFail) {
      dataSet.status = 500;
      dataSet.message = ex.message;
    } else {
      dataSet.status = 500;
      dataSet.message = 'Unable to view experiments.';
    }
  }
  return dataSet;
}

/**
 * @description       - Search only one experiment by ID
 * @param {array}     experimentDbIds - List of experiment ID
 * @returns {object}  - Information from API
 * @returns {object}  return0.status  - Object status about the request
 * @returns {number}  return0.status.status [200|201|400]  - Number of status GET - 200 => success, POST 201 => succes, 400 => Not found and 500 => error
 * @returns {string}  return0.status.message - Message of error
 * @returns {array}   return0.data - Data get of API
 * @returns {object}  return0.metadata - General information
 * @returns {object}  return0.metadata.pagination - Information about the pagination
 * @returns {number}  return0.metadata.pagination.totalPages - Total of pages
 * @returns {number}  return0.metadata.pagination.totalCount - Total of element in global request
 */
export async function getExperimentById(experimentDbIds) {
  let dataSet = {
    status: {
      status: 100,
      message: '',
    },
    data: [],
    metadata: {
      pagination: {
        totalPages: 0,
        totalCount: 0,
      },
    },
  };
  const url = getUrlB4rExperimentSearch(100, 1);
  const parameter = {
    experimentDbId: experimentDbIds.join(','),
  };
  try {
    let result = await clientAxios.post(url, parameter);
    dataSet.data = result.data.result.data;
    dataSet.metadata.pagination.totalPages =
      result.data.metadata.pagination.totalPages;
    dataSet.metadata.pagination.totalCount =
      result.data.metadata.pagination.totalCount;
    dataSet.status.status = 200;
  } catch (ex) {
    //#CHANGE-LOG
    dataSet.status.status = ex.response.status;
    if (401 === ex.response.status) {
      dataSet.status.message = 'Unauthorized to view experiments.';
    } else if (404 === ex.response.status) {
      dataSet.status.message = 'No experiments found.';
    } else if (500 <= ex.response.status) {
      dataSet.status.message = 'Unable to view experiments.';
    }
  }
  return dataSet;
}
