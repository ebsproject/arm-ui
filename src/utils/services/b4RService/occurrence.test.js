import { getIntersectionTraitFromOccurrences } from './occurrence';

test('Test get intersection trait from ocurrences - one trait', () => {
  const args = {
    traits: [
      [{ variableDataType: 'FLOAT', variableDbId: 1, variableAbbrev: 'Test1' }],
    ],
  };
  const resutl = getIntersectionTraitFromOccurrences(args);
  expect(resutl.length).toBe(1);
  expect(resutl[0].variableDbId).toBe(1);
  expect(resutl[0].variableAbbrev).toBe('Test1');
});

test('Test get intersection trait from ocurrences - two trait', () => {
  const args = {
    traits: [
      [{ variableDataType: 'FLOAT', variableDbId: 1, variableAbbrev: 'Test1' }],
      [{ variableDataType: 'FLOAT', variableDbId: 1, variableAbbrev: 'Test1' }],
    ],
  };
  const resutl = getIntersectionTraitFromOccurrences(args);
  expect(resutl.length).toBe(1);
  expect(resutl[0].variableDbId).toBe(1);
  expect(resutl[0].variableAbbrev).toBe('Test1');
});

test('Test get intersection trait from ocurrences - occurrences with multiple equals trait ', () => {
  const args = {
    traits: [
      [
        { variableDataType: 'FLOAT', variableDbId: 1, variableAbbrev: 'Test1' },
        { variableDataType: 'INT', variableDbId: 2, variableAbbrev: 'Test2' },
      ],
      [
        { variableDataType: 'FLOAT', variableDbId: 1, variableAbbrev: 'Test1' },
        { variableDataType: 'INT', variableDbId: 2, variableAbbrev: 'Test2' },
      ],
    ],
  };
  const resutl = getIntersectionTraitFromOccurrences( args );
  expect(resutl.length).toBe(2);
});