import { getExperiment, getExperimentById } from './experiment';
import { isExpired } from 'react-jwt';
import clientAxios from 'utils/client/clientAxiosB4R';

jest.mock('react-jwt', () => {
  return {
    isExpired: jest
      .fn()
      .mockImplementationOnce(() => true)
      .mockImplementationOnce(() => false),
  };
});
jest.mock('utils/client/clientAxiosB4R', () => {
  return {
    post: jest
      .fn()
      .mockImplementationOnce(() => {
        return {
          data: {
            result: {
              data: [],
            },
            metadata: {
              pagination: {
                totalPages: 0,
                totalCount: 0,
              },
            },
          },
        };
      })
      .mockImplementationOnce(() => {
        return {
          data: {
            result: {
              data: [{ bar: 'data' }],
            },
            metadata: {
              pagination: {
                totalPages: 1,
                totalCount: 1,
              },
            },
          },
        };
      }),
  };
});
jest.mock('utils/services/b4RService/occurrence', () => {
  return {
    getOccurrence: jest.fn().mockImplementation(() => {
      return {
        status: 200,
        message: '',
        data: [{experimentDbId: 1}],
        metadata: {
          pagination: {
            totalPages: 0,
            totalCount: 0,
          },
        },
      };
    }),
    getOccurrenceWithTraitByExperimentDbId: jest.fn().mockImplementation(() =>{
      return {
        status: 200,
        message: '',
        data: [],
        metadata: {
          pagination: {
            totalPages: 0,
            totalCount: 0,
          },
        },
      }
    }),
  };
});

test('Test service get experiment with expired token', async () => {
  jest.mock('../../config.js', () => ({
    TOKEN: '',
  }));
  const result = await getExperiment({
    limit: 10,
    page: 0,
    searchParameters: [],
    filter: {},
    programDbIds: [],
  });
  expect(result.status).toBe(500);
  expect(result.message).toBe(
    'Session Expired, Please close this tab and log in again.',
  );
});

test('Test service get experiment with token alive and empty data', async () => {
  jest.mock('../../config.js', () => ({
    TOKEN: '',
  }));
  const result = await getExperiment({
    limit: 10,
    page: 0,
    searchParameters: [],
    filter: {},
    programDbIds: [],
  });
  expect(result.status).toBe(200);
});

test('Test service get experiment by id found', async () => {
  jest.mock('../../config.js', () => ({
    TOKEN: '',
  }));
  const result = await getExperimentById([100]);
  expect(result.status.status).toBe(200);
});
