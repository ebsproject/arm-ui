import { getPersonByEmail, getPersonProgram } from './person';

jest.mock('utils/client/clientAxiosB4R', () => {
  return {
    get: jest
      .fn()
      .mockImplementationOnce(() => {
        return {
          data: {
            result: {
              data: [],
            },
            metadata: {
              pagination: {
                totalPages: 0,
                totalCount: 0,
              },
            },
          },
        };
      })
      .mockImplementationOnce(() => {
        return {
          data: {
            result: {
              data: [{ programId: 1 }],
            },
            metadata: {
              pagination: {
                totalPages: 1,
                totalCount: 1,
              },
            },
          },
        };
      })
      .mockImplementationOnce(() => {
        return {
          data: {
            result: {
              data: [],
            },
            metadata: {
              pagination: {
                totalPages: 0,
                totalCount: 0,
              },
            },
          },
        };
      })
      .mockImplementationOnce(() => {
        return {
          data: {
            result: {
              data: [{ programId: 1 }],
            },
            metadata: {
              pagination: {
                totalPages: 1,
                totalCount: 1,
              },
            },
          },
        };
      }),
    post: jest
      .fn()
      .mockImplementationOnce(() => {
        return {
          data: {
            result: {
              data: [{ programId: 1 }],
            },
            metadata: {
              pagination: {
                totalPages: 0,
                totalCount: 0,
              },
            },
          },
        };
      })
      .mockImplementationOnce(() => {
        return {
          data: {
            result: {
              data: [{ personDbId: 10 }],
            },
            metadata: {
              pagination: {
                totalPages: 1,
                totalCount: 1,
              },
            },
          },
        };
      }),
  };
});

jest.mock('react-jwt', () => {
  return {
    isExpired: jest
      .fn()
      .mockImplementationOnce(() => true)
      .mockImplementationOnce(() => false),
  };
});

test('Test finds program who did not exist', async () => {
  const result = await getPersonProgram({ personDbId: 100 });
  expect(result.status).toBe(400);
});

test('Test finds program who did exist', async () => {
  const result = await getPersonProgram({ personDbId: 1 });
  expect(result.status).toBe(200);
});

test('Test finds person who did not exist', async () => {
  const result = await getPersonByEmail('notvalidate@emial.com');
  expect(result.status).toBe(500);
});

test('Test finds person who did not exist program', async () => {
  const result = await getPersonByEmail('withouprogram@emial.com');
  expect(result.status).toBe(400);
});

test('Test finds person who did exist', async () => {
    const result = await getPersonByEmail('valid@emial.com');
    expect(result.status).toBe(200);
  });
