import { getB4RPersonProgram, getB4RPersonSearch } from 'utils/helpers/b4rHelper';
import { DATA_SET } from 'utils/config';
import { ExceptionRequestFail } from 'utils/helpers/coreHelper';
import clientAxios from 'utils/client/clientAxiosB4R';

/**
 * @description       Search information from a person
 * @param {string}    email - Email of user to search information about it 
 * @returns {object}  Information from API
 * @returns {number}  return0.status [200|201|400]  - Number of status GET - 200 => success, POST 201 => succes, 400 => Not found and 500 => error
 * @returns {string}  return0.message - Message of error
 * @returns {array}   return0.data - Data get of API
 * @returns {object}  return0.metadata - General information
 * @returns {object}  return0.metadata.pagination - Information about the pagination
 * @returns {number}  return0.metadata.pagination.totalPages - Total of pages
 * @returns {number}  return0.metadata.pagination.totalCount - Total of element in global request
 */
export async function getPersonByEmail(email) {
  let dataSet = DATA_SET();
  try {
    const url = getB4RPersonSearch();
    const parameter = {
      email: email,
    };
    dataSet.status = 200;
    const result = await clientAxios.post(url, parameter);
    if (result.data.result.data.length === 0) {
      dataSet.status = 400;
      dataSet.message = 'User not found to get information about it';
    }
    const resultProgram = await getPersonProgram({
      personDbId: result.data.result.data[0].personDbId,
    });
    if (resultProgram.data.length === 0) {
      dataSet.status = 400;
      dataSet.message = 'Programs not found for this user';
    }
    dataSet.data = {
      programs: resultProgram.data,
      ...result.data.result.data[0],
    };
  } catch (ex) {
    if (ex instanceof ExceptionRequestFail) {
      dataSet.status = 500;
      dataSet.message = ex.message;
    } else {
      dataSet.status = 500;
      dataSet.message = ex.toString();
    }
  }
  return dataSet;
}

/**
 * @description       Get progrma from the person
 * @param {object}    param0 - Parameter to find all the programs that the person has
 * @param {number}    param0.personDbId - Id of person
 * @returns {object}  - Information from API
 * @returns {number}  return0.status [200|201|400]  - Number of status GET - 200 => success, POST 201 => succes, 400 => Not found and 500 => error
 * @returns {string}  return0.message - Message of error
 * @returns {array}   return0.data - Data get of API
 * @returns {object}  return0.metadata - General information
 * @returns {object}  return0.metadata.pagination - Information about the pagination
 * @returns {number}  return0.metadata.pagination.totalPages - Total of pages
 * @returns {number}  return0.metadata.pagination.totalCount - Total of element in global request
 */
export async function getPersonProgram({ personDbId }) {
  let dataSet = DATA_SET();
  try {
    const url = getB4RPersonProgram({ personDbId });
    const parameter = {};
    let result = await clientAxios.get(url, parameter);
    dataSet.data = result.data.result.data;
    dataSet.metadata.pagination.totalPages =
      result.data.metadata.pagination.totalPages;
    dataSet.metadata.pagination.totalCount =
      result.data.metadata.pagination.totalCount;
    dataSet.status = 200;
    if (dataSet.data.length === 0) {
      dataSet.status = 400;
      dataSet.message = 'User not found to get information about it';
    }
  } catch (ex) {
    if (ex instanceof ExceptionRequestFail) {
      dataSet.status = 500;
      dataSet.message = ex.message;
    } else {
      dataSet.status = 500;
      dataSet.message = ex.toString();
    }
  }
  return dataSet;
}
