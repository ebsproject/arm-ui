import { client } from 'utils/client/clientApolloCore';
import { gql } from '@apollo/client';
import { DATA_SET } from 'utils/config';

/**
 * @description       getUserProfileByToken - return the user profile by the email.
 * @param {string}    email - email to searh the user.
 * @property {object} query - Query to send the GraphQL service.
 * @property {object} client - Axios client to send the request.
 * @returns {object}  dataSet - object with information from API.
 * @returns {object}  dataSet.status - object relate to the response result.
 * @returns {number}  [dataSet.status.status = 200 | 300 | 400] - status of the response 200 = OK.
 * @returns {number}  dataSet.status.message - Text of the error.
 * @returns {array}   dataSet.data - Data from the API.
 * @returns {number}  dataSet.data[].requestorId - ID from the user.
 * @returns {string}  dataSet.data[].institute - First institute assigned to the user.
 * @returns {string}  dataSet.data[].crop - First crop assigned to the user.
 * @returns {object}  dataSet.metadata - General information about the data.
 * @returns {object}  dataSet.pagination - Paging information.
 * @returns {number}  [dataSet.pagination.totalPages = 0 | {number}] - Total of the page for pagination.
 */
export async function getUserProfileByToken(email) {
  let dataSet = DATA_SET();
  let data = {
    requestorId: 0,
    institute: '',
    crop: '',
  };
  try {
    const query = gql`
      query findUserList($val: String!) {
        findUserList(filters: { col: "userName", mod: EQ, val: $val }) {
          content {
            id
            contact {
              id
              person {
                familyName
                givenName
                additionalName
              }
            }
            tenants {
              organization {
                name
              }
            }
          }
        }
      }
    `;
    await client
      .query({
        query: query,
        variables: { val: email.toLowerCase() },
        fetchPolicy: 'no-cache',
      })
      .then((result) => {
        if (result.data.findUserList.content.length === 1) {
          data.requestorId = result.data.findUserList.content[0].id;
          dataSet.data.requestorName =
            `${result.data.findUserList.content[0].contact.person.familyName} ${result.data.findUserList.content[0].contact.person.givenName}`.toString();
          data.institute =
            result.data.findUserList.content[0].tenants[0].organization.name;
          dataSet.status = 200;
        } else {
          dataSet.status = 400;
          dataSet.message = 'No user profile found.';
        }
        dataSet.data = data;
        dataSet.metadata.pagination.totalPages = 0;
      });
  } catch (ex) {
    dataSet.status = 500;
    dataSet.message = ex.toString();
  }
  return dataSet;
}

/**
 * @description       getUserProfileById - return the user profile by the ID.
 * @param {string}    id - ID to searh the user.
 * @property {object} query - Query to send the GraphQL service.
 * @property {object} client - Axios client to send the request.
 * @returns {object}  dataSet - object with information from API.
 * @returns {object}  dataSet.status - object relate to the response result.
 * @returns {number}  [dataSet.status.status = 200 | 300 | 400] - status of the response 200 = OK.
 * @returns {number}  dataSet.status.message - Text of the error.
 * @returns {array}   dataSet.data - Data from the API.
 * @returns {number}  dataSet.data[].requestorId - ID from the user.
 * @returns {string}  dataSet.data[].institute - First institute assigned to the user.
 * @returns {string}  dataSet.data[].crop - First crop assigned to the user.
 * @returns {object}  dataSet.metadata - General information about the data.
 * @returns {object}  dataSet.pagination - Paging information.
 * @returns {number}  [dataSet.pagination.totalPages = 0 | {number}] - Total of the page for pagination.
 */
export async function getUserProfileById(id) {
  let dataSet = {
    status: {
      status: 100,
      message: '',
    },
    data: [],
    metadata: {
      pagination: {
        totalPages: 0,
      },
    },
  };
  let data = {
    requestorId: 0,
    requestorName: '',
  };
  try {
    const query = gql`
      query findUserList($val: ID!) {
        findUser(id: $val) {
          id
          userName
          contact {
            id
            person {
              familyName
              givenName
              additionalName
            }
          }
        }
      }
    `;
    await client
      .query({
        query: query,
        variables: { val: id },
        fetchPolicy: 'no-cache',
      })
      .then((result) => {
        dataSet.status.status = 200;
        data.requestorId = result.data.findUser.id;
        data.requestorName =
          `${result.data.findUser.contact.person.familyName} ${result.data.findUser.contact.person.givenName}`.toString();
        dataSet.data = data;
        dataSet.metadata.pagination.totalPages = 0;
      });
  } catch (ex) {
    //#CHANGE-LOG
    dataSet.status.status = 500;
    dataSet.status.message = ex.toString();
  }
  return dataSet;
}
