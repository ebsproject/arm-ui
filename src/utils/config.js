import { getDomainContext, getTokenId, getAuthState } from '@ebs/layout';

/**
 * @description Number of version to display
 */
export const VERSION = '23.01.31';

/**
 * @description Const to allow select one or multiple experiments
 */
export const MULTIPLE_IN_GRID = 'Multiple';

/**
 * @description URL to API in core breeding
 */
export const CB_API = getDomainContext('cb').sgContext;

/**
 * Change default URL base here
 * #BA-1346
 * @description URL to API in breeding
 */
export const BA_API = getDomainContext('ba').sgContext;

/**
 * @description URL to GraphQL in core system
 */
export const CS_API_GRAPHQL = `${
  getDomainContext('cs').sgContext
}graphql`.toString();

/**
 * @description Enable or disable [0|1] pop up to set new token in storage location
 */
export const TOKEN_HELP_POPUP = 0;

/**
 * @description Bear token with authentication
 */
export const TOKEN =
  TOKEN_HELP_POPUP === 1
    ? localStorage.getItem(TOKEN_KEY_STORAGE_LOCATION)
    : getTokenId();

/**
 * @description       Filter row configuration
 * @returns {object}  Configuration setting
 * @returns {string}  return0.keyStorageLocation - Key to search in locat storage
 * @returns {object}  return0.sameExperiemnt - Information about the filter same experiment
 * @returns {bool}    return0.sameExperiemnt.isActive - Active filter
 * @returns {bool}    return0.sameExperiemnt.isEnable - Enable or disable filter in component
 * @returns {object}  return0.responseVariableOnlyFloat - Information about the filter response variable only float
 * @returns {bool}    return0.responseVariableOnlyFloat.isActive - Active filter
 * @returns {bool}    return0.responseVariableOnlyFloat.isEnable - Enable or disable filter in component
 */
export const FILTER_ROW = {
  keyStorageLocation: 'ba-filter-row',
  configuration: {
    sameExperiemnt: {
      isActive: true,
      isEnable: false,
    },
    responseVariableOnlyFloat: {
      isActive: true,
      isEnable: true,
      valueShouldTrue: ['FLOAT', 'INTEGER'],
    },
  },
};

/**
 * @description Key to search in local storage token
 */
export const TOKEN_KEY_STORAGE_LOCATION = 'id_token';

/**
 * @description Search the user information from context
 */
export async function FUNCTION_USER_PROFILE() {
  const userProfile = await getAuthState();
  return {
    userID: userProfile['userId'],
    userExternalID: userProfile['externalId'],
    userEmail: userProfile['http://wso2.org/claims/emailaddress'],
  };
}

/**
 * @description         Variable to return function in service function
 * @returns {object}    Object to save all information from request
 * @returns {number}    return0.status.status [200|201|400]  - Number of status GET - 200 => success, POST 201 => succes, 400 => Not found and 500 => error
 * @returns {string}    return0.status.message - Message of error
 * @returns {array}     return0.data - Data get of API
 * @returns {object}    return0.metadata - General information
 * @returns {object}    return0.metadata.pagination - Information about the pagination
 * @returns {number}    return0.metadata.pagination.totalPages - Total of pages
 * @returns {number}    return0.metadata.pagination.totalCount - Total of element in global request
 */
export const DATA_SET = () => ({
  status: 100,
  message: '',
  data: [],
  metadata: {
    pagination: {
      totalPages: 0,
      totalCount: 0,
    },
  },
});

export const MESSAGE_SEVERITY = {
  warning: 'warning',
  info: 'info',
  error: 'error',
};

export const TYPE_DATA = {
  experiment: 'experiment',
  occurence: 'occurrence',
};
