import {
  ApolloClient,
  ApolloLink,
  from,
  HttpLink,
  InMemoryCache,
} from '@apollo/client';
import { CS_API_GRAPHQL, TOKEN } from 'utils/config';

const httpLink = new HttpLink({
  uri: CS_API_GRAPHQL,
});

/**
 * @description - Adding default configuration to get information from server with GraphQL
 */
const authLink = new ApolloLink((operation, forward) => {
  operation.setContext(({ headers }) => ({
    headers: {
      ...headers,
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      Authorization: `Bearer ${TOKEN}`,
    },
  }));
  return forward(operation);
});

/**
 * @description - Expose the ApolloClient
 */
export const client = new ApolloClient({
  link: from([authLink, httpLink]),
  cache: new InMemoryCache({
    addTypename: false,
  }),
});
