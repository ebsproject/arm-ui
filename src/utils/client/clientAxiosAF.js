import Axios from 'axios';
import { BA_API, TOKEN } from 'utils/config';

/**
 * @description - Instance of axios to connect to AF API
 * @returns - instance of axios
 */
const instance = Axios.create({
  baseURL: BA_API,
  headers: {
    Accept: 'application/json',
    //Authorization: `Bearer ${TOKEN}`,
  },
});

/**
 * @description - Create a interceptors to update the token
 */
//client.interceptors.request.use(function (config) {
//  config.headers.Authorization = `Bearer ${TOKEN}`;
//  return config;
//});

export default instance;
