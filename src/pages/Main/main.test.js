import React, { useState } from 'react';
import ReactDOM, {unmountComponentAtNode} from 'react-dom';
// Component to be Test
import Main from './mainview';
// Test Library
import { render, cleanup, screen } from '@testing-library/react';
import '@testing-library/dom';
import '@testing-library/jest-dom/extend-expect';
import { act } from 'react-dom/test-utils';
import * as user from '../../utils/services/coreService/user';
import * as ApolloMock from '@apollo/client';


let container = null;
beforeEach(() => {
  container = document.createElement('div');
  document.body.appendChild(container);
});
afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

// Props to send component to be rendered
const props = {};
// const realUseState = useState;
// jest.mock('../../reducks/modules/NewRequest', () => {
//   return { NEW_REQUEST_ACTION: 1 };
// });
// jest.spyOn(user, 'getUserProfileByToken').mockImplementation(() =>
//   Promise.resolve({
//     status: {
//       status: 100,
//       message: '',
//     },
//     data: [],
//     metadata: {
//       pagination: {
//         totalPages: 0,
//         totalCount: 0,
//       },
//     },
//   }),
// );
// jest.spyOn(user, 'getUserProfileById').mockImplementation(() =>
//   Promise.resolve({
//     status: {
//       status: 100,
//       message: '',
//     },
//     data: [],
//     metadata: {
//       pagination: {
//         totalPages: 0,
//         totalCount: 0,
//       },
//     },
//   }),
// );
// jest.mock('../../components/ui/templates/MainRequest', () => () => {
//   return <div>Test</div>;
// });
jest.mock('@apollo/client', () => {
  return {
    ApolloClient: jest.fn().mockImplementation(() => {
      return {};
    }),
    ApolloLink: jest.fn().mockImplementation(() => {
      return {};
    }),
    from: jest.fn().mockImplementation(() => {
      return {};
    }),
    HttpLink: jest.fn().mockImplementation(() => {
      return {};
    }),
    InMemoryCache: jest.fn().mockImplementation(() => {
      return {};
    }),
  };
});
test('Report name',async () => {
  const div = document.createElement('div');
  await act(async () => {
    ReactDOM.render(<Main {...props}></Main>, div);
  });
});

test('Render correctly', async () => {
  await act(async () => {
    const { getByTestId } = render(<Main {...props}></Main>);
    expect(getByTestId('MainTestId')).toBeInTheDocument();
  });
});
