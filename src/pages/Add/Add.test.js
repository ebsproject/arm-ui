import Add from './Add';
import React, { useState } from 'react';
import { render, cleanup, screen } from '@testing-library/react';
import '@testing-library/dom';
import '@testing-library/jest-dom/extend-expect';
import * as ApolloMock from '@apollo/client';
import * as user from '../../utils/services/coreService/user';

afterEach(cleanup);

const realUseState = useState;
jest.mock('@apollo/client', () => {
  return {
    ApolloClient: jest.fn().mockImplementation(() => {
      return {};
    }),
    ApolloLink: jest.fn().mockImplementation(() => {
      return {};
    }),
    from: jest.fn().mockImplementation(() => {
      return {};
    }),
    HttpLink: jest.fn().mockImplementation(() => {
      return {};
    }),
    InMemoryCache: jest.fn().mockImplementation(() => {
      return {};
    }),
  };
});


test('Search component by ID', () => {
  jest
    .spyOn(React, 'useState')
    .mockImplementationOnce(() =>
      realUseState({ success: 0, message: '', data: {} }),
    );
  render(<Add></Add>);
  expect(screen.getByTestId('AddTestId')).toBeInTheDocument();
  expect(screen.getByTestId('alertAddTestId')).toBeInTheDocument();
});

test('Search Alter message with error', () => {
  jest
    .spyOn(React, 'useState')
    .mockImplementationOnce(() =>
      realUseState({ success: -1, message: 'Mensage-Error', data: {} }),
    );
  const { container, unmount } = render(<Add></Add>);
  const message = screen.getByText(/Mensage-Error/i);
  expect(message).toBeInTheDocument();
});

jest.mock('../../components/ui/templates/AddRequest', () => () => <div>ChildComponent</div>);
test('Mount children component', () => {
  jest
  .spyOn(React, 'useState')
  .mockImplementationOnce(() =>
    realUseState({ success: 1, message: '', data: {} }),
  );
  render(<Add></Add>);
  expect(screen.getByText(/ChildComponent/i)).toBeInTheDocument();
});
