import React, { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
// GLOBALIZATION COMPONENT
import { FormattedMessage } from 'react-intl';
import { Core } from '@ebs/styleguide';
// CORE COMPONENTS
const { Typography } = Core;
//Other components
import { Alert, AlertTitle } from '@material-ui/lab';
import {
  FILTER_ROW,
  TOKEN_HELP_POPUP,
  FUNCTION_USER_PROFILE,
  CB_API,
  VERSION,
} from 'utils/config';
import { ROUTE_ADD_REQUEST, ROUTE_LIST_REQUEST } from 'utils/routes';
import { getSpatialAdjusting } from 'utils/services/afService/spacialAdjusting';
import { getMainModel } from 'utils/services/afService/mainModel';
import { getAnalysisConfiguration } from 'utils/services/afService/analysisConfiguration';
import { getExperimentLocationAnalysisPattern } from 'utils/services/afService/experimentLocationAnalysisPattern';
import { getTraitAnalysisPattern } from 'utils/services/afService/traitAnalysisPattern';
import { getAnalysisObjective } from 'utils/services/afService/analysisObjective';
import {
  getIntersectionTraitFromOccurrences,
  intersectionTrait,
  occurrencesColumns,
} from 'utils/services/b4RService/occurrence';
import { getExperiment } from 'utils/services/b4RService/experiment';
import { getPersonByEmail } from 'utils/services/b4RService/person';
import AddRequestTemplate from 'components/ui/templates/AddRequest';
import { postSaveNewRequest } from 'utils/services/afService/request';
import { getPredictionByExpLoc } from 'utils/helpers/afHelper';
import { getUserProfileByToken } from 'utils/services/coreService/user';
import { useConfig } from 'utils/useConfig';

/**
 * @description         Pages to add new request
 * @property {state}    userProfileStatus - State to save all data of the user profile
 * @property {state}    requestorProfile - State to save only the necessary data of the user profile
 * @property {object}   titleData - Object with the configuration of title
 * @property {object}   selects - Object with the configuration of combo box
 * @property {object}   setting - Object with the configuration to setting all component
 * @property {object}   experiment - Object with the configuration of experiment and occurrence
 * @property {object}   request - Object with the configuration of the request
 * @property {void}     useEffect - Get the user profile
 * @property {string}   data-testid - Id to use inside add.test.js file.
 * @returns {component} Page to add new request
 */
export default function AddView({ }) {
  const [userConfiguration, setUserConfiguration] = React.useState({
    success: 0,
    message: '',
    data: {},
  });
  const mountedRef = useRef(true);

  const titleData = {
    title: 'Analytics Request Manager',
    subtitle: 'Add New Request',
    note: '',
  };
  const setting = {
    tokenHelpPopUp: TOKEN_HELP_POPUP === 1 ? true : false,
    routes: {
      rotuelist: `/ba${ROUTE_LIST_REQUEST}`,
      routeAdd: `/ba${ROUTE_ADD_REQUEST}`,
    },
    filterRow: FILTER_ROW,
  };
  const selects = {
    responseVariableTrait: {
      title: 'Response variable',
      functionGetData: getIntersectionTraitFromOccurrences,
    },
    analysisObjective: {
      title: 'Analysis objective',
      functionGetData: getAnalysisObjective,
    },
    traitAnalysisPattern: {
      title: 'Trait Analysis Pattern',
      functionGetData: getTraitAnalysisPattern,
    },
    experimentLocationAnalysisPattern: {
      title: 'Experiment Location Analysis Pattern',
      functionGetData: getExperimentLocationAnalysisPattern,
    },
    analysisConfiguration: {
      title: 'Analysis Configuration',
      functionGetData: getAnalysisConfiguration,
    },
    mainModel: {
      title: 'Main Model',
      functionGetData: getMainModel,
    },
    prediction: {
      title: 'Prediction',
      functionGetData: getPredictionByExpLoc,
    },
    spatialAdjusting: {
      title: 'Spatial Adjusting',
      functionGetData: getSpatialAdjusting,
    },
    analysisType: {
      value: 'ANALYZE',
    },
  };
  const experiment = {
    functionGetExperiment: getExperiment,
    columns: occurrencesColumns,
    functionGetTrait: intersectionTrait,
    buttons: {
      setting: {
        title: 'Personalize Grid Configuration',
        helpText: 'Grid Configuration',
        id: 'arm-grid-config'
      },
      filter: {
        title: 'Filter for select occurrences',
        helpText: 'Update filter for occurrences',
      },
    },
    style: {
      rowOccurrence: {},
    },
  };

  const request = {
    functionSaveRequest: postSaveNewRequest,
    dataSourceUrl: CB_API,
    userConfiguration: userConfiguration,
  };

  useEffect(() => {
    const fetchData = async () => {
      if (userConfiguration.success === 0) {
        const userContext = await FUNCTION_USER_PROFILE();
        if (userConfiguration.success === 0 && userContext.userEmail !== undefined) {
          const result = await getPersonByEmail(userContext.userEmail);
          const resultCore = await getUserProfileByToken(userContext.userEmail);
          const { canPermissionAplicacion } = useConfig();

          const sinlge = canPermissionAplicacion({ prefix: 'ba', product: 'Analysis Request Manager', type: 'actions', actionName: 'Single_Design' });
          const multi = canPermissionAplicacion({ prefix: 'ba', product: 'Analysis Request Manager', type: 'actions', actionName: 'Multi_Design' });
          //TODO: force permision multi
          //const multi = true;
          //console.log('Permision : ', { multi, sinlge });

          if (!mountedRef.current) return null;
          if (result.status === 200 && resultCore.status === 200 && (multi || sinlge)) {
            setUserConfiguration({
              success: 1,
              data: {
                dataSource: 'EBS',
                permissions: {
                  desingExperiment: {
                    multiValue: multi === true,
                    label: multi ? 'Multi experiment' : 'Single experiment'
                  },
                },
                ...userContext,
                ...result.data,
                ...resultCore.data,
              },
              message: '',
            });
          } else {
            setUserConfiguration({
              success: -1,
              message: result.message === '' ? (!multi && !sinlge) ? 'Failed to get permission please check if your user has permission to create a request' : 'Failed to get user information' : result.message,
              data: {},
            });
          }
        }
      }
    };
    fetchData().catch((ex) =>
      setUserConfiguration({
        success: -1,
        message: ex.toString(),
        data: {},
      }),
    );
    return () => {
      mountedRef.current = false;
    }
  }, [setUserConfiguration, userConfiguration]);
  return (
    <div data-testid={'AddTestId'}>
      {userConfiguration.success === 1 ? (
        <AddRequestTemplate
          titledata={titleData}
          setting={setting}
          selects={selects}
          experiment={experiment}
          request={request}
          version={VERSION}
        ></AddRequestTemplate>
      ) : (
        <Alert severity='error' data-testid={'alertAddTestId'}>
          <AlertTitle>{userConfiguration.message !== '' ? 'Error' : ''}</AlertTitle>
          {userConfiguration.message !== '' ? `${userConfiguration.message} -v${VERSION}` : `Loading...`}
        </Alert>
      )}
    </div>
  );
}
// Type and required properties
AddView.propTypes = {};
// Default properties
AddView.defaultProps = {};
