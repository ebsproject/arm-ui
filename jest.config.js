module.exports = {
  testEnvironment: 'jsdom',
  collectCoverageFrom: ['src/**/*.js'],
  modulePathIgnorePatterns: ['/cache', '/dist'],
  transform: {
    '^.+\\.(j|t)sx?$': 'babel-jest',
  },
  moduleNameMapper: {
    '\\.(css)$': 'identity-obj-proxy',
    'single-spa-react/parcel': 'single-spa-react/lib/cjs/parcel.cjs',
    '@ebs/styleguide': '<rootDir>/__mocks__/styleguide.js',
    '@ebs/layout': '<rootDir>/__mocks__/layout.js',
    '@ebs/components': '<rootDir>/__mocks__/ebs-component.js',
  },
  moduleDirectories: ['node_modules', 'src'],
  setupFilesAfterEnv: ['./jest.setup.js'],
  coverageDirectory: 'coverage/',
  coverageReporters: ['json','html'],
};
